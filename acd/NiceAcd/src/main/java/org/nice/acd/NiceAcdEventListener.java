package org.nice.acd;

import com.nice.saas.wfo.acd.rta.GenericACDEvent;
import com.nice.saas.wfo.acd.rta.GenericACDEventListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class NiceAcdEventListener implements GenericACDEventListener{
    @Override
    public void processEvents(List<GenericACDEvent> list) {

    }

    @Override
    public void processEvent(GenericACDEvent genericACDEvent) {

    }
}
