package org.nice.acd;


//import com.nice.saas.wfo.acd.cisco.AcdEventServiceJtapi;
import com.nice.saas.wfo.acd.cisco.uc.AcdEventServiceUcceImpl;


//import com.nice.saas.wfo.acd.cisco.AcdReportFactory;
//import com.nice.saas.wfo.comp.manage.asc.cisco.AdherenceNormalizer;
//import com.nice.saas.wfo.comp.manage.asc.cisco.AdherenceRowItem;
//import com.nice.saas.wfo.comp.manage.asc.cisco.ForecastRowItem;
//import com.nice.saas.wfo.comp.manage.asc.cisco.NormalizerFactory;
//import com.nice.saas.wfo.comp.manage.asc.cisco.interfaces.GenericNormalizer;
//import com.nice.saas.wfo.comp.manage.asc.cisco.interfaces.GenericNormalizerFactory;
//import com.nice.saas.wfo.acd.cisco.uc.AcdUccxReportFactory;


import com.nice.saas.wfo.acd.cisco.uc.AcdUcceReportFactory;

import com.nice.saas.wfo.acd.cisco.uc.AcdUcceReportFactory;
import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;
import com.nice.saas.wfo.acd.historicaldata.GenericReportService;
import com.nice.saas.wfo.acd.rta.GenericACDEventListener;
import com.nice.saas.wfo.acd.rta.GenericACDEventService;

import com.nice.saas.wfo.comp.manage.asc.cisco.NormalizerFactoryUcce;
import com.nice.saas.wfo.comp.manage.asc.cisco.NormalizerFactoryUccx;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;

import com.nice.saas.wfo.comp.manager.asc.generic.Normalizer;
import com.nice.saas.wfo.comp.manager.asc.generic.NormalizerFactory;
import com.nice.saas.wfo.comp.manager.asc.generic.ReportType;
import com.nice.saas.wfo.comp.manager.asc.generic.json.AdherenceRowItem;
import com.nice.saas.wfo.comp.manager.asc.generic.json.ForecastRowItem;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Component;


import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Component
@Configuration
@ComponentScan(basePackages = {
        "org.nice.acd",
        "com.nice.saas.wfo.comp.manage.asc.cisco",
        "com.nice.saas.wfo.acd.cisco"
})
@PropertySource("classpath:app.properties")
public class AcdCiscoNodeApp {
    final static Logger log4j = Logger.getLogger(AcdCiscoNodeApp.class);

    final static GenericLogger logger = new GenericLogger() {
        @Override
        public void info(String s) {
            log4j.info(s);
        }

        @Override
        public void error(String s, Throwable throwable, Object... objects) {
            log4j.error(s, throwable);
        }

        @Override
        public void warning(String s) {
            log4j.warn(s);
        }
    };

    public static void main(String[] args) throws IOException, InterruptedException {


        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        try {
            context.register(AcdCiscoNodeApp.class);
            context.refresh();
            MyACDErrorHandler.context= context;
//            Map<String,NormalizerFactory>  l = context.getBeansOfType(NormalizerFactory.class);
//            List<Normalizer> nlist = new ArrayList<>();
//            for (NormalizerFactory f: l.values()) {
//                nlist.add(f.getNormalizer(ReportType.Adherence, 0));
//                nlist.add(f.getNormalizer(ReportType.Forecast, 0));
//            }

//            AcdEventServiceJtapi jtapi = new AcdEventServiceJtapi();
//            jtapi.addLogger(logger);
//            jtapi.setHost("172.21.13.193");
//            jtapi.start();



            GenericACDEventListener acdEventListener =  context.getBean(NiceAcdEventListener.class);
            GenericACDEventService acdEventService = context.getBean(AcdEventServiceUcceImpl.class);
            acdEventService.addLogger(logger);
            acdEventService.addEventListener(acdEventListener);
            acdEventService.start();



            Map<String, String> initDetails = new HashMap<String, String> (){{
//                put ("url", "jdbc:informix-sqli://{host}:{port}/{schema}:INFORMIXSERVER={server}");
//                put ("host", "172.21.13.198");
//                put ("port", "1504");
//                put ("server", "cuccx115_uccx");
//                put ("schema", "db_cra");
//                put ("username", "uccxhruser");
//                put ("password", "12345");
                put ("url", "jdbc:jtds:sqlserver://{host}:{port}/{schema}");
                  put ("host", "172.21.13.87");
                  put ("port", "1433");
                  put ("server", "");
                  put ("schema", "ic115_awdb");
                put ("username", "uccehr");
                put ("password", "123456");
            }};
            AcdUcceReportFactory factory =  context.getBean(AcdUcceReportFactory.class);
            factory.addLogger(logger);
           GenericReportService service = factory.getReportService(ReportType.Adherence);
           service.init(initDetails);
            boolean connected = service.connect();
        //    for (int i =0; i < 5000; ++i) {
                String csv = service.getReportData(Instant.parse("2017-09-26T03:50:00.00Z"), Instant.parse("2017-10-26T04:10:47.00Z")); //Instant.now().plus(180, ChronoUnit.MINUTES).minus(1, ChronoUnit.DAYS), Instant.now().plus(180, ChronoUnit.MINUTES));

                NormalizerFactory nfactory = context.getBean(NormalizerFactoryUcce.class);
                Normalizer adherenceNormalizer = nfactory.getNormalizer(ReportType.Adherence, 0);
            Iterator it;
            if (csv != null) {
                    it = adherenceNormalizer.getIterator(csv, logger);
                    logger.info(String.format("Executed adherence report: %s", csv));
                }

                GenericReportService service1 = factory.getReportService(ReportType.Forecast);
                service1.init(initDetails);

                 csv = service1.getReportData(Instant.parse("2017-09-26T03:50:00.00Z"), Instant.parse("2017-10-26T04:10:47.00Z"));//Instant.now().plus(180, ChronoUnit.MINUTES).minus(80, ChronoUnit.HOURS), Instant.now().plus(180, ChronoUnit.MINUTES));
                 nfactory = context.getBean(NormalizerFactoryUccx.class);
               Normalizer hdNormalizer = nfactory.getNormalizer(ReportType.Forecast, 0);
               if (csv != null) {
                   it = hdNormalizer.getIterator(csv, logger);
                   logger.info(String.format("Executed forecast report: %s", csv));
               }
               // logger.info(String.format("ITERATION: %s", i));
                Thread.sleep(10);
      //     }

            while (true) { Thread.sleep(10000);   }

        }
        catch (Exception ex) {
            int q=0;
        }finally {
            context.close();
        }


    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfig() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}

