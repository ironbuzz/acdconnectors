package org.nice.acd;

import com.nice.saas.wfo.comp.manager.asc.generic.ACDErrorHandler;
import com.nice.saas.wfo.comp.manager.asc.generic.ConnectionError;
import com.nice.saas.wfo.comp.manager.asc.generic.GeneralError;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class MyACDErrorHandler implements ACDErrorHandler {
    static AnnotationConfigApplicationContext context;

    public void generalError(GeneralError error) {
        System.out.println("Error" + error.toString());



        context.close();

        System.exit(-1);
    }

    public void connectionError(ConnectionError error) {
        System.out.println("ConnectionError" + error.toString());

        context.close();

        System.exit(-1);
    }
}
