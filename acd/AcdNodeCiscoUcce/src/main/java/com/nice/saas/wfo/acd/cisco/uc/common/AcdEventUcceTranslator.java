package com.nice.saas.wfo.acd.cisco.uc.common;

import com.nice.saas.wfo.acd.cisco.common.Constants;
import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import com.nice.saas.wfo.acd.rta.GenericACDEvent;
import org.springframework.stereotype.Component;

@Component(value = "AcdEventUcTranslator")
public class AcdEventUcceTranslator extends AcdEventUcTranslator {

    private final String className = "AcdEventUcceTranslator";

    /**
     * Translate UC message to GenericAcdEvent
     * @param message
     * @return
     */
    @Override
    public GenericACDEvent translate(Message message) {
        GenericACDEvent event = super.translate(message);
        if (event != null) {
            event.setAcdId(Constants.AcdCiscoUcceReportType);
        }
        return event;
    }
}
