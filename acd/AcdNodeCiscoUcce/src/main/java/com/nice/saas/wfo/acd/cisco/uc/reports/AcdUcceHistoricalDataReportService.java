package com.nice.saas.wfo.acd.cisco.uc.reports;

import com.nice.saas.wfo.acd.cisco.AcdReportService;
import com.nice.saas.wfo.acd.cisco.common.Constants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * AcdUcceHistoricalDataReportService
 */
@Component
public class AcdUcceHistoricalDataReportService extends AcdReportService {
    public AcdUcceHistoricalDataReportService() {
        super.setDbClassName(Constants.MsSqlDriverClassName);
    }
    @Override
    @Value("${cisco.uc.acd.reports.acdUcceHistoricalDataReportService.query}")
    protected void setQuery(String query) {
        super.setQuery(query);
    }

}
