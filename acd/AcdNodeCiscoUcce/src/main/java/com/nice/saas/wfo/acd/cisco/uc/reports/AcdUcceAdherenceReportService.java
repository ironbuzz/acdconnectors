package com.nice.saas.wfo.acd.cisco.uc.reports;


import com.nice.saas.wfo.acd.cisco.AcdReportService;
import com.nice.saas.wfo.acd.cisco.common.Constants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * AcdUcceAdherenceReportService
 */
@Component
public class AcdUcceAdherenceReportService extends AcdReportService {
    public AcdUcceAdherenceReportService() {
        super.setDbClassName(Constants.MsSqlDriverClassName);
    }

    @Override
    @Value("${cisco.uc.acd.reports.acdUcceAdherenceReportService.query}")
    protected void setQuery(String query) {
        super.setQuery(query);
    }

}
