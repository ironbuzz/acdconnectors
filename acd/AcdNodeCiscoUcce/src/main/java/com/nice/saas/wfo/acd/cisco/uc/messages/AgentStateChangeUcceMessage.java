package com.nice.saas.wfo.acd.cisco.uc.messages;

import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;

/**
 * AgentStateChangeUcceMessage - indicates agent state change
 */
public class AgentStateChangeUcceMessage extends AgentStateChangeUcMessage {
    public AgentStateChangeUcceMessage() {
        fixedFieldsLength.put(ConstantsUc.UcMessageFields.UnusedBlock2, 28);
    }
}
