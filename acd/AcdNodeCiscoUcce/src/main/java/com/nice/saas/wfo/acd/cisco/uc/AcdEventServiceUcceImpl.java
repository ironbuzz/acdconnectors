package com.nice.saas.wfo.acd.cisco.uc;

import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;
import com.nice.saas.wfo.acd.cisco.uc.messages.*;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

import static com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc.*;

@Service
public class AcdEventServiceUcceImpl extends AcdEventServiceUcImpl {

    @PostConstruct
    public void init() {
        super.init();
        UcFloatingFieldsTypeIds.put((byte)5, UcMessageFields.AgentId);
        UcMessageTypeIds.put(AgentStateChangeUcceMessage.class, 30);
        UcMessageTypes.put(ConstantsUc.UcMessageTypeIds.get(AgentStateChangeUcceMessage.class), AgentStateChangeUcceMessage.class);
    }

}
