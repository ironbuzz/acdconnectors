package com.nice.saas.wfo.acd.cisco.uc;

import com.nice.saas.wfo.acd.cisco.common.TcpClient;
import com.nice.saas.wfo.acd.cisco.uc.common.*;
import com.nice.saas.wfo.acd.cisco.uc.messages.*;
import com.nice.saas.wfo.acd.rta.GenericACDEvent;
import com.nice.saas.wfo.acd.rta.GenericACDEventListener;
import com.nice.saas.wfo.comp.manager.asc.generic.*;
import org.junit.Before;
import org.junit.Test;

import java.awt.event.ActionEvent;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.*;

public class AcdEventServiceUcceImplTest {
    @Before
    public void  setUp() {
        service.receiver = new UcReceiver();
        service.messageParser = new UcMessageParser();
        service.acdMessageTranslator = new AcdEventUcceTranslator();
    }

    AcdEventServiceUcceImpl service = new AcdEventServiceUcceImpl();

    @Test
    public void  init() throws NoSuchFieldException, IllegalAccessException {

        Field field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 50);
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 50);
        field = service.getClass().getSuperclass().getDeclaredField("openReqUcMessage");
        field.setAccessible(true);
        field.set(service, new OpenReqUcMessage());


        service.init();
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("messages");
        field.setAccessible(true);
        assertNotNull(field.get(service));
        assertTrue(field.get(service) instanceof ArrayBlockingQueue);
        assertEquals(((ArrayBlockingQueue)field.get(service)).remainingCapacity(), 50);

        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("eventsProcessorPool");
        field.setAccessible(true);
        assertNotNull(field.get(service));
        assertTrue(field.get(service) instanceof ExecutorService);

    }

    @Test
    public void  addEventListener() throws NoSuchFieldException, IllegalAccessException {
        Field field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 50);
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 50);
        field = service.getClass().getSuperclass().getDeclaredField("openReqUcMessage");
        field.setAccessible(true);
        field.set(service, new OpenReqUcMessage());


        service.init();
        service.addEventListener(new GenericACDEventListener() {
            @Override
            public void processEvents(List<GenericACDEvent> list) {

            }

            @Override
            public void processEvent(GenericACDEvent genericACDEvent) {

            }
        });
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("acdEventListeners");
        field.setAccessible(true);
        ArrayList l = (ArrayList)field.get(service);
        assertNotNull(l);
        assertEquals(l.size(), 1);
    }

    @Test
    public void  addLogger() throws NoSuchFieldException, IllegalAccessException {
        Field field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 50);
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 50);
        field = service.getClass().getSuperclass().getDeclaredField("openReqUcMessage");
        field.setAccessible(true);
        field.set(service, new OpenReqUcMessage());


        service.init();
        service.addLogger(new GenericLogger() {
            @Override
            public void info(String s) {

            }

            @Override
            public void error(String s, Throwable throwable, Object... objects) {

            }

            @Override
            public void warning(String s) {

            }
        });
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("logger");
        field.setAccessible(true);
        GenericLogger l = (GenericLogger)field.get(service);
        assertNotNull(l);
    }

    @Test
    public void  destroy() throws NoSuchFieldException, IllegalAccessException {
        Field field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 50);
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 50);
        field = service.getClass().getSuperclass().getDeclaredField("openReqUcMessage");
        field.setAccessible(true);
        field.set(service, new OpenReqUcMessage());


        service.init();
        service.addLogger(new GenericLogger() {
            @Override
            public void info(String s) {

            }

            @Override
            public void error(String s, Throwable throwable, Object... objects) {

            }

            @Override
            public void warning(String s) {

            }
        });
        UcReceiver receiver = (UcReceiver)service.receiver;
        field = receiver.getClass().getSuperclass().getDeclaredField("client");
        field.setAccessible(true);
        field.set(receiver, new TcpClient("127.0.0.1", 8080, 40, 40) {
            @Override
            protected void onRead(ByteBuffer buf) throws Exception {

            }

            @Override
            protected void onConnected() throws Exception {

            }

            @Override
            protected void onDisconnected() throws Exception {

            }

            @Override
            protected void onFailure(Exception e) throws Exception {

            }
        });
        field = service.getClass().getSuperclass().getDeclaredField("ucErrorEventsCount");
        field.setAccessible(true);
        field.set(service, new AtomicInteger(100));


        service.destroy();

        assertTrue(service.stopped.get());
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("isStarted");
        field.setAccessible(true);
        assertFalse(((AtomicBoolean)field.get(service)).get());

    }

    @Test
    public void  actionPerformed() throws IllegalAccessException, NoSuchFieldException {
        Field field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 50);
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 50);
        field = service.getClass().getSuperclass().getDeclaredField("openReqUcMessage");
        field.setAccessible(true);
        field.set(service, new OpenReqUcMessage());


        service.init();
        service.addLogger(new GenericLogger() {
            @Override
            public void info(String s) {

            }

            @Override
            public void error(String s, Throwable throwable, Object... objects) {

            }

            @Override
            public void warning(String s) {

            }
        });
        UcReceiver receiver = (UcReceiver)service.receiver;
        field = receiver.getClass().getSuperclass().getDeclaredField("client");
        field.setAccessible(true);
        field.set(receiver, new TcpClient("127.0.0.1", 8080, 40, 40) {
            @Override
            protected void onRead(ByteBuffer buf) throws Exception {

            }

            @Override
            protected void onConnected() throws Exception {

            }

            @Override
            protected void onDisconnected() throws Exception {

            }

            @Override
            protected void onFailure(Exception e) throws Exception {

            }
        });
        field = service.getClass().getSuperclass().getDeclaredField("ucErrorEventsCount");
        field.setAccessible(true);
        field.set(service, new AtomicInteger(100));
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("acdErrorHandler");
        field.setAccessible(true);
        field.set(service, new ACDErrorHandler() {
            @Override
            public void generalError(GeneralError error) {

            }

            @Override
            public void connectionError(ConnectionError error) {

            }
        });

        service.actionPerformed(new ActionEvent(this, 1, "disconnected"));
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("isStarted");
        field.setAccessible(true);
        assertFalse(((AtomicBoolean)field.get(service)).get());
        field.set(service, new AtomicBoolean(true) );
        assertTrue(((AtomicBoolean)field.get(service)).get());
        service.actionPerformed(new ActionEvent(this, 1, "failure"));
        assertFalse(((AtomicBoolean)field.get(service)).get());
    }

    @Test
    public void  eventsProcessorPoolCore() throws IllegalAccessException, NoSuchFieldException {
        Field field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 50);
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 50);
        field = service.getClass().getSuperclass().getDeclaredField("openReqUcMessage");
        field.setAccessible(true);
        field.set(service, new OpenReqUcMessage());


        service.init();
        service.addLogger(new GenericLogger() {
            @Override
            public void info(String s) {

            }

            @Override
            public void error(String s, Throwable throwable, Object... objects) {

            }

            @Override
            public void warning(String s) {

            }
        });
        UcReceiver receiver = (UcReceiver)service.receiver;
        field = receiver.getClass().getSuperclass().getDeclaredField("client");
        field.setAccessible(true);
        field.set(receiver, new TcpClient("127.0.0.1", 8080, 40, 40) {
            @Override
            protected void onRead(ByteBuffer buf) throws Exception {

            }

            @Override
            protected void onConnected() throws Exception {

            }

            @Override
            protected void onDisconnected() throws Exception {

            }

            @Override
            protected void onFailure(Exception e) throws Exception {

            }
        });

        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("acdErrorHandler");
        field.setAccessible(true);
        field.set(service,new ACDErrorHandler() {
            @Override
            public void generalError(GeneralError error) {

            }

            @Override
            public void connectionError(ConnectionError error) {

            }
        });
        field = service.getClass().getSuperclass().getDeclaredField("ucErrorEventsCount");
        field.setAccessible(true);
        field.set(service, new AtomicInteger(1));
        FailureEventUcMessage failureEventUcMessage = new FailureEventUcMessage();
        service.eventsProcessorPoolCore(failureEventUcMessage);

        assertEquals(((AtomicInteger)field.get(service)).get(), 2);

        FailureConfUcMessage failureconfUccxMessage = new FailureConfUcMessage();
        service.eventsProcessorPoolCore(failureconfUccxMessage);

        assertEquals(((AtomicInteger)field.get(service)).get(), 3);

        OpenConfUcMessage openConfUcMessage = new OpenConfUcMessage();
        service.eventsProcessorPoolCore(openConfUcMessage);

        assertEquals(((AtomicInteger)field.get(service)).get(), 0);

        HeartbeatConfUcMessage heartbeatConfUcMessage = new HeartbeatConfUcMessage();
        heartbeatConfUcMessage.fixedFields.put(ConstantsUc.UcMessageFields.InvokeId, 1);
        service.eventsProcessorPoolCore(heartbeatConfUcMessage);

        assertEquals(((AtomicInteger)field.get(service)).get(), 0);

        CloseConfUcMessage closeConfUcMessage = new CloseConfUcMessage();
        service.eventsProcessorPoolCore(closeConfUcMessage);

        assertEquals(((AtomicInteger)field.get(service)).get(), 0);


    }


}