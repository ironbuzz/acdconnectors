package com.nice.saas.wfo.acd.cisco.uc;

import com.nice.saas.wfo.acd.cisco.AcdReportService;
import com.nice.saas.wfo.acd.cisco.uc.reports.AcdUcceAdherenceReportService;

import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import org.junit.Test;
import org.mockito.Mockito;

import java.lang.reflect.Field;
import java.sql.*;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.assertEquals;

public class AcdReportServiceTest {
    AcdReportService service = new AcdUcceAdherenceReportService() ;
    @Test
    public void  getReportData() throws SQLException, NoSuchFieldException, IllegalAccessException {
        ResultSet resultSetMock = Mockito.mock(ResultSet.class);
        Mockito.when(resultSetMock.getMetaData()).thenReturn(new ResultSetMetaData() {
            @Override
            public int getColumnCount() throws SQLException {
                return 2;
            }

            @Override
            public boolean isAutoIncrement(int column) throws SQLException {
                return false;
            }

            @Override
            public boolean isCaseSensitive(int column) throws SQLException {
                return false;
            }

            @Override
            public boolean isSearchable(int column) throws SQLException {
                return false;
            }

            @Override
            public boolean isCurrency(int column) throws SQLException {
                return false;
            }

            @Override
            public int isNullable(int column) throws SQLException {
                return 0;
            }

            @Override
            public boolean isSigned(int column) throws SQLException {
                return false;
            }

            @Override
            public int getColumnDisplaySize(int column) throws SQLException {
                return 0;
            }

            @Override
            public String getColumnLabel(int column) throws SQLException {
                if (column == 1) {
                    return "Name";
                } else {
                    return "SecondName";
                }
            }

            @Override
            public String getColumnName(int column) throws SQLException {
                if (column == 1) {
                    return "Name";
                } else {
                    return "SecondName";
                }
            }

            @Override
            public String getSchemaName(int column) throws SQLException {
                return null;
            }

            @Override
            public int getPrecision(int column) throws SQLException {
                return 0;
            }

            @Override
            public int getScale(int column) throws SQLException {
                return 0;
            }

            @Override
            public String getTableName(int column) throws SQLException {
                return null;
            }

            @Override
            public String getCatalogName(int column) throws SQLException {
                return null;
            }

            @Override
            public int getColumnType(int column) throws SQLException {
                return 0;
            }

            @Override
            public String getColumnTypeName(int column) throws SQLException {
                return null;
            }

            @Override
            public boolean isReadOnly(int column) throws SQLException {
                return false;
            }

            @Override
            public boolean isWritable(int column) throws SQLException {
                return false;
            }

            @Override
            public boolean isDefinitelyWritable(int column) throws SQLException {
                return false;
            }

            @Override
            public String getColumnClassName(int column) throws SQLException {
                return null;
            }

            @Override
            public <T> T unwrap(Class<T> iface) throws SQLException {
                return null;
            }

            @Override
            public boolean isWrapperFor(Class<?> iface) throws SQLException {
                return false;
            }
        });
        Mockito.when(resultSetMock.getString(1)).thenReturn("Vasya");
        Mockito.when(resultSetMock.getString(2)).thenReturn("Ivanov");
        Mockito.when(resultSetMock.next()).thenReturn(true).thenReturn(false);

        Statement statement = Mockito.mock(Statement.class);
        PreparedStatement pstatement = Mockito.mock(PreparedStatement.class);
        String query = "SELECT name FROM tables";
        Mockito.when(statement.executeQuery(query)).thenReturn(resultSetMock);
        Mockito.when(pstatement.executeQuery()).thenReturn(resultSetMock);

        Connection jdbcConnection = Mockito.mock(Connection.class);
        Mockito.when(jdbcConnection.createStatement()).thenReturn(statement);
        Mockito.when(jdbcConnection.prepareStatement(query)).thenReturn(pstatement);

        Field field = service.getClass().getSuperclass().getDeclaredField("connection");
        field.setAccessible(true);
        field.set(service, jdbcConnection);
        field = service.getClass().getSuperclass().getDeclaredField("query");
        field.setAccessible(true);
        field.set(service, query);

        service.addLogger(new GenericLogger() {
            @Override
            public void info(String s) {

            }

            @Override
            public void error(String s, Throwable throwable, Object... objects) {

            }

            @Override
            public void warning(String s) {

            }
        });
        String csv = service.getReportData(Instant.now().minus(1, ChronoUnit.HOURS), Instant.now());
        String[] splitted = csv.split("@dt@");
        assertEquals(splitted.length, 5);
        String br = System.lineSeparator();
        assertEquals(splitted[0], "Name|SecondName"+br+"Vasya|Ivanov"+br);
        assertEquals(splitted[3], "60");
        assertEquals(splitted[4], "UTC");

    }

}