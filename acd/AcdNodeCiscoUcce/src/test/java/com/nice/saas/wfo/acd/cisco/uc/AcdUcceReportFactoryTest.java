package com.nice.saas.wfo.acd.cisco.uc;

import com.nice.saas.wfo.acd.cisco.common.Constants;
import com.nice.saas.wfo.acd.cisco.uc.reports.*;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import com.nice.saas.wfo.comp.manager.asc.generic.ReportType;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class AcdUcceReportFactoryTest {
    AcdUcceReportFactory f = new AcdUcceReportFactory();
    @Test
    public void addLogger() throws Exception {
        f.addLogger(new GenericLogger() {
            @Override
            public void info(String s) {

            }

            @Override
            public void error(String s, Throwable throwable, Object... objects) {

            }

            @Override
            public void warning(String s) {

            }
        });
        Field field = f.getClass().getDeclaredField("logger");
        field.setAccessible(true);
        assertNotNull(field.get(f));
    }


    @Test
    public void getReportService() throws Exception {
        f.acdUcceAdherenceReportService = new AcdUcceAdherenceReportService();
        f.acdUcceHistoricalDataReportService = new AcdUcceHistoricalDataReportService();
        assertTrue(f.getReportService(ReportType.Adherence) instanceof AcdUcceAdherenceReportService);
        assertTrue(f.getReportService(ReportType.Forecast) instanceof AcdUcceHistoricalDataReportService);
    }

    @Test
    public void getAcdType() throws Exception {
        assertEquals(f.getAcdType(), Constants.AcdCiscoUcceReportType);
    }
}