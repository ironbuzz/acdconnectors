package com.nice.saas.wfo.acd.cisco.uc.common;


import com.nice.saas.wfo.acd.cisco.common.Constants;
import com.nice.saas.wfo.acd.cisco.uc.messages.AgentStateChangeUcMessage;
import com.nice.saas.wfo.acd.cisco.uc.messages.AgentStateChangeUcceMessage;
import com.nice.saas.wfo.acd.rta.GenericACDEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AcdEventUcceTranslatorTest {
    AcdEventUcTranslator t = new AcdEventUcceTranslator();
    @Before
    public void  setUp() {
    }

    @After
    public void  tearDown() {
    }

    @Test
    public void  translate() {
        AgentStateChangeUcMessage agentStateChangeUccxMessage = new AgentStateChangeUcceMessage();
        agentStateChangeUccxMessage.floatingFields.put(ConstantsUc.UcMessageFields.AgentId, "1");
        agentStateChangeUccxMessage.fixedFields.put(ConstantsUc.UcMessageFields.EventReasonCode, "2");
        agentStateChangeUccxMessage.fixedFields.put(ConstantsUc.UcMessageFields.AgentState, "3");

        GenericACDEvent event =  t.translate(agentStateChangeUccxMessage);
        assertEquals(event.getAcdId(), Constants.AcdCiscoUcceReportType );
        assertEquals(event.getReasonCode(), "2" );
        assertEquals(event.getEventCode(), "3" );
        assertEquals(event.getLoginId(), "1" );
    }

}