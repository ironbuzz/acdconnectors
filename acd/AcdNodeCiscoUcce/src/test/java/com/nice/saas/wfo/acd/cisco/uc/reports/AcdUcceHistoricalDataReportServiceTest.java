package com.nice.saas.wfo.acd.cisco.uc.reports;

import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import org.junit.Test;


import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import static org.junit.Assert.*;


public class AcdUcceHistoricalDataReportServiceTest {
    AcdUcceHistoricalDataReportService service = new AcdUcceHistoricalDataReportService();
    @Test
    public void setQuery() throws NoSuchFieldException, IllegalAccessException {
        service.setQuery("query");
        Field field = service.getClass().getSuperclass().getDeclaredField("query");
        field.setAccessible(true);
        assertEquals(field.get(service), "query");
    }

    @Test
    public void addLogger() throws NoSuchFieldException, IllegalAccessException {
        service.addLogger(new GenericLogger() {
            @Override
            public void info(String s) {

            }

            @Override
            public void error(String s, Throwable throwable, Object... objects) {

            }

            @Override
            public void warning(String s) {

            }
        });
        Field field = service.getClass().getSuperclass().getDeclaredField("logger");
        field.setAccessible(true);
        assertNotNull(field.get(service));
    }





    @Test
    public void getAcdType() {

        assertEquals(service.getAcdType(), "CISCO_UCCX");
    }
}