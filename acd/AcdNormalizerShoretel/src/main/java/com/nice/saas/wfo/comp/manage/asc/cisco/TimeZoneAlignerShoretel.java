package com.nice.saas.wfo.comp.manage.asc.cisco;

import com.nice.saas.wfo.comp.manage.asc.cisco.TimeZoneAligner;

public class TimeZoneAlignerShoretel extends TimeZoneAligner{
    @Override
    public String align(String s) {
        if (s == null || s.length() == 0) {
            return "+00:00";
        }
        Integer value = Integer.parseInt(s.trim());
        boolean isNegative = false;
        if (value < 0) {
            isNegative = true;
        }
        String ret = String.valueOf(Math.abs(value)/60);
        if (value == 0) {
            return "+00:00";
        }
        int len = ret.length();
        StringBuilder sb = new StringBuilder(ret);
        if (len == 1) {
            sb.insert(0, "0");
        }
        if (isNegative) {
            sb.insert(0, "-");
        } else {
            sb.insert(0, "+");
        }
        sb.append(":00");
        return sb.toString();
    }
}
