package com.nice.saas.wfo.comp.manage.asc.cisco;


import org.springframework.stereotype.Component;

@Component
public class NormalizerFactoryShoretel extends NormalizerFactoryBase {

    public NormalizerFactoryShoretel() {
        acdType = "SHORETEL";
        timeZoneAligner = new TimeZoneAlignerShoretel();
        super.init();
    }

}
