package com.ironbuzz;
import java.io.*;
import java.net.*;
import java.nio.*;
import java.nio.charset.Charset;

public class Main {

    public static void main(String[] args) throws IOException {
        String clientSentence;
        String capitalizedSentence;
        ServerSocket welcomeSocket = new ServerSocket(6790);
        Socket connectionSocket = welcomeSocket.accept();
        System.out.println("Accepted " + connectionSocket.toString());
        BufferedReader inFromClient =
                new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
        DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
        while (true) {
            try {
//                System.out.println("Waiting for message from client");
//                inFromClient.read();
//                System.out.println("Received message from client");
                //capitalizedSentence = clientSentence.toUpperCase() + '\n';
                int length = 73;
                ByteBuffer buffer = ByteBuffer.allocateDirect(length);
                buffer.putInt(length - 8); // body length
                buffer.putInt(30); // messageTypeId

                // reserved fields
                buffer.putInt(0);
                buffer.putInt(1);
                buffer.putInt(0);
                buffer.putShort((short) 21);

                buffer.putShort((short) 1); //CSQState
                buffer.putInt(21); //StateDuration
                buffer.putInt(22); //CSQID

                // reserved fields
                buffer.putInt(23);
                buffer.putShort((short) 24);

                buffer.putShort((short) 3332); // agent state
                buffer.putShort((short) 3334); // event reason code

                // reserved fields
                buffer.putInt(25);
                buffer.putInt(26);
                buffer.putShort((short) 27);
                buffer.putInt(28);
                buffer.putInt(29);
                buffer.putInt(30);
                buffer.putShort((short) 31); //numCSQs

                // floating part
                // agentId code
                buffer.put((byte) 0xc2);
                // agentId data length
                buffer.put((byte) 5);
                // agentId data
                String agentId = "Misha";
                byte[] arr = agentId.getBytes(Charset.forName("UTF-8"));
                buffer.put(arr);   //TODO: check which encoding is needed))


                buffer.rewind();
                byte[] tempArray = new byte[length];
                buffer.get(tempArray, 0, length);
                outToClient.write(tempArray);
                Thread.sleep(5);

            }
            catch (Exception e){
                System.out.println(String.format("Error: ", e));
            }
        }
    }
}



