package com.nice.saas.wfo.acd.shoretel.messages;

import com.google.gson.annotations.Expose;
import com.nice.saas.wfo.acd.cisco.common.Constants;
import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import com.nice.saas.wfo.acd.shoretel.ConstantsShoretel;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.ByteBuffer;

/**
 * UnsubscribeEventsReqShoretelMessage
 */
@Component
public class UnsubscribeEventsReqShoretelMessage extends ShoretelMessageReqRes {
    /**
     * Constant
     */
    @Expose
    public String request = "Unsubscribe";
    /**
     * Event types to unsucscribe from
     */
    @Expose
    private String[] subscribe;

    public String[] getSubscribe() {
        return subscribe;
    }

    @Value("${cisco.uccx.acd.rta.unsubscribeEventsReqShoretelMessage.subscribe}")
    public void setSubscribe(String[] subscribe) {
        this.subscribe = subscribe;
    }

    /**
     * Constructor
     */
    public UnsubscribeEventsReqShoretelMessage() {
        message = ConstantsShoretel.ShoretelUnsubscribeMessage;
    }

    /**
     * Deserialize is not supported
     * @param byteBuffer
     * @return
     */
    @Override
    public Message deserialize(ByteBuffer byteBuffer) {
        throw new UnsupportedOperationException();
    }
}