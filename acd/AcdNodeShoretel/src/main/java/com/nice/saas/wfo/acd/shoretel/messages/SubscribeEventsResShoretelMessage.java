package com.nice.saas.wfo.acd.shoretel.messages;
import com.google.gson.annotations.*;

import java.nio.ByteBuffer;

/**
 * SubscribeEventsResShoretelMessage - sent by server in response to subscrine request
 */
public class SubscribeEventsResShoretelMessage extends ShoretelMessageReqRes {

    /**
     * timestamp
     */
    @Expose
    public String timestamp;

    /**
     * Array containing types of messages which this client subscribed for
     */
    @Expose
    public String[] subscribe;

    /**
     * Result code
     */
    @Expose
    public int result;

    /**
     * Additional info
     */
    @Expose
    @SerializedName("additional-info")
    public String additionalInfo;

    /**
     * Error message
     */
    @Expose
    @SerializedName("error-msg")
    public String errorMsg;

    /**
     * Serialize is not supported
     * @return
     */
    @Override
    public ByteBuffer serialize() {
        throw new UnsupportedOperationException();
    }
}
