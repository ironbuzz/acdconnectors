package com.nice.saas.wfo.acd.shoretel.messages;
import com.google.gson.annotations.*;

import java.nio.ByteBuffer;

/**
 * Base class for every Shoretel agent event message
 */
public abstract class ShoretelMessageEv extends ShoretelMessage {
    /**
     * Timestamp
     */
    @Expose
    public String timestamp;

    /**
     * Server time in GMT
     */
    @Expose
    public String gmt;

    /**
     * ShoreTel Connect Contact Center application that agent is logging in to.
     */
    @Expose
    @SerializedName("sub-topic")
    public String subTopic;

    /**
     * Agent ID
     */
    @Expose
    @SerializedName("agent-id")
    public String agentId;

    /**
     * State's cause
     */
    @Expose
    public int cause;

    /**
     * Serialize is not supported
     * @return
     */
    @Override
    public ByteBuffer serialize() {
        throw new UnsupportedOperationException();
    }
}
