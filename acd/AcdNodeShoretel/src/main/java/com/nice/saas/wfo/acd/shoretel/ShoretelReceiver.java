package com.nice.saas.wfo.acd.shoretel;

//import com.nice.saas.wfo.acd.cisco.common.MyLogger;
import com.nice.saas.wfo.acd.cisco.common.TcpReceiverBase;
import com.nice.saas.wfo.comp.manager.asc.generic.GeneralError;
import org.springframework.stereotype.Component;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Shoretel receiver listens on TCP socket and translates received byte data to Shoretel raw (byte buffer) messages
 */
@Component(value = "ShoretelReceiver")
public class ShoretelReceiver extends TcpReceiverBase {
    // region constants
    private final String className = "ShoretelReceiver";

    // endregion


    /**
     * Listen on TCP socket and translates received byte data to Shoretel raw (byte buffer) messages
     * @return
     */
    @Override
    public List<ByteBuffer> receive() {
        final String functionName = "receive()";
        List<ByteBuffer> retArray = new ArrayList<>();
        try {
            if (buffer == null) { // buffer is used to accumulate currently received data, if it's null,
                // take new data from buffers queue or wait on this (blocking) queue for new data
                buffer = buffers.take();
            } else {
                ByteBuffer nextBuffer = buffers.take(); // if buffer is not null, take new buffer and concatenate 2 buffers
                buffer = concatByteBuffers(buffer, nextBuffer, buffer.remaining(), nextBuffer.remaining());
            }
            // split currently received buffer with '\0' delimiter (that divides Shoretel messages)
            String stringBuf = StandardCharsets.UTF_8.decode(buffer).toString();
            String[] splitted = stringBuf.split("\0", -1);
            if (splitted == null || splitted.length == 0) {
                return null; // if '\0' delimiter is not found, it means that the end of Shoretel message is not reached, so return and take the next buffer
            }
            for (int i =0; i < splitted.length; i++) {
                String item = splitted[i];
                int itemLength = item.length();
                if (itemLength > 0) {
                    if (((i+1)  <= (splitted.length -1)) || ((splitted.length > i+1) && splitted[i +1].length() == 0)) {
                        ByteBuffer newBuffer = StandardCharsets.UTF_8.encode(item);  // got Shoretel raw message, add it to return array
                        retArray.add(newBuffer);
                    } else {
                        buffer = StandardCharsets.UTF_8.encode(item); // Shoretel message is not finished, need to take next byte buffer and concatenate
                    }
                } else if (i == splitted.length -1) {
                    buffer = null; // final item is reached, set buffer to null and continue receiving data
                }
            }
        }
        catch (Exception e) {
            throw new GeneralError(String.format("%s.%s: Exception: ", className, functionName), e);
        }

        return retArray;
    }


}
