package com.nice.saas.wfo.acd.shoretel.messages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * AgentOnCallEvent message, indicates that agent is on call
 */
public class AgentOnCallEvent extends ShoretelMessageEv {

    /**
     * Call type
     */
    @Expose
    @SerializedName("call-type")
    public int callType;

    /**
     * Call ID
     */
    @Expose
    @SerializedName("call-id")
    public int callId;

    /**
     * PBX call ID
     */
    @Expose
    @SerializedName("pbx-call-id")
    public int pbxCallId;

    /**
     * GUID
     */
    @Expose
    public String guid;
}
