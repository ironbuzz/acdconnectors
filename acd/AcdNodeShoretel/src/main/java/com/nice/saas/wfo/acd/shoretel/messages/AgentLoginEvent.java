package com.nice.saas.wfo.acd.shoretel.messages;
import com.google.gson.annotations.*;

/**
 * AgentLoginEvent message, indicates that agent logged in
 */
public class AgentLoginEvent extends ShoretelMessageEv {

    /**
     * Agent number
     */
    @Expose
    @SerializedName("agent-number")
    public String agentNumber;

    /**
     * Agent's extension
     */
    @Expose
    @SerializedName("agent-ext")
    public String agentExt;

    /**
     * Agent groups
     */
    @Expose
    public String[] groups;

}
