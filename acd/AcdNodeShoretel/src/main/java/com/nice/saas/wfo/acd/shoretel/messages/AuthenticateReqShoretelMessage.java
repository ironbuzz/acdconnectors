package com.nice.saas.wfo.acd.shoretel.messages;

import com.google.gson.annotations.*;
import com.nice.saas.wfo.acd.cisco.common.Constants;
import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import com.nice.saas.wfo.acd.shoretel.ConstantsShoretel;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.ByteBuffer;


/**
 * Shoretel authenticate request message (should be sent to server to authenticate the client and start the communication)
 */
@Component
public class AuthenticateReqShoretelMessage extends ShoretelMessageReqRes {


    /**
     * User name
     */
    @Expose
    private String user;
    /**
     * User password
     */
    @Expose
    private String password;

    /**
     * Get user name
     * @return
     */
    public String getUser() {
        return user;
    }

    /**
     * Set user name
     * @param user
     */
    @Value("${cisco.uccx.acd.rta.authenticateReqShoretelMessage.user}")
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * Get user password
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set user password
     * @param password
     */
    @Value("${cisco.uccx.acd.rta.authenticateReqShoretelMessage.password}")
    public void setPassword(String password) {
        this.password = password;
    }


    /**
     * Constructor
     */
    public AuthenticateReqShoretelMessage() {
        message = ConstantsShoretel.ShoretelAuthenticateMessage;
    }

    /**
     * Deserialize is not supported
     * @param byteBuffer
     * @return
     */
    @Override
    public Message deserialize(ByteBuffer byteBuffer) {
        throw new UnsupportedOperationException();
    }

}
