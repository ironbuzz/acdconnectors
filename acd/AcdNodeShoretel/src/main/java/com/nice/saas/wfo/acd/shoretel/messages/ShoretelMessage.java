package com.nice.saas.wfo.acd.shoretel.messages;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.nice.saas.wfo.acd.cisco.common.Constants;
import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import com.nice.saas.wfo.acd.shoretel.ConstantsShoretel;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;


/**
 * Base class for every Shoretel message
 */
public abstract class ShoretelMessage implements Message {

    /**
     * Client application version
     */
    @Expose
    private int version;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * Sequential number of the message, unsigned long type of value between 0 and 4,294,967,295
     */
    @Expose
    protected long sequence;

    public long getSequence() {
        return sequence;
    }

    public void setSequence(long sequence) {
        this.sequence = sequence;
    }

    /**
     * Should be set to "contact-center"
     */
    @Expose
    public String topic = ConstantsShoretel.DefaultShoretelTopic;

    /**
     * Indicates message type
     */
    @Expose
    public String message;

    /**
     * Seserialize message from byte buffer
     * @param byteBuffer
     * @return
     */
    @Override
    public Message deserialize(ByteBuffer byteBuffer) {
        int length = byteBuffer.remaining() -1; // length -1 because we don't need to deserialize termination NULL character
        byte[] tempArray = new byte[length];
        byteBuffer.get(tempArray, 0, length);
        String jsonString = new String(tempArray, Charset.forName("UTF-8"));   //TODO: check which encoding is needed
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        return gson.fromJson(jsonString, this.getClass());
    }

    /**
     * Serialize message to byte buffer
     * @return
     */
    @Override
    public ByteBuffer serialize() {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String jsonString = gson.toJson(this);
        byte[] tempArray = jsonString.getBytes(Charset.forName("UTF-8")); //TODO: check which encoding is needed
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(tempArray.length + 1); // length + 1 because we are adding NULL  character at the end of every message
        byteBuffer.put(tempArray);
        byteBuffer.put((byte) '\0');
        byteBuffer.rewind();
        return byteBuffer;
    }

}
