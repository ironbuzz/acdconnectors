package com.nice.saas.wfo.acd.shoretel.messages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * AgentReleasedEvent message, indicates that agent is released
 */
public class AgentReleasedEvent extends ShoretelMessageEv {

    /**
     * Release code
     */
    @Expose
    @SerializedName("release-code")
    public int releaseCode;

}
