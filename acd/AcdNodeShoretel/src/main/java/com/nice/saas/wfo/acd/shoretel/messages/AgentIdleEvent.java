package com.nice.saas.wfo.acd.shoretel.messages;

/**
 * AgentIdleEvent message, indicates that agent's state is idle
 */
public class AgentIdleEvent extends ShoretelMessageEv {

}
