package com.nice.saas.wfo.acd.shoretel.messages;

import com.google.gson.annotations.Expose;

import java.nio.ByteBuffer;

/**
 * UnsubscribeEventsResShoretelMessage - sent by server in response to unsubscrine request
 */
public class UnsubscribeEventsResShoretelMessage extends ShoretelMessageReqRes {
    /**
     * timestamp
     */
    @Expose
    public String timestamp;

    /**
     * Event types to unsucscribe from
     */
    @Expose
    public String[] subscribe;

    /**
     * Result code
     */
    @Expose
    public int result;

    /**
     * Serialize is not supported
     * @return
     */
    @Override
    public ByteBuffer serialize() {
        throw new UnsupportedOperationException();
    }
}
