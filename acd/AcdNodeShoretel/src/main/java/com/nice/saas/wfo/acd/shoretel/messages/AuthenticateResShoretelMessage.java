package com.nice.saas.wfo.acd.shoretel.messages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.nio.ByteBuffer;

/**
 * Shoretel authenticate response message (should be by server as an answer to authenticate request)
 */
public class AuthenticateResShoretelMessage extends ShoretelMessageReqRes {

    /**
     * Result code
     */
    @Expose
    public int result;
    /**
     * Error message
     */
    @SerializedName("error-msg")
    public String errorMsg;

    /**
     * Serialize is not supported
     * @return
     */
    @Override
    public ByteBuffer serialize() {
        throw new UnsupportedOperationException();
    }
}
