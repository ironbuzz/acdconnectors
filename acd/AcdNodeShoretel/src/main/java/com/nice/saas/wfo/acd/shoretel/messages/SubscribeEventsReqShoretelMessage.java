package com.nice.saas.wfo.acd.shoretel.messages;

import com.google.gson.annotations.*;
import com.nice.saas.wfo.acd.shoretel.ConstantsShoretel;
import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.ByteBuffer;

/**
 * SubscribeEventsReqShoretelMessage - subscribe to events (should be sent after authenticate message)
 */
@Component
public class SubscribeEventsReqShoretelMessage extends ShoretelMessageReqRes {

    @Expose
    private String[] subscribe;

    public String[] getSubscribe() {
        return subscribe;
    }

    @Value("${cisco.uccx.acd.rta.subscribeEventsReqShoretelMessage.subscribe}")
    public void setSubscribe(String[] subscribe) {
        this.subscribe = subscribe;
    }

    /**
     * Constructor
     */
    public SubscribeEventsReqShoretelMessage() {
        message = ConstantsShoretel.ShoretelSubscribeMessage;
    }

    /**
     * Deserialize is not supported
     * @param byteBuffer
     * @return
     */
    @Override
    public Message deserialize(ByteBuffer byteBuffer) {
        throw new UnsupportedOperationException();
    }

}
