package com.nice.saas.wfo.acd.shoretel.reports;

import com.nice.saas.wfo.acd.cisco.AcdReportService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * AcdShoretelHistoricalDataReportService - shoretel historical data report service
 */
@Component
public class AcdShoretelHistoricalDataReportService extends AcdReportService {

    @Override
    @Value("${cisco.uccx.acd.reports.acdShoretelHistoricalDataReportService.query}")
    protected void setQuery(String query) {
        super.setQuery(query);
    }
}
