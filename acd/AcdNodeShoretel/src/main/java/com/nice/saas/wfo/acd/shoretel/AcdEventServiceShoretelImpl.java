package com.nice.saas.wfo.acd.shoretel;//package com.nice.saas.wfo.acd.cisco;

import com.nice.saas.wfo.acd.cisco.AcdEventServiceBase;
import com.nice.saas.wfo.acd.cisco.interfaces.GenericAcdEventTranslator;
import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import com.nice.saas.wfo.acd.cisco.interfaces.MessageParser;
import com.nice.saas.wfo.acd.cisco.interfaces.Receiver;
import com.nice.saas.wfo.acd.shoretel.messages.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.nio.ByteBuffer;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class AcdEventServiceShoretelImpl extends AcdEventServiceBase {

    // region private fields

    private final String className = "AcdEventServiceShoretelImpl";
    private AtomicBoolean gotAuthenticateResShoretelMessage = new AtomicBoolean(false);
    private AtomicBoolean gotSubscribeResShoretelMessage = new AtomicBoolean(false);

    // endregion private fields

    // region autowired fields

    @Autowired
    @Qualifier("ShoretelReceiver")
    private void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    @Autowired
    @Qualifier("ShoretelMessageParser")
    private void setMessageParser (MessageParser messageParser) {
        this.messageParser = messageParser;
    }


    @Autowired
    @Qualifier("AcdEventShoretelTranslator")
    private void setAcdMessageTranslator (GenericAcdEventTranslator acdMessageTranslator) {
        this.acdMessageTranslator = acdMessageTranslator;
    }

    @Autowired
    private AuthenticateReqShoretelMessage authenticateReqShoretelMessage;

    @Autowired
    private SubscribeEventsReqShoretelMessage subscribeEventsReqShoretelMessage;

    @Autowired
    private UnsubscribeEventsReqShoretelMessage unsubscribeEventsReqShoretelMessage;

    @Autowired
    @Value("${shoretelEventService.shoretelIdleTimeout}")
    private int shoretelIdleTimeout;

    // endregion autowired fields

    // region post construct

    @PostConstruct
    public void init() {
        super.init();
    }

    // endregion post construct

    // region base class overrides

    @Override
    protected void senderPoolCore() {
        final String functionName = "senderPoolCore()";
        try {
            // send AuthenticateReq message ti initialize shoretel communications
            initShoretelCommunication();

            // send AuthenticateReq message ti initialize shoretel communications
            subscribeShoretelEvents();

            // loop while stopped
            while (!stopped.get()) {
                try {
                        final Future<?> handle = timer.schedule(() -> {

                    }, shoretelIdleTimeout, TimeUnit.SECONDS);
                    try {
                        handle.get(shoretelIdleTimeout, TimeUnit.SECONDS);
                    }
                    catch (TimeoutException te) {}
                }
                catch (Exception e) {
                    logger.error(String.format("%s.%s: Exception: ", className, functionName), e);
                }
            }

        } catch (Exception e) {
            logger.error(String.format("%s.%s: Exception: ", className, functionName), e);
        }
    }

    @Override
    protected void eventsProcessorPoolCore(Message message) {
        final String functionName = "eventsProcessorPoolCore()";
        // got some message different from ACD event, process it
        if (message instanceof AuthenticateResShoretelMessage) {
            AuthenticateResShoretelMessage authenticateResShoretelMessage = (AuthenticateResShoretelMessage) message;
            if (authenticateResShoretelMessage.result == 0) {
                gotAuthenticateResShoretelMessage.set(true);
                logger.info(String.format("%s.%s: AuthenticateResShoretelMessage received.",
                        className, functionName));
            } else {
                logger.info(String.format("%s.%s: AuthenticateResShoretelMessage received with failure: %s",
                        className, functionName, authenticateResShoretelMessage.errorMsg));
            }
        } else if (message instanceof SubscribeEventsResShoretelMessage) {
            SubscribeEventsResShoretelMessage subscribeEventsResShoretelMessage = (SubscribeEventsResShoretelMessage) message;
            if (subscribeEventsResShoretelMessage.result == 0) {
                logger.info(String.format("%s.%s: SubscribeEventsResShoretelMessage received. Subscribed to events: %s",
                        className, functionName, subscribeEventsResShoretelMessage.subscribe));
            } else {
                logger.info(String.format("%s.%s: SubscribeEventsResShoretelMessage received with failure: %s",
                        className, functionName, subscribeEventsResShoretelMessage.errorMsg));
            }
        } else if (message instanceof UnsubscribeEventsResShoretelMessage) {
            UnsubscribeEventsResShoretelMessage unsubscribeEventsResShoretelMessage = (UnsubscribeEventsResShoretelMessage) message;
            if (unsubscribeEventsResShoretelMessage.result == 0) {
                logger.info(String.format("%s.%s: UnsubscribeEventsResShoretelMessage received.",
                        className, functionName));
            } else {
                logger.info(String.format("%s.%s: UnsubscribeEventsResShoretelMessage received with failure.",
                        className, functionName));
            }
        }
    }

    /**
     * Destroy implementation
     */
    @Override
    public void destroy() {
        // close shoretel communication
        ByteBuffer unsubscribeEventsReqShoretelMessageBuffer = unsubscribeEventsReqShoretelMessage.serialize();
        if (unsubscribeEventsReqShoretelMessage != null) {

            logger.info(String.format("%s.%s: Sending Authenticate message", className, "destroy()"));
            receiver.send(unsubscribeEventsReqShoretelMessageBuffer);
        }
        super.destroy();
    }

    // endregion base class overrides

    // region private methods

    private void initShoretelCommunication() {
        final String functionName = "initShoretelCommunication()";

        while (!gotAuthenticateResShoretelMessage.get() && (!stopped.get())) {
            try {

                ByteBuffer e = authenticateReqShoretelMessage.serialize();
                if (e == null) {
                    continue;
                }
                logger.info(String.format("%s.%s: Sending Authenticate message", className, functionName));
                receiver.send(e);


                final Future<?> handle = timer.schedule(() -> {
                    if (!gotAuthenticateResShoretelMessage.get()) {
                        logger.warning(String.format("%s.%s: Haven't received AuthenticateRes during %s s, resending", className, functionName, shoretelIdleTimeout));
                    }
                }, shoretelIdleTimeout, TimeUnit.SECONDS);
                try {
                    handle.get(shoretelIdleTimeout, TimeUnit.SECONDS);
                }
                catch (TimeoutException te) {}
            }
            catch (Exception e) {
                logger.error(String.format("%s.%s: Exception: ", className, functionName), e);
            }
        }
    }

    private void subscribeShoretelEvents() {
        final String functionName = "subscribeShoretelEvents()";

        while (!gotSubscribeResShoretelMessage.get() && (!stopped.get())) {
            try {

                ByteBuffer subscribeEventsReqShoretelMessageBuffer = subscribeEventsReqShoretelMessage.serialize();
                if (subscribeEventsReqShoretelMessageBuffer == null) {
                    continue;
                }
                logger.info(String.format("%s.%s: Sending Subscribe message", className, functionName));
                receiver.send(subscribeEventsReqShoretelMessageBuffer);


                final Future<?> handle = timer.schedule(() -> {
                    if (!gotSubscribeResShoretelMessage.get()) {
                        logger.warning(String.format("%s.%s: Haven't received SubscribeRes during %s s, resending", className, functionName, shoretelIdleTimeout));
                    }
                }, shoretelIdleTimeout, TimeUnit.SECONDS);
                try {
                    handle.get(shoretelIdleTimeout, TimeUnit.SECONDS);
                }
                catch (TimeoutException te) {}
            }
            catch (Exception e) {
                logger.error(String.format("%s.%s: Exception: ", className, functionName), e);
            }
        }
    }

    // endregion private methods
}
