package com.nice.saas.wfo.acd.shoretel;

import com.nice.saas.wfo.acd.cisco.common.Constants;
import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import com.nice.saas.wfo.acd.cisco.interfaces.MessageParser;
import com.nice.saas.wfo.acd.shoretel.messages.*;
import com.nice.saas.wfo.comp.manager.asc.generic.GeneralError;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import org.springframework.stereotype.Component;

import java.nio.ByteBuffer;

//import com.nice.saas.wfo.acd.cisco.common.MyLogger;

/**
 * Shoretel message parser (translates raw byte buffer to Shoretel message)
 */
@Component(value = "ShoretelMessageParser")
public class ShoretelMessageParser implements MessageParser<ByteBuffer> {
    private final String className = "ShoretelMessageParser";

    private GenericLogger logger;

    /**
     * Translate raw byte buffer to Shoretel message
     * @param rawMessage
     * @return
     */
    @Override
    public Message parse(ByteBuffer rawMessage) {
        final String functionName = "parse()";
        ShoretelMessage shoretelMessage;
        ShoretelMessageDiscoverer discovererMessage = new ShoretelMessageDiscoverer();

        try {
            discovererMessage = (ShoretelMessageDiscoverer) discovererMessage.deserialize(rawMessage);

            shoretelMessage = (ShoretelMessage) ConstantsShoretel.ShoretelMessageTypes.get(discovererMessage.message).getConstructor().newInstance();
            rawMessage.rewind();
            shoretelMessage = (ShoretelMessage) shoretelMessage.deserialize(rawMessage);

        } catch (Exception e) {
            throw new GeneralError(String.format("%s.%s: Exception: ", className, functionName), e);
        }
        return shoretelMessage;
    }

    @Override
    public void addLogger(GenericLogger logger) {
        this.logger = logger;
    }
}
