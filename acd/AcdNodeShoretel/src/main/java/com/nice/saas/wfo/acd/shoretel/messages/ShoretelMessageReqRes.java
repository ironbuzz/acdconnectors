package com.nice.saas.wfo.acd.shoretel.messages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Base class for Shoretel messages having request and response
 */
public abstract class ShoretelMessageReqRes extends ShoretelMessage {
    /**
     * Request ID
     */
    @Expose
    @SerializedName("request-id")
    protected int requestId;
    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }
}
