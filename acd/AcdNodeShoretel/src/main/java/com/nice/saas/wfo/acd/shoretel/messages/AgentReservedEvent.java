package com.nice.saas.wfo.acd.shoretel.messages;

import com.google.gson.annotations.Expose;

/**
 * AgentReservedEvent message, indicates that agent is reserved
 */
public class AgentReservedEvent extends ShoretelMessageEv {

    /**
     * Agent groups
     */
    @Expose
    public String[] groups;

    /**
     * GUID
     */
    @Expose
    public String guid;
}
