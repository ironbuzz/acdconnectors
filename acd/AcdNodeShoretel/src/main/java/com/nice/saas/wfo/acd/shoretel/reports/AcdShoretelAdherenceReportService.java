package com.nice.saas.wfo.acd.shoretel.reports;

import com.nice.saas.wfo.acd.cisco.AcdReportService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * AcdShoretelAdherenceReportService - shoretel adherence report service
 */
@Component
public class AcdShoretelAdherenceReportService extends AcdReportService {

    @Override
    @Value("${cisco.uccx.acd.reports.acdShoretelAdherenceReportService.query}")
    protected void setQuery(String query) {
        super.setQuery(query);
    }
}
