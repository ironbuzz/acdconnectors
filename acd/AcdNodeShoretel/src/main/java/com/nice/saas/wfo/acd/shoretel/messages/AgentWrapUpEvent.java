package com.nice.saas.wfo.acd.shoretel.messages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * AgentWrapUpEvent message, indicates that agent is wraped up
 */
public class AgentWrapUpEvent extends ShoretelMessageEv {

    /**
     * Media type
     */
    @Expose
    @SerializedName("media-type")
    public String mediaType;

    /**
     * Call ID
     */
    @Expose
    @SerializedName("call-id")
    public int callId;

    /**
     * TAPI call ID
     */
    @Expose
    @SerializedName("tapi-call-id")
    public int tapiCallId;

    /**
     * GUID
     */
    @Expose
    public String guid;

    /**
     * Wrap up code
     */
    @Expose
    @SerializedName("wrapup-code")
    public String wrapupCode;
}
