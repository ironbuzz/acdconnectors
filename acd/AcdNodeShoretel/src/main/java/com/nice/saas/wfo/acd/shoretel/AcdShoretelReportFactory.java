package com.nice.saas.wfo.acd.shoretel;

import com.nice.saas.wfo.acd.cisco.AcdEventServiceBase;
import com.nice.saas.wfo.acd.cisco.AcdReportService;
import com.nice.saas.wfo.acd.cisco.common.Constants;
import com.nice.saas.wfo.acd.historicaldata.GenericReportFactory;
import com.nice.saas.wfo.acd.historicaldata.GenericReportService;
import com.nice.saas.wfo.acd.shoretel.reports.*;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import com.nice.saas.wfo.comp.manager.asc.generic.ReportType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AcdShoretelReportFactory implements GenericReportFactory {
    private String acdType = "SHORETEL";

    @Autowired
    AcdShoretelAdherenceReportService acdShoretelAdherenceReportService;

    @Autowired
    AcdShoretelHistoricalDataReportService acdShoretelHistoricalDataReportService;

    @Autowired
    AcdEventServiceBase rtaService;


    private GenericLogger logger;

    @Override
    public void addLogger(GenericLogger logger) {
        this.logger = logger;
    }

    @Override
    public GenericReportService getReportService(ReportType reportType) {
        AcdReportService reportService = null;
        if (acdType == null) {
            acdType = Constants.AcdCiscoUccxReportType;
        }

        switch (reportType) {
            case Adherence:
                reportService = acdShoretelAdherenceReportService;
                break;
            case Forecast:
                reportService = acdShoretelHistoricalDataReportService;
                break;
        }



        if(reportService != null){
            reportService.addLogger(logger);
            reportService.setAcdType(acdType);
        }
        return reportService;
    }

    @Override
    public String getAcdType() {
        return acdType;
    }

    @Override
    public void terminate(GenericLogger logger) {
        rtaService.destroy();
    }
}
