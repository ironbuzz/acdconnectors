package com.nice.saas.wfo.acd.shoretel;

//import com.nice.saas.wfo.acd.cisco.common.MyLogger;

import com.nice.saas.wfo.acd.cisco.interfaces.GenericAcdEventTranslator;
import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import com.nice.saas.wfo.acd.shoretel.messages.*;
import com.nice.saas.wfo.acd.rta.GenericACDEvent;
import com.nice.saas.wfo.comp.manager.asc.generic.GeneralError;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import org.springframework.stereotype.Component;

/**
 * ACD - Shoretel translator (translates ACD message to GenericAcdEvent)
 */
@Component(value = "AcdEventShoretelTranslator")
public class AcdEventShoretelTranslator implements GenericAcdEventTranslator {
    private final String className = "AcdEventShoretelTranslator";

    private GenericLogger logger;

    /**
     * Translate ACD message to GenericAcdEvent
     * @param message
     * @return
     */
    @Override
    public GenericACDEvent translate(Message message) {

        final String functionName = "translate()";
        GenericACDEvent event = new GenericACDEvent();
        try {
            if (!(message instanceof ShoretelMessageEv)) {
                return null;
            }
            ShoretelMessageEv agentStateMessage = (ShoretelMessageEv) message;

            event.setEventCode(agentStateMessage.message);
            event.setLoginId(agentStateMessage.agentId);
            event.setEventCode(agentStateMessage.cause + "_" + agentStateMessage.message); //TODO: create some combination od agent state and reason code (rule?)
            //MyLogger.logger.info(String.format("%s.%s: AgentStateChange message received and translated. loginId = %s, reasonCode = %s", className, functionName, event.getLoginId(), event.getReasonCode()));
        } catch (Exception e) {
            throw new GeneralError(String.format("%s.%s: Exception: ", className, functionName), e);
        }
        return event;
    }

    @Override
    public void addLogger(GenericLogger logger) {
        this.logger = logger;
    }
}
