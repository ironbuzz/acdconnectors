package com.nice.saas.wfo.acd.shoretel;


import com.nice.saas.wfo.acd.shoretel.messages.*;

import java.util.HashMap;
import java.util.Map;

public class ConstantsShoretel {

    /**
     * Default application version for communication with shoretel
     */
    public static final int DefaultAppVersion = 1;

    /**
     * Default topic for communication with shoretel
     */
    public static final String DefaultShoretelTopic = "contact-center";

    /**
     * Default request id for communication with shoretel
     */
    public static final int DefaultShoretelRequestId = -1;

    /**
     * shoretel authenticate message identifier
     */
    public static final String ShoretelAuthenticateMessage = "authenticate";

    /**
     * shoretel subscribe message identifier
     */
    public static final String ShoretelSubscribeMessage = "subscribe-events";

    /**
     * shoretel unsubscribe message identifier
     */
    public static final String ShoretelUnsubscribeMessage = "unsubscribe-events";

    /**
     * shoretel AgentIdleEvent message identifier
     */
    public static final String ShoretelAgentIdleEventMessage = "agent-idle";

    /**
     * shoretel AgentLoginEvent message identifier
     */
    public static final String ShoretelAgentLoginEventMessage = "agent-login";

    /**
     * shoretel AgentOnCallEvent message identifier
     */
    public static final String ShoretelAgentOnCallEventMessage = "agent-on-call";

    /**
     * shoretel AgentReleasedEvent message identifier
     */
    public static final String ShoretelAgentReleasedEventMessage = "agent-release";

    /**
     * shoretel AgentReservedEvent message identifier
     */
    public static final String ShoretelAgentReservedEventMessage = "agent-reserved";

    /**
     * shoretel AgentWrapUpEvent message identifier
     */
    public static final String ShoretelAgentWrapUpEventMessage = "agent-wrap";

    /**
     * Field names of fields that are in use in UCCX protocol
     */
    public enum ShoretelMessageFields {
        Version ("version"),
        Sequence ("sequence"),
        Topic ("topic"),
        RequestId ("request-id"),
        Message ("message"),
        User ("user"),
        Password ("password");

        private String value;

        ShoretelMessageFields(String value) {
            this.value = value;
        }
    }


    /**
     * shoretel message type -> shoretel message id map
     */
    public static final Map<Class<?>, String> ShoretelMessageTypeIds = new HashMap<Class<?>, String>()
    {{
        put(AgentIdleEvent.class, ShoretelAgentIdleEventMessage);
        put(AgentLoginEvent.class, ShoretelAgentLoginEventMessage);
        put(AgentOnCallEvent.class, ShoretelAgentOnCallEventMessage);
        put(AgentReleasedEvent.class, ShoretelAgentReleasedEventMessage);
        put(AgentReservedEvent.class, ShoretelAgentReservedEventMessage);
        put(AgentWrapUpEvent.class, ShoretelAgentWrapUpEventMessage);
        put(AuthenticateResShoretelMessage.class, ShoretelAuthenticateMessage);
        put(SubscribeEventsResShoretelMessage.class, ShoretelSubscribeMessage);
        put(UnsubscribeEventsResShoretelMessage.class, ShoretelUnsubscribeMessage);
    }};

    /**
     * shoretel message id -> shoretel message type map
     */
    public static final Map<String, Class<?>> ShoretelMessageTypes = new HashMap<String, Class<?>>() {{
        put(ShoretelMessageTypeIds.get(AgentIdleEvent.class), AgentIdleEvent.class);
        put(ShoretelMessageTypeIds.get(AgentLoginEvent.class), AgentLoginEvent.class);
        put(ShoretelMessageTypeIds.get(AgentOnCallEvent.class), AgentOnCallEvent.class);
        put(ShoretelMessageTypeIds.get(AgentReleasedEvent.class), AgentReleasedEvent.class);
        put(ShoretelMessageTypeIds.get(AgentReservedEvent.class), AgentReservedEvent.class);
        put(ShoretelMessageTypeIds.get(AgentWrapUpEvent.class), AgentWrapUpEvent.class);
        put(ShoretelMessageTypeIds.get(AuthenticateResShoretelMessage.class), AuthenticateResShoretelMessage.class);
        put(ShoretelMessageTypeIds.get(SubscribeEventsResShoretelMessage.class), SubscribeEventsResShoretelMessage.class);
        put(ShoretelMessageTypeIds.get(UnsubscribeEventsResShoretelMessage.class), UnsubscribeEventsResShoretelMessage.class);
    }};


}
