package com.nice.saas.wfo.acd.shoretel.common;

import com.nice.saas.wfo.acd.shoretel.ShoretelReceiver;
import com.nice.saas.wfo.acd.shoretel.common.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static org.junit.Assert.assertEquals;

public class ShoretelReceiverTest {
    ShoretelReceiver receiver;
    Field buffersField;
    BlockingQueue<ByteBuffer> buffers;
    String[] strArr;
    List<ByteBuffer> bufferList;
    @Before
    public void setUp() throws NoSuchFieldException {
        receiver = new ShoretelReceiver();
        buffersField = receiver.getClass().getSuperclass().getDeclaredField("buffers");
        buffersField.setAccessible(true);
        buffers = new ArrayBlockingQueue<>(100);
        bufferList =  new ArrayList<>();
    }

    @After
    public void tearDown() throws IllegalAccessException {
        String joined = String.join("", strArr);

        String[] joinedAndSplitted = joined.split("\0");
        List<String> joinedAndSplittedList = new LinkedList(Arrays.asList(joinedAndSplitted));

        joinedAndSplittedList.removeIf(p -> p.equals(""));
        joinedAndSplitted = joinedAndSplittedList.toArray(new String[0]);


        for (String item : strArr) {
            bufferList.add(StandardCharsets.UTF_8.encode(item));
        }
        buffers.addAll(bufferList);
        buffersField.set(receiver, buffers);
        List<ByteBuffer> results = new ArrayList<>();
        while (buffers.size() > 0) {

            results.addAll(receiver.receive());
        }
        List<String> resStrList = new ArrayList<>();
        for (ByteBuffer res : results) {
            resStrList.add(StandardCharsets.UTF_8.decode(res).toString());
        }
        String[] resStrArr = resStrList.toArray(new String[0]);
        assertEquals(joinedAndSplitted.length, resStrArr.length);
        for (int i=0; i< joinedAndSplitted.length; i++) {
            assertEquals(joinedAndSplitted[i], resStrArr[i]);
        }
    }

    @Test
    public void receive1() throws NoSuchFieldException, IllegalAccessException {

        strArr = new String[] {
                "qwerty\0",
                "asdfgh\0"
        };

    }

    @Test
    public void receive2() throws NoSuchFieldException, IllegalAccessException {

        strArr = new String[] {
                "qwerty\0",
                "asdfgh\0",
                "vasya\0",
        };

    }

    @Test
    public void receive3() throws NoSuchFieldException, IllegalAccessException {

        strArr = new String[] {
                "qwerty\0asdfgh\0",
                "asdfgh\0",
                "vasya\0",
        };

    }
    @Test
    public void receive4() throws NoSuchFieldException, IllegalAccessException {

        strArr = new String[] {
                "qwerty\0asdfgh\0qqq",
                "asdfgh\0www",
                "vasya\0",
        };

    }

    @Test
    public void receive5() throws NoSuchFieldException, IllegalAccessException {

        strArr = new String[] {
                "qwerty\0asdfgh\0qqq",
                "\0asdfgh\0www",
                "vasya\0",
        };

    }

    @Test
    public void receive6() throws NoSuchFieldException, IllegalAccessException {

        strArr = new String[] {
                "\0qwerty\0asdfgh\0qqq",
                "\0asdfgh\0www",
                "\0vasya\0",
        };

    }

    @Test
    public void receive7() throws NoSuchFieldException, IllegalAccessException {

        strArr = new String[] {
                "\0qwerty\0asdfgh\0qqq",
                "\0\0\0asdfgh\0www",
                "\0vasya\0",
        };

    }

}