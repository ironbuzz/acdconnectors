package com.nice.saas.wfo.acd.shoretel.messages;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nice.saas.wfo.acd.cisco.common.Constants;
import com.nice.saas.wfo.acd.shoretel.ConstantsShoretel;
import com.nice.saas.wfo.acd.shoretel.messages.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;

public class SubscribeEventsResShoretelMessageTest {
    SubscribeEventsResShoretelMessage message;
    @Before
    public void setUp() {
        message = new SubscribeEventsResShoretelMessage();

        message.setVersion(ConstantsShoretel.DefaultAppVersion);
        message.setSequence(1);
        message.topic = ConstantsShoretel.DefaultShoretelTopic;
        message.setRequestId(ConstantsShoretel.DefaultShoretelRequestId);

        message.subscribe = new String[] {"123"};
        message.errorMsg = "error";
    }

    @After
    public void tearDown() {
    }

    @Test
    public void getSubscribe() {
        assertEquals(message.subscribe[0], "123");
    }

    @Test
    public void setSubscribe() throws NoSuchFieldException, IllegalAccessException {
        message.subscribe= new String[]{"2","1"};
        Field field =message.getClass().getDeclaredField("subscribe");
        field.setAccessible(true);
        assertEquals("2", message.subscribe[0]);
        assertEquals("1", message.subscribe[1]);
    }



    @Test
    public void getRequestId() {
        assertEquals(message.getRequestId(), ConstantsShoretel.DefaultShoretelRequestId);
    }

    @Test
    public void setRequestId() throws NoSuchFieldException, IllegalAccessException {
        message.setRequestId(2);
        Field field =message.getClass().getSuperclass().getDeclaredField("requestId");
        field.setAccessible(true);
        assertEquals(2, field.get(message));
    }

    @Test
    public void getVersion() {
        assertEquals(message.getVersion(), 1);
    }

    @Test
    public void setVersion() throws NoSuchFieldException, IllegalAccessException {
        message.setVersion(2);
        Field field =message.getClass().getSuperclass().getSuperclass().getDeclaredField("version");
        field.setAccessible(true);
        assertEquals(2, field.get(message));
    }

    @Test
    public void getSequence() {
        assertEquals(message.getSequence(), 1);
    }

    @Test
    public void setSequence() throws NoSuchFieldException, IllegalAccessException {
        message.setSequence((long)2);
        Field field =message.getClass().getSuperclass().getSuperclass().getDeclaredField("sequence");
        field.setAccessible(true);
        assertEquals((long)2, field.get(message));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void serialize() {
        ByteBuffer buffer = message.serialize();
    }

    @Test
    public void deserialize() {
        message.setVersion(1);
        message.setSequence(2);
        message.topic = "contact-center";
        message.setRequestId(10);
        message.result = 0;
        message.errorMsg = "error";
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String jsonString = gson.toJson(message);
        byte[] tempArray = jsonString.getBytes(Charset.forName("UTF-8")); //TODO: check which encoding is needed
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(tempArray.length + 1); // length + 1 because we are adding NULL  character at the end of every message
        byteBuffer.put(tempArray);
        byteBuffer.put((byte) '\0');
        byteBuffer.rewind();

        AuthenticateResShoretelMessage message1 = new AuthenticateResShoretelMessage();
        message1 = (AuthenticateResShoretelMessage) message1.deserialize(byteBuffer);
        assertEquals(message1.getVersion(), message.getVersion());
        assertEquals(message1.getSequence(), message.getSequence());
        assertEquals(message1.topic, message.topic);
        assertEquals(message1.getRequestId(), message.getRequestId());
        assertEquals(message1.result, message.result);
        assertEquals(message1.errorMsg, message.errorMsg);
    }


}