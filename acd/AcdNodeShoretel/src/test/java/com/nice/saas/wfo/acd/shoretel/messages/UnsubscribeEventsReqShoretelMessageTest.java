package com.nice.saas.wfo.acd.shoretel.messages;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nice.saas.wfo.acd.cisco.common.Constants;
import com.nice.saas.wfo.acd.shoretel.ConstantsShoretel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class UnsubscribeEventsReqShoretelMessageTest {
    UnsubscribeEventsReqShoretelMessage message;
    @Before
    public void setUp() {
        message = new UnsubscribeEventsReqShoretelMessage();

        message.setVersion(ConstantsShoretel.DefaultAppVersion);
        message.setSequence(1);
        message.topic = ConstantsShoretel.DefaultShoretelTopic;
        message.setRequestId(ConstantsShoretel.DefaultShoretelRequestId);

        message.setSubscribe(new String[] {"123"});
    }

    @After
    public void tearDown() {
    }

    @Test
    public void getSubscribe() {
        assertEquals(((String[])message.getSubscribe())[0], "123");
    }

    @Test
    public void setSubscribe() throws NoSuchFieldException, IllegalAccessException {
        message.setSubscribe(new String[]{"2","1"});
        Field field =message.getClass().getDeclaredField("subscribe");
        field.setAccessible(true);
        assertEquals("2", ((String[])field.get(message))[0]);
        assertEquals("1", ((String[])field.get(message))[1]);
    }

    @Test
    public void deserialize() {
    }

    @Test
    public void getRequestId() {
        assertEquals(message.getRequestId(), ConstantsShoretel.DefaultShoretelRequestId);
    }

    @Test
    public void setRequestId() throws NoSuchFieldException, IllegalAccessException {
        message.setRequestId(2);
        Field field =message.getClass().getSuperclass().getDeclaredField("requestId");
        field.setAccessible(true);
        assertEquals(2, field.get(message));
    }

    @Test
    public void getVersion() {
        assertEquals(message.getVersion(), 1);
    }

    @Test
    public void setVersion() throws NoSuchFieldException, IllegalAccessException {
        message.setVersion(2);
        Field field =message.getClass().getSuperclass().getSuperclass().getDeclaredField("version");
        field.setAccessible(true);
        assertEquals(2, field.get(message));
    }

    @Test
    public void getSequence() {
        assertEquals(message.getSequence(), 1);
    }

    @Test
    public void setSequence() throws NoSuchFieldException, IllegalAccessException {
        message.setSequence((long)2);
        Field field =message.getClass().getSuperclass().getSuperclass().getDeclaredField("sequence");
        field.setAccessible(true);
        assertEquals((long)2, field.get(message));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void deserialize1() {
        ByteBuffer buffer = ByteBuffer.allocateDirect(8);

        buffer.putInt(3);
        buffer.putInt(4);
        buffer.rewind();
        message.deserialize(buffer);
    }

    @Test
    public void serialize() {
        ByteBuffer buffer = message.serialize();
        assertNotNull(buffer);
        assertEquals(buffer.position(), 0);
        int length = buffer.remaining() - 1;  // length -1 because we don't need to take NULL character
        byte[] tempArray = new byte[length];
        buffer.get(tempArray, 0, length);
        String jsonString = new String(tempArray, Charset.forName("UTF-8"));
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        message = gson.fromJson(jsonString, message.getClass());
        assertEquals(message.getVersion(), ConstantsShoretel.DefaultAppVersion);
        assertEquals(message.getSequence(), 1);
        assertEquals(message.topic, ConstantsShoretel.DefaultShoretelTopic);
        assertEquals(message.getRequestId(), ConstantsShoretel.DefaultShoretelRequestId);
        assertEquals(message.message, "unsubscribe-events");
        assertEquals(((String[])message.getSubscribe())[0], "123");
        assertEquals(message.request, "Unsubscribe");

    }


}