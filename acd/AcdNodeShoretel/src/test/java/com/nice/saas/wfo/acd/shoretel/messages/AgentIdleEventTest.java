package com.nice.saas.wfo.acd.shoretel.messages;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nice.saas.wfo.acd.shoretel.messages.AgentIdleEvent;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;

public class AgentIdleEventTest {
    AgentIdleEvent message;
    @Before
    public void setUp() {
        message = new AgentIdleEvent();
    }



    @Test(expected = UnsupportedOperationException.class)
    public void serialize() {
        ByteBuffer buffer = message.serialize();
    }

    @Test
    public void getVersion() {
        assertEquals(message.getVersion(), 0);
    }

    @Test
    public void setVersion() throws NoSuchFieldException, IllegalAccessException {
        message.setVersion(2);
        Field field =message.getClass().getSuperclass().getSuperclass().getDeclaredField("version");
        field.setAccessible(true);
        assertEquals(2, field.get(message));
    }

    @Test
    public void getSequence() {
        assertEquals(message.getSequence(), 0);
    }

    @Test
    public void setSequence() throws NoSuchFieldException, IllegalAccessException {
        message.setSequence((long)2);
        Field field =message.getClass().getSuperclass().getSuperclass().getDeclaredField("sequence");
        field.setAccessible(true);
        assertEquals((long)2, field.get(message));
    }

    @Test
    public void deserialize() {
        message.setVersion(1);
        message.setSequence(2);
        message.timestamp = "12";
        message.gmt = "12";
        message.topic = "contact-center";
        message.message = "agent-idle";
        message.subTopic = "sub";
        message.agentId = "agent1";
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String jsonString = gson.toJson(message);
        byte[] tempArray = jsonString.getBytes(Charset.forName("UTF-8")); //TODO: check which encoding is needed
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(tempArray.length + 1); // length + 1 because we are adding NULL  character at the end of every message
        byteBuffer.put(tempArray);
        byteBuffer.put((byte)'\0');
        byteBuffer.rewind();

        AgentIdleEvent message1 = new AgentIdleEvent();
        message1 = (AgentIdleEvent) message1.deserialize(byteBuffer);
        assertEquals(message1.getVersion(), message.getVersion());
        assertEquals(message1.getSequence(), message.getSequence());
        assertEquals(message1.timestamp, message.timestamp);
        assertEquals(message1.gmt, message.gmt);
        assertEquals(message1.topic, message.topic);
        assertEquals(message1.message, message.message);
        assertEquals(message1.subTopic, message.subTopic);
        assertEquals(message1.agentId, message.agentId);
    }

}