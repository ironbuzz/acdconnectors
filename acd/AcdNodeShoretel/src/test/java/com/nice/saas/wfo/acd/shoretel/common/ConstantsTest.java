package com.nice.saas.wfo.acd.shoretel.common;

import com.nice.saas.wfo.acd.shoretel.ConstantsShoretel;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConstantsTest {
    @Test
    public void shoretelMessageFields(){
        Assert.assertEquals(ConstantsShoretel.ShoretelMessageFields.Version.ordinal(), 0);
        assertEquals(ConstantsShoretel.ShoretelMessageFields.Sequence.ordinal(), 1);
        assertEquals(ConstantsShoretel.ShoretelMessageFields.Topic.ordinal(), 2);
        assertEquals(ConstantsShoretel.ShoretelMessageFields.RequestId.ordinal(), 3);
        assertEquals(ConstantsShoretel.ShoretelMessageFields.Message.ordinal(), 4);
        assertEquals(ConstantsShoretel.ShoretelMessageFields.User.ordinal(), 5);
        assertEquals(ConstantsShoretel.ShoretelMessageFields.Password.ordinal(), 6);

    }

}