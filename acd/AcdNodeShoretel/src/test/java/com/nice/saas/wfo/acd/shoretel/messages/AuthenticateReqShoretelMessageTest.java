package com.nice.saas.wfo.acd.shoretel.messages;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nice.saas.wfo.acd.cisco.common.Constants;
import com.nice.saas.wfo.acd.shoretel.ConstantsShoretel;
import com.nice.saas.wfo.acd.shoretel.messages.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AuthenticateReqShoretelMessageTest {
    AuthenticateReqShoretelMessage message;
    @Before
    public void setUp() {
        message = new AuthenticateReqShoretelMessage();
        message.setVersion(ConstantsShoretel.DefaultAppVersion);
        message.setSequence(1);
        message.topic = ConstantsShoretel.DefaultShoretelTopic;
        message.setRequestId(ConstantsShoretel.DefaultShoretelRequestId);
        message.setUser("Vasya");
        message.setPassword("123");
     }

    @After
    public void tearDown() {
    }

    @Test(expected = UnsupportedOperationException.class)
    public void deserialize() {
        ByteBuffer buffer = ByteBuffer.allocateDirect(8);

        buffer.putInt(3);
        buffer.putInt(4);
        buffer.rewind();
        message.deserialize(buffer);
    }

    @Test
    public void serialize() {
        ByteBuffer buffer = message.serialize();
        assertNotNull(buffer);
        assertEquals(buffer.position(), 0);
        int length = buffer.remaining() - 1;  // length -1 because we don't need to take NULL character
        byte[] tempArray = new byte[length];
        buffer.get(tempArray, 0, length);
        String jsonString = new String(tempArray, Charset.forName("UTF-8"));
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        message = gson.fromJson(jsonString, message.getClass());
        assertEquals(message.getVersion(), ConstantsShoretel.DefaultAppVersion);
        assertEquals(message.getSequence(), 1);
        assertEquals(message.topic, ConstantsShoretel.DefaultShoretelTopic);
        assertEquals(message.getRequestId(), ConstantsShoretel.DefaultShoretelRequestId);
        assertEquals(message.getUser(), "Vasya");
        assertEquals(message.getPassword(), "123");
    }

}