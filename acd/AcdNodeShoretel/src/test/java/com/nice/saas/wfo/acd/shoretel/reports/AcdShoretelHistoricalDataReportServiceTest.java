package com.nice.saas.wfo.acd.shoretel.reports;

import com.nice.saas.wfo.acd.shoretel.reports.*;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AcdShoretelHistoricalDataReportServiceTest {

    @Test
    public void  getAcdType() throws NoSuchFieldException, IllegalAccessException {
        AcdShoretelHistoricalDataReportService service = new AcdShoretelHistoricalDataReportService();

        service.setQuery("query");
        Field field = service.getClass().getSuperclass().getDeclaredField("query");
        field.setAccessible(true);
        assertEquals(field.get(service), "query");
    }
}