package com.nice.saas.wfo.acd.shoretel;

import com.nice.saas.wfo.acd.cisco.common.TcpClient;
import com.nice.saas.wfo.acd.rta.GenericACDEvent;
import com.nice.saas.wfo.acd.rta.GenericACDEventListener;
import com.nice.saas.wfo.comp.manager.asc.generic.ACDErrorHandler;
import com.nice.saas.wfo.comp.manager.asc.generic.ConnectionError;
import com.nice.saas.wfo.comp.manager.asc.generic.GeneralError;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import org.junit.Before;
import org.junit.Test;

import java.awt.event.ActionEvent;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.*;

public class AcdEventServiceShoretelImplTest {
    @Before
    public void  setUp() {
        service.receiver = new ShoretelReceiver();
        service.messageParser = new ShoretelMessageParser();
        service.acdMessageTranslator = new AcdEventShoretelTranslator();
    }

    AcdEventServiceShoretelImpl service = new AcdEventServiceShoretelImpl();

    @Test
    public void  init() throws NoSuchFieldException, IllegalAccessException {

        Field field = service.getClass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 50);
        field = service.getClass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 50);



        service.init();
        field = service.getClass().getSuperclass().getDeclaredField("messages");
        field.setAccessible(true);
        assertNotNull(field.get(service));
        assertTrue(field.get(service) instanceof ArrayBlockingQueue);
        assertEquals(((ArrayBlockingQueue)field.get(service)).remainingCapacity(), 50);

        field = service.getClass().getSuperclass().getDeclaredField("eventsProcessorPool");
        field.setAccessible(true);
        assertNotNull(field.get(service));
        assertTrue(field.get(service) instanceof ExecutorService);

    }

    @Test
    public void  addEventListener() throws NoSuchFieldException, IllegalAccessException {
        Field field = service.getClass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 50);
        field = service.getClass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 50);



        service.init();
        service.addEventListener(new GenericACDEventListener() {
            @Override
            public void processEvents(List<GenericACDEvent> list) {

            }

            @Override
            public void processEvent(GenericACDEvent genericACDEvent) {

            }
        });
        field = service.getClass().getSuperclass().getDeclaredField("acdEventListeners");
        field.setAccessible(true);
        ArrayList l = (ArrayList)field.get(service);
        assertNotNull(l);
        assertEquals(l.size(), 1);
    }

    @Test
    public void  addLogger() throws NoSuchFieldException, IllegalAccessException {
        Field field = service.getClass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 50);
        field = service.getClass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 50);


        service.init();
        service.addLogger(new GenericLogger() {
            @Override
            public void info(String s) {

            }

            @Override
            public void error(String s, Throwable throwable, Object... objects) {

            }

            @Override
            public void warning(String s) {

            }
        });
        field = service.getClass().getSuperclass().getDeclaredField("logger");
        field.setAccessible(true);
        GenericLogger l = (GenericLogger)field.get(service);
        assertNotNull(l);
    }

    @Test
    public void  destroy() throws NoSuchFieldException, IllegalAccessException {
        Field field = service.getClass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 50);
        field = service.getClass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 50);



        service.init();
        service.addLogger(new GenericLogger() {
            @Override
            public void info(String s) {

            }

            @Override
            public void error(String s, Throwable throwable, Object... objects) {

            }

            @Override
            public void warning(String s) {

            }
        });
        ShoretelReceiver receiver = (ShoretelReceiver)service.receiver;
        field = receiver.getClass().getSuperclass().getDeclaredField("client");
        field.setAccessible(true);
        field.set(receiver, new TcpClient("127.0.0.1", 8080, 40, 40) {
            @Override
            protected void onRead(ByteBuffer buf) throws Exception {

            }

            @Override
            protected void onConnected() throws Exception {

            }

            @Override
            protected void onDisconnected() throws Exception {

            }

            @Override
            protected void onFailure(Exception e) throws Exception {

            }
        });



//        service.destroy();
//
//        assertTrue(service.stopped.get());
//        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("isStarted");
//        field.setAccessible(true);
//        assertFalse(((AtomicBoolean)field.get(service)).get());

    }

    @Test
    public void  actionPerformed() throws IllegalAccessException, NoSuchFieldException {
        Field field = service.getClass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 50);
        field = service.getClass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 50);



        service.init();
        service.addLogger(new GenericLogger() {
            @Override
            public void info(String s) {

            }

            @Override
            public void error(String s, Throwable throwable, Object... objects) {

            }

            @Override
            public void warning(String s) {

            }
        });
        ShoretelReceiver receiver = (ShoretelReceiver)service.receiver;
        field = receiver.getClass().getSuperclass().getDeclaredField("client");
        field.setAccessible(true);
        field.set(receiver, new TcpClient("127.0.0.1", 8080, 40, 40) {
            @Override
            protected void onRead(ByteBuffer buf) throws Exception {

            }

            @Override
            protected void onConnected() throws Exception {

            }

            @Override
            protected void onDisconnected() throws Exception {

            }

            @Override
            protected void onFailure(Exception e) throws Exception {

            }
        });

        field = service.getClass().getSuperclass().getDeclaredField("acdErrorHandler");
        field.setAccessible(true);
        field.set(service, new ACDErrorHandler() {
            @Override
            public void generalError(GeneralError error) {

            }

            @Override
            public void connectionError(ConnectionError error) {

            }
        });

        service.actionPerformed(new ActionEvent(this, 1, "disconnected"));
        field = service.getClass().getSuperclass().getDeclaredField("isStarted");
        field.setAccessible(true);
        assertFalse(((AtomicBoolean)field.get(service)).get());
        field.set(service, new AtomicBoolean(true) );
        assertTrue(((AtomicBoolean)field.get(service)).get());
        service.actionPerformed(new ActionEvent(this, 1, "failure"));
        assertFalse(((AtomicBoolean)field.get(service)).get());
    }

    @Test
    public void  eventsProcessorPoolCore() throws IllegalAccessException, NoSuchFieldException {
        Field field = service.getClass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 50);
        field = service.getClass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 50);



        service.init();
        service.addLogger(new GenericLogger() {
            @Override
            public void info(String s) {

            }

            @Override
            public void error(String s, Throwable throwable, Object... objects) {

            }

            @Override
            public void warning(String s) {

            }
        });
        ShoretelReceiver receiver = (ShoretelReceiver)service.receiver;
        field = receiver.getClass().getSuperclass().getDeclaredField("client");
        field.setAccessible(true);
        field.set(receiver, new TcpClient("127.0.0.1", 8080, 40, 40) {
            @Override
            protected void onRead(ByteBuffer buf) throws Exception {

            }

            @Override
            protected void onConnected() throws Exception {

            }

            @Override
            protected void onDisconnected() throws Exception {

            }

            @Override
            protected void onFailure(Exception e) throws Exception {

            }
        });

        field = service.getClass().getSuperclass().getDeclaredField("acdErrorHandler");
        field.setAccessible(true);
        field.set(service,new ACDErrorHandler() {
            @Override
            public void generalError(GeneralError error) {

            }

            @Override
            public void connectionError(ConnectionError error) {

            }
        });




    }

}