package com.nice.saas.wfo.acd.shoretel.common;

import com.nice.saas.wfo.acd.shoretel.common.*;
import com.nice.saas.wfo.acd.shoretel.messages.*;
import com.nice.saas.wfo.acd.rta.GenericACDEvent;
import com.nice.saas.wfo.acd.shoretel.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class AcdEventShoretelTranslatorTest {
    @Test
    public void translate() {
        AcdEventShoretelTranslator translator =  new AcdEventShoretelTranslator();
        AgentLoginEvent message = new AgentLoginEvent();
        message.agentId ="1";
        message.cause = 2;

        GenericACDEvent event = translator.translate(message);
        assertEquals(event.getAcdId(),null );
        assertEquals(event.getReasonCode(), null );
        assertEquals(event.getEventCode(), "2_null" );
        assertEquals(event.getLoginId(), "1" );

    }

}