package com.nice.saas.wfo.acd.shoretel.messages;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nice.saas.wfo.acd.shoretel.messages.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;

public class AuthenticateResShoretelMessageTest {
    AuthenticateResShoretelMessage message;
    @Before
    public void setUp() {
        message = new AuthenticateResShoretelMessage();
    }

    @After
    public void tearDown() {
    }

    @Test(expected = UnsupportedOperationException.class)
    public void serialize() {
        ByteBuffer buffer = message.serialize();
    }

    @Test
    public void deserialize() {
        message.setVersion(1);
        message.setSequence(2);
        message.topic = "contact-center";
        message.setRequestId(10);
        message.result = 0;
        message.errorMsg = "error";
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String jsonString = gson.toJson(message);
        byte[] tempArray = jsonString.getBytes(Charset.forName("UTF-8")); //TODO: check which encoding is needed
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(tempArray.length + 1); // length + 1 because we are adding NULL  character at the end of every message
        byteBuffer.put(tempArray);
        byteBuffer.put((byte) 0x00);
        byteBuffer.rewind();

        AuthenticateResShoretelMessage message1 = new AuthenticateResShoretelMessage();
        message1 = (AuthenticateResShoretelMessage) message1.deserialize(byteBuffer);
        assertEquals(message1.getVersion(), message.getVersion());
        assertEquals(message1.getSequence(), message.getSequence());
        assertEquals(message1.topic, message.topic);
        assertEquals(message1.getRequestId(), message.getRequestId());
        assertEquals(message1.result, message.result);
        assertEquals(message1.errorMsg, message.errorMsg);

    }

}