package com.nice.saas.wfo.acd.shoretel.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import com.nice.saas.wfo.acd.shoretel.ShoretelMessageParser;
import com.nice.saas.wfo.acd.shoretel.messages.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class ShoretelMessageParserTest {
    ShoretelMessageParser parser;

    @Before
    public void setUp() {
        parser = new ShoretelMessageParser();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void parseAgentIdleEvent() {
        AgentIdleEvent message = new AgentIdleEvent();
        message.setVersion(1);
        message.setSequence((long)2);
        message.timestamp = "12";
        message.gmt = "12";
        message.topic = "contact-center";
        message.message = "agent-idle";
        message.subTopic = "sub";
        message.agentId = "agent1";
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String jsonString = gson.toJson(message);
        byte[] tempArray = jsonString.getBytes(Charset.forName("UTF-8")); //TODO: check which encoding is needed
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(tempArray.length + 1); // length + 1 because we are adding NULL  character at the end of every message
        byteBuffer.put(tempArray);
        byteBuffer.put((byte)'\0');
        byteBuffer.rewind();
        Message m = parser.parse(byteBuffer);
        assertTrue(m instanceof AgentIdleEvent);
        AgentIdleEvent shoretelMessage = (AgentIdleEvent) m;
        assertEquals(shoretelMessage.getVersion(), message.getVersion());
        assertEquals(shoretelMessage.getSequence(), message.getSequence());
        assertEquals(shoretelMessage.timestamp, message.timestamp);
        assertEquals(shoretelMessage.gmt, message.gmt);
        assertEquals(shoretelMessage.topic, message.topic);
        assertEquals(shoretelMessage.message, message.message);
        assertEquals(shoretelMessage.subTopic, message.subTopic);
        assertEquals(shoretelMessage.agentId, message.agentId);
    }

    @Test
    public void parseAgentLoginEvent() {
        AgentLoginEvent message = new AgentLoginEvent();
        message.setVersion(1);
        message.setSequence(2);
        message.timestamp = "12";
        message.gmt = "12";
        message.topic = "contact-center";
        message.message = "agent-login";
        message.subTopic = "sub";
        message.agentId = "agent1";
        message.cause = 1;
        message.agentNumber = "2";
        message.agentExt = "3";
        message.groups = new String[] {"2","1"};

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String jsonString = gson.toJson(message);
        byte[] tempArray = jsonString.getBytes(Charset.forName("UTF-8")); //TODO: check which encoding is needed
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(tempArray.length + 1); // length + 1 because we are adding NULL  character at the end of every message
        byteBuffer.put(tempArray);
        byteBuffer.put((byte)'\0');
        byteBuffer.rewind();
        Message m = parser.parse(byteBuffer);
        assertTrue(m instanceof AgentLoginEvent);
        AgentLoginEvent shoretelMessage = (AgentLoginEvent) m;
        assertEquals(shoretelMessage.getVersion(), message.getVersion());
        assertEquals(shoretelMessage.getSequence(), message.getSequence());
        assertEquals(shoretelMessage.timestamp, message.timestamp);
        assertEquals(shoretelMessage.gmt, message.gmt);
        assertEquals(shoretelMessage.topic, message.topic);
        assertEquals(shoretelMessage.message, message.message);
        assertEquals(shoretelMessage.subTopic, message.subTopic);
        assertEquals(shoretelMessage.agentId, message.agentId);
        assertEquals(shoretelMessage.cause, message.cause);
        assertEquals(shoretelMessage.agentNumber, message.agentNumber);
        assertEquals(shoretelMessage.agentExt, message.agentExt);
        for (int i=0; i< message.groups.length; i++) {
            assertEquals(shoretelMessage.groups[i], message.groups[i]);
        }
    }

    @Test
    public void parseAgentOnCallEvent() {
        AgentOnCallEvent message = new AgentOnCallEvent();
        message.setVersion(1);
        message.setSequence (2);
        message.timestamp = "12";
        message.gmt = "12";
        message.topic = "contact-center";
        message.message = "agent-on-call";
        message.subTopic = "sub";
        message.agentId = "agent1";
        message.cause = 1;
        message.callType = 2;
        message.callId = 3;
        message.pbxCallId = 4;
        message.guid = "guid";

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String jsonString = gson.toJson(message);
        byte[] tempArray = jsonString.getBytes(Charset.forName("UTF-8")); //TODO: check which encoding is needed
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(tempArray.length + 1); // length + 1 because we are adding NULL  character at the end of every message
        byteBuffer.put(tempArray);
        byteBuffer.put((byte)'\0');
        byteBuffer.rewind();
        Message m = parser.parse(byteBuffer);
        assertTrue(m instanceof AgentOnCallEvent);
        AgentOnCallEvent shoretelMessage = (AgentOnCallEvent) m;
        assertEquals(shoretelMessage.getVersion(), message.getVersion());
        assertEquals(shoretelMessage.getSequence(), message.getSequence());
        assertEquals(shoretelMessage.timestamp, message.timestamp);
        assertEquals(shoretelMessage.gmt, message.gmt);
        assertEquals(shoretelMessage.topic, message.topic);
        assertEquals(shoretelMessage.message, message.message);
        assertEquals(shoretelMessage.subTopic, message.subTopic);
        assertEquals(shoretelMessage.agentId, message.agentId);
        assertEquals(shoretelMessage.cause, message.cause);
        assertEquals(shoretelMessage.callType, message.callType);
        assertEquals(shoretelMessage.callId, message.callId);
        assertEquals(shoretelMessage.pbxCallId, message.pbxCallId);
        assertEquals(shoretelMessage.guid, message.guid);

    }

    @Test
    public void parseAgentReleasedEvent() {
        AgentReleasedEvent message = new AgentReleasedEvent();
        message.setVersion(1);
        message.setSequence(2);
        message.timestamp = "12";
        message.gmt = "12";
        message.topic = "contact-center";
        message.message = "agent-release";
        message.subTopic = "sub";
        message.agentId = "agent1";
        message.cause = 1;
        message.releaseCode = 2;

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String jsonString = gson.toJson(message);
        byte[] tempArray = jsonString.getBytes(Charset.forName("UTF-8")); //TODO: check which encoding is needed
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(tempArray.length + 1); // length + 1 because we are adding NULL  character at the end of every message
        byteBuffer.put(tempArray);
        byteBuffer.put((byte)'\0');
        byteBuffer.rewind();
        Message m = parser.parse(byteBuffer);
        assertTrue(m instanceof AgentReleasedEvent);
        AgentReleasedEvent shoretelMessage = (AgentReleasedEvent) m;
        assertEquals(shoretelMessage.getVersion(), message.getVersion());
        assertEquals(shoretelMessage.getSequence(), message.getSequence());
        assertEquals(shoretelMessage.timestamp, message.timestamp);
        assertEquals(shoretelMessage.gmt, message.gmt);
        assertEquals(shoretelMessage.topic, message.topic);
        assertEquals(shoretelMessage.message, message.message);
        assertEquals(shoretelMessage.subTopic, message.subTopic);
        assertEquals(shoretelMessage.agentId, message.agentId);
        assertEquals(shoretelMessage.cause, message.cause);
        assertEquals(shoretelMessage.releaseCode, message.releaseCode);


    }

    @Test
    public void parseAgentReservedEvent() {
        AgentReservedEvent message = new AgentReservedEvent();
        message.setVersion(1);
        message.setSequence(2);
        message.timestamp = "12";
        message.gmt = "12";
        message.topic = "contact-center";
        message.message = "agent-reserved";
        message.subTopic = "sub";
        message.agentId = "agent1";
        message.cause = 1;
        message.groups = new String[] {"2","1"};
        message.guid = "guid";

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String jsonString = gson.toJson(message);
        byte[] tempArray = jsonString.getBytes(Charset.forName("UTF-8")); //TODO: check which encoding is needed
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(tempArray.length + 1); // length + 1 because we are adding NULL  character at the end of every message
        byteBuffer.put(tempArray);
        byteBuffer.put((byte)'\0');
        byteBuffer.rewind();
        Message m = parser.parse(byteBuffer);
        assertTrue(m instanceof AgentReservedEvent);
        AgentReservedEvent shoretelMessage = (AgentReservedEvent) m;
        assertEquals(shoretelMessage.getVersion(), message.getVersion());
        assertEquals(shoretelMessage.getSequence(), message.getSequence());
        assertEquals(shoretelMessage.timestamp, message.timestamp);
        assertEquals(shoretelMessage.gmt, message.gmt);
        assertEquals(shoretelMessage.topic, message.topic);
        assertEquals(shoretelMessage.message, message.message);
        assertEquals(shoretelMessage.subTopic, message.subTopic);
        assertEquals(shoretelMessage.agentId, message.agentId);
        assertEquals(shoretelMessage.cause, message.cause);
        for (int i=0; i< message.groups.length; i++) {
            assertEquals(shoretelMessage.groups[i], message.groups[i]);
        }
        assertEquals(shoretelMessage.guid, message.guid);


    }

    @Test
    public void parseAgentWrapUpEvent() {
        AgentWrapUpEvent message = new AgentWrapUpEvent();
        message.setVersion(1);
        message.setSequence(2);
        message.timestamp = "12";
        message.gmt = "12";
        message.topic = "contact-center";
        message.message = "agent-wrap";
        message.subTopic = "sub";
        message.agentId = "agent1";
        message.cause = 1;
        message.mediaType = "2";
        message.callId = 3;
        message.tapiCallId = 4;
        message.guid = "a";
        message.wrapupCode = "q";

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String jsonString = gson.toJson(message);
        byte[] tempArray = jsonString.getBytes(Charset.forName("UTF-8")); //TODO: check which encoding is needed
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(tempArray.length + 1); // length + 1 because we are adding NULL  character at the end of every message
        byteBuffer.put(tempArray);
        byteBuffer.put((byte)'\0');
        byteBuffer.rewind();
        Message m = parser.parse(byteBuffer);
        assertTrue(m instanceof AgentWrapUpEvent);
        AgentWrapUpEvent shoretelMessage = (AgentWrapUpEvent) m;
        assertEquals(shoretelMessage.getVersion(), message.getVersion());
        assertEquals(shoretelMessage.getSequence(), message.getSequence());
        assertEquals(shoretelMessage.timestamp, message.timestamp);
        assertEquals(shoretelMessage.gmt, message.gmt);
        assertEquals(shoretelMessage.topic, message.topic);
        assertEquals(shoretelMessage.message, message.message);
        assertEquals(shoretelMessage.subTopic, message.subTopic);
        assertEquals(shoretelMessage.agentId, message.agentId);
        assertEquals(shoretelMessage.cause, message.cause);
        assertEquals(shoretelMessage.mediaType, message.mediaType);
        assertEquals(shoretelMessage.callId, message.callId);
        assertEquals(shoretelMessage.tapiCallId, message.tapiCallId);
        assertEquals(shoretelMessage.guid, message.guid);
        assertEquals(shoretelMessage.wrapupCode, message.wrapupCode);


    }

    @Test
    public void parseAuthenticateResShoretelMessage() {
        AuthenticateResShoretelMessage message = new AuthenticateResShoretelMessage();
        message.setVersion(1);
        message.setSequence(2);
        message.topic = "contact-center";
        message.setRequestId(10);
        message.result = 0;
        message.errorMsg = "error";
        message.message = "authenticate";

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String jsonString = gson.toJson(message);
        byte[] tempArray = jsonString.getBytes(Charset.forName("UTF-8")); //TODO: check which encoding is needed
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(tempArray.length + 1); // length + 1 because we are adding NULL  character at the end of every message
        byteBuffer.put(tempArray);
        byteBuffer.put((byte)'\0');
        byteBuffer.rewind();
        Message m = parser.parse(byteBuffer);
        assertTrue(m instanceof AuthenticateResShoretelMessage);
        AuthenticateResShoretelMessage shoretelMessage = (AuthenticateResShoretelMessage) m;
        assertEquals(shoretelMessage.getVersion(), message.getVersion());
        assertEquals(shoretelMessage.getSequence(), message.getSequence());
        assertEquals(shoretelMessage.topic, message.topic);
        assertEquals(shoretelMessage.message, message.message);
        assertEquals(shoretelMessage.getRequestId(), message.getRequestId());
        assertEquals(shoretelMessage.result, message.result);
        assertEquals(shoretelMessage.errorMsg, message.errorMsg);

    }

    @Test
    public void parseSubscribeEventsResShoretelMessage() {
        SubscribeEventsResShoretelMessage message = new SubscribeEventsResShoretelMessage();
        message.setVersion(1);
        message.setSequence(2);
        message.topic = "contact-center";
        message.setRequestId(10);
        message.result = 0;
        message.errorMsg = "error";
        message.message = "subscribe-events";

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String jsonString = gson.toJson(message);
        byte[] tempArray = jsonString.getBytes(Charset.forName("UTF-8")); //TODO: check which encoding is needed
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(tempArray.length + 1); // length + 1 because we are adding NULL  character at the end of every message
        byteBuffer.put(tempArray);
        byteBuffer.put((byte)'\0');
        byteBuffer.rewind();
        Message m = parser.parse(byteBuffer);
        assertTrue(m instanceof SubscribeEventsResShoretelMessage);
        SubscribeEventsResShoretelMessage shoretelMessage = (SubscribeEventsResShoretelMessage) m;
        assertEquals(shoretelMessage.getVersion(), message.getVersion());
        assertEquals(shoretelMessage.getSequence(), message.getSequence());
        assertEquals(shoretelMessage.topic, message.topic);
        assertEquals(shoretelMessage.message, message.message);
        assertEquals(shoretelMessage.getRequestId(), message.getRequestId());
        assertEquals(shoretelMessage.result, message.result);
        assertEquals(shoretelMessage.errorMsg, message.errorMsg);

    }

    @Test
    public void parseUnsubscribeEventsResShoretelMessage() {
        UnsubscribeEventsResShoretelMessage message = new UnsubscribeEventsResShoretelMessage();
        message.setVersion(1);
        message.setSequence(2);
        message.topic = "contact-center";
        message.setRequestId(10);
        message.result = 0;
        message.message = "unsubscribe-events";

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String jsonString = gson.toJson(message);
        byte[] tempArray = jsonString.getBytes(Charset.forName("UTF-8")); //TODO: check which encoding is needed
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(tempArray.length + 1); // length + 1 because we are adding NULL  character at the end of every message
        byteBuffer.put(tempArray);
        byteBuffer.put((byte)'\0');
        byteBuffer.rewind();
        Message m = parser.parse(byteBuffer);
        assertTrue(m instanceof UnsubscribeEventsResShoretelMessage);
        UnsubscribeEventsResShoretelMessage shoretelMessage = (UnsubscribeEventsResShoretelMessage) m;
        assertEquals(shoretelMessage.getVersion(), message.getVersion());
        assertEquals(shoretelMessage.getSequence(), message.getSequence());
        assertEquals(shoretelMessage.topic, message.topic);
        assertEquals(shoretelMessage.message, message.message);
        assertEquals(shoretelMessage.getRequestId(), message.getRequestId());
        assertEquals(shoretelMessage.result, message.result);

    }




}