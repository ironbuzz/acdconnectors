package com.nice.saas.wfo.comp.manage.asc.cisco;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import com.nice.saas.wfo.comp.manager.asc.generic.ReportType;
import com.nice.saas.wfo.comp.manager.asc.generic.json.AdherenceRowItem;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

public class AdherenceNormalizer extends NormalizerBase<AdherenceRowItem> {
    final String className = "AdherenceNormalizer";
    final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Override
    public Iterator getIterator(String csv, GenericLogger genericLogger) {
        logger = genericLogger;
        List<AdherenceRowItem> list = normalize(csv);
        return list.iterator();
    }


    @Override
    protected AdherenceRowItem mapToItem(String[] line) {
        final String functionName = "mapToItem()";
        AdherenceRowItem item = new AdherenceRowItem();
        try {
            item.setAgentId(line[0]);
            item.setStateIndex(line[1]);
            LocalDateTime startTime = LocalDateTime.parse(line[2].split("\\.")[0], formatter);
            item.setStartDate(String.valueOf(startTime.toEpochSecond(ZoneOffset.UTC)));
            item.setAgentStateId(line[3]);
            item.setAgentSessionId(line[4]);
            item.setSkillId(line[5]);
            item.setOutStateId(line[6]);
            item.setOutStateDescription(line[7]);
            item.setDuration(line[8]);
            if (timeZone != null && timeZone.equals("UTC") && timeZoneAligner != null) {
                timeZone += timeZoneAligner.align(line[9]);
            }
            item.setTimeZone(timeZone);
        } catch (Exception e) {
            logger.error(String.format("%s.%s: Exception: ", className, functionName), e);
            throw e;
        }
        return item;
    }

    @Override
    protected void prepareItems(List<AdherenceRowItem> items) {
        final String functionName = "prepareItems()";
        try{
            List<AdherenceRowItem> orderedItems = new ArrayList<>(items);
            Collections.sort(orderedItems, (o1, o2) -> {
                String agentId1 = o1.getAgentId();
                String agentId2 = o2.getAgentId();
                int res = String.CASE_INSENSITIVE_ORDER.compare(agentId1, agentId2);
                if (res == 0) {
                    res = agentId1.compareTo(agentId2);
                    if (res == 0) {
                        res = o1.getStartDate().compareTo(o2.getStartDate());
                    }
                }
                return res;
            });

            // fill stateIndex
            int index =0;
            for (AdherenceRowItem item : items) {
                item.setStateIndex(String.valueOf(index));
                // fill outStateId
                String agentStateId = item.getAgentStateId();
                if (agentStateId == "1" ||
                    agentStateId == "2" ||
                    agentStateId == "5" ||
                    agentStateId == "7" ||
                    agentStateId == "8" ||
                    agentStateId == "9") {
                        item.setOutStateId(agentStateId);
                }
                index++;
            }
            // fill duration
            AdherenceRowItem prevItem = null;
            for (AdherenceRowItem item : orderedItems) {
                if (prevItem != null && prevItem.getAgentId().equals(item.getAgentId())) {
                    Long startDate1String = Long.parseLong(prevItem.getStartDate());
                    Long startDate2String = Long.parseLong(item.getStartDate());
                    prevItem.setDuration(String.valueOf(Math.abs(startDate1String - startDate2String)));
                }
                prevItem = item;
                prevItem.setDuration("");
            }
            // remove elements with undefined duration
            Predicate<AdherenceRowItem> predicate = p-> p.getDuration() == "";
            items.removeIf(predicate);
        } catch (Exception e) {
            logger.error(String.format("%s.%s: Exception: ", className, functionName), e);
            throw e;
        }
    }

    @Override
    public ReportType getReportType() {
        return ReportType.Adherence;
    }

    @Override
    public int getValidMaxErrors() {
        return 0;
    }

    @Override
    public int getVersion() {
        return 0;
    }
}
