package com.nice.saas.wfo.comp.manage.asc.cisco;

public abstract class TimeZoneAligner {
    public abstract String align(String dateTime);
}
