package com.nice.saas.wfo.comp.manage.asc.cisco;

import com.nice.saas.wfo.comp.manager.asc.generic.Normalizer;
import com.nice.saas.wfo.comp.manager.asc.generic.NormalizerFactory;
import com.nice.saas.wfo.comp.manager.asc.generic.ReportType;

import java.util.HashMap;
import java.util.Map;

public abstract class NormalizerFactoryBase implements NormalizerFactory {
    private Map<ReportType, Normalizer> normalizers;
    private AdherenceNormalizer adherenceNormalizer = new AdherenceNormalizer();

    private ForecastNormalizer forecastNormalizer = new ForecastNormalizer();


    protected String acdType;

    protected TimeZoneAligner timeZoneAligner;

    public void init() {
        normalizers = new HashMap<ReportType, Normalizer>() {{
            adherenceNormalizer.setTimeZoneAligner(timeZoneAligner);
            put(ReportType.Adherence, adherenceNormalizer);
            forecastNormalizer.setTimeZoneAligner(timeZoneAligner);
            put(ReportType.Forecast, forecastNormalizer);
        }};
    }

    @Override
    public Normalizer getNormalizer(ReportType reportType, int version) {
        // TODO: versions
        return normalizers.get(reportType);
    }

    @Override
    public String getAcdType() {
        return acdType;
    }
}