package com.nice.saas.wfo.comp.manage.asc.cisco;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import com.nice.saas.wfo.comp.manager.asc.generic.Normalizer;
import com.nice.saas.wfo.comp.manager.asc.generic.ParsingError;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public abstract class NormalizerBase<T> implements Normalizer {
    final String className = "NormalizerBase";

    private String csvSeparator = Constants.CsvSeparator;

    protected String startDateTime;
    protected String endDateTime;
    protected int intervalDuration;
    protected String timeZone;

    public abstract int getVersion();
    protected GenericLogger logger;
    protected TimeZoneAligner timeZoneAligner;
    public void setTimeZoneAligner(TimeZoneAligner timeZoneAligner) {
        this.timeZoneAligner = timeZoneAligner;
    }


    protected List<T> normalize(String csvString) {
        final String functionName = "normalize()";
        try {
            if (csvString.indexOf(Constants.CsvPartSeparator) < 0) {
                return normalizeCore(csvString);
            } else {
                final List<T> items = new ArrayList<>();
                String[] splitted = csvString.split(Constants.CsvPartSeparator);
                for (String csvStringItem : splitted ) {
                    items.addAll(normalizeCore(csvStringItem));
                }
                return items;
            }
        } catch (Exception e) {
            String errorString = e.getMessage() +" class:"+getClass().getSimpleName()+" ("+getVersion()+")"+" data:"+csvString;
            logger.error(String.format("%s.%s: %s: ", className, functionName, errorString), e);
            throw new ParsingError(errorString, e);
        }
    }

    private List<T> normalizeCore(String csvString) throws IOException {
        final String functionName = "normalize()";
        final List<T> items = new ArrayList<>();
        // extract start/end datetime
        String dtSeparator = "@dt@";
        String[] splitted = csvString.split(dtSeparator);
        csvString = splitted[0];
        startDateTime = splitted[1];
        endDateTime = splitted[2];
        intervalDuration = Integer.parseInt(splitted[3]);
        timeZone = splitted[4];  // initially is equal to 'UTC', will be set finally in mapToItem

        BufferedReader br = new BufferedReader(new StringReader(csvString));
        // skip the header of the csv
        br.lines().skip(1).forEachOrdered(s -> {
            String[] p = s.split(csvSeparator);
            T item = mapToItem(p);
            items.add(item);
        });
        br.close();
        // prepare items for consuming
        prepareItems(items);
        return items;
    }

    protected abstract T mapToItem(String[] line);

    protected abstract void prepareItems(List<T> items);
}
