package com.nice.saas.wfo.comp.manage.asc.cisco;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import com.nice.saas.wfo.comp.manager.asc.generic.ReportType;
import com.nice.saas.wfo.comp.manager.asc.generic.json.ForecastRowItem;

import java.util.Iterator;
import java.util.List;

public class ForecastNormalizer extends NormalizerBase<ForecastRowItem>  {
    final String className = "ForecastNormalizer";

    @Override
    public Iterator getIterator(String csv, GenericLogger genericLogger) {
        logger = genericLogger;
        List<ForecastRowItem> list = normalize(csv);
        return list.iterator();
    }

    @Override
    protected ForecastRowItem mapToItem(String[] line) {
        final String functionName = "mapToItem()";
        ForecastRowItem item = new ForecastRowItem();
        try {
            item.setSkillName(line[0]);
            if (timeZone != null && timeZone.equals("UTC") && timeZoneAligner != null) {
                timeZone += timeZoneAligner.align(line[1]);
            }
            item.setUTCTimeZone(timeZone);
            item.setIntervalDuration(intervalDuration * 60);
            item.setUTCStartDateTime(startDateTime);
            item.setOfferedCalls(Integer.parseInt(line[4]));
            item.setAnsweredCalls(Integer.parseInt(line[5]));
            item.setAbandonedCalls(Integer.parseInt(line[6]));
            item.setAverageHandleTime(Float.parseFloat(line[7]));
            item.setAverageAbandonmentTime(Float.parseFloat(line[8]));
            item.setDials(Integer.parseInt(line[9]));
            item.setConnects(Integer.parseInt(line[10]));
            item.setConnectsAht(Integer.parseInt(line[11]));
            item.setRightPartyConnects(Integer.parseInt(line[12]));
            item.setRightPartyConnectsAht(Integer.parseInt(line[13]));

        } catch (Exception e) {
            logger.error(String.format("%s.%s: Exception: ", className, functionName), e);
            throw e;
        }
        return item;
    }

    @Override
    protected void prepareItems(List<ForecastRowItem> items) {

    }

    @Override
    public ReportType getReportType() {
        return ReportType.Forecast;
    }

    @Override
    public int getValidMaxErrors() {
        return 0;
    }

    @Override
    public int getVersion() {
        return 0;
    }
}
