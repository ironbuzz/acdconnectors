package com.nice.saas.wfo.comp.manage.asc.cisco;

public class Constants {

    /**
     * CSV separator
     */
    public static final String CsvSeparator = "\\|";

    /**
     * CSVs separator (in the case of multiple CSVs)
     */
    public static final String CsvPartSeparator = "#recovery#";
}
