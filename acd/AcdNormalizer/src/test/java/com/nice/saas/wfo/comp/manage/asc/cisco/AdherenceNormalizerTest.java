package com.nice.saas.wfo.comp.manage.asc.cisco;

import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import com.nice.saas.wfo.comp.manager.asc.generic.ReportType;
import com.nice.saas.wfo.comp.manager.asc.generic.json.AdherenceRowItem;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.io.Console;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


public class AdherenceNormalizerTest {
    final static GenericLogger logger = new GenericLogger() {
        @Override
        public void info(String s) {
            System.out.println(s);
        }

        @Override
        public void error(String s, Throwable throwable, Object... objects) {
            System.out.println(s);
        }

        @Override
        public void warning(String s){System.out.println(s);
        }
    };
    @Test
    public void getIterator() {
        AdherenceNormalizer normalizer = new AdherenceNormalizer();
        TimeZoneAligner a = new TimeZoneAligner() {
            @Override
            public String align(String s) {
                if (s == null || s =="") {
                    return "";
                }
                Integer value = Integer.parseInt(s.trim());
                String ret = String.valueOf(value/60);
                if (value == 0) {
                    return "";
                }
                if (value >= 0) {
                    ret = "+" + ret;
                }
                return ret;
            }
        };
        normalizer.setTimeZoneAligner(a);
        String csv ="agentid|(constant)|eventdatetime|eventtype|(constant)|(constant)|(constant)|reasoncode|(constant)|gmtoffset \n 19|0|2017-09-13 07:54:24.847|1|0|0|0|0|0|-180 \n 19|0|2017-09-13 07:54:24.849|2|0|0|0|32760|0|-180\n 19|0|2017-09-13 07:54:44.96|3|0|0|0|0|0|-180\n 19|0|2017-09-13 09:15:59.605|2|0|0|0|32759|0|-180 \n 19|0|2017-09-13 11:52:28.621|7|0|0|0|255|0|-180 \n@dt@09-13-2017 03:12:39@dt@2017-09-14 09:12:39@dt@1800@dt@UTC@dt@";
        Iterator it =normalizer.getIterator(csv, logger);
        int count =0;
        while (it.hasNext()) {
            it.next();
            count ++;
        }
        assertEquals(count, 4);
        assertEquals(normalizer.timeZone, "UTC-3");


    }

    @Test
    public void getIteratorRecovery() {
        AdherenceNormalizer normalizer = new AdherenceNormalizer();
        TimeZoneAligner a = new TimeZoneAligner() {
            @Override
            public String align(String s) {
                if (s == null || s =="") {
                    return "";
                }
                Integer value = Integer.parseInt(s.trim());
                String ret = String.valueOf(value/60);
                if (value == 0) {
                    return "";
                }
                if (value >= 0) {
                    ret = "+" + ret;
                }
                return ret;
            }
        };
        normalizer.setTimeZoneAligner(a);
        String csv ="agentid|(constant)|eventdatetime|eventtype|(constant)|(constant)|(constant)|reasoncode|(constant)|gmtoffset \n 19|0|2017-09-13 07:54:24.847|1|0|0|0|0|0|-180 \n 19|0|2017-09-13 07:54:24.849|2|0|0|0|32760|0|-180\n 19|0|2017-09-13 07:54:44.96|3|0|0|0|0|0|-180\n 19|0|2017-09-13 09:15:59.605|2|0|0|0|32759|0|-180 \n 19|0|2017-09-13 11:52:28.621|7|0|0|0|255|0|-180 \n@dt@09-13-2017 03:12:39@dt@2017-09-14 09:12:39@dt@1800@dt@UTC@dt@#recovery#agentid|(constant)|eventdatetime|eventtype|(constant)|(constant)|(constant)|reasoncode|(constant)|gmtoffset \n 19|0|2017-09-14 09:15:59.605|2|0|0|0|32759|0|-180 \n 19|0|2017-09-14 11:52:28.621|7|0|0|0|255|0|-180 \n@dt@09-13-2017 03:12:39@dt@2017-09-14 09:12:39@dt@1800@dt@UTC@dt@";
        Iterator it =normalizer.getIterator(csv, logger);
        int count =0;
        while (it.hasNext()) {
            it.next();
            count ++;
        }
        assertEquals(count, 5);
        assertEquals(normalizer.timeZone, "UTC-3");


    }

    @Test
    public void getReportType() {
        AdherenceNormalizer normalizer = new AdherenceNormalizer();
        assertEquals(normalizer.getReportType(), ReportType.Adherence);
    }

    @Test
    public void getValidMaxErrors() {
        AdherenceNormalizer normalizer = new AdherenceNormalizer();
        assertEquals(normalizer.getValidMaxErrors(), 0);
    }

    @Test
    public void getVersion() {
        AdherenceNormalizer normalizer = new AdherenceNormalizer();
        assertEquals(normalizer.getVersion(), 0);
    }



    @Test
    public void mapToItem() {
        AdherenceNormalizer normalizer = new AdherenceNormalizer();
        normalizer.timeZone="UTC";
        TimeZoneAligner a = new TimeZoneAligner() {
            @Override
            public String align(String s) {
                if (s == null || s =="") {
                    return "";
                }
                Integer value = Integer.parseInt(s.trim());
                String ret = String.valueOf(value/60);
                if (value == 0) {
                    return "";
                }
                if (value >= 0) {
                    ret = "+" + ret;
                }
                return ret;
            }
        };
        normalizer.setTimeZoneAligner(a);
        String[] item = new String[] {"agentId", "stateIndex", "2017-09-11 12:12:11", "agentStateId",
                "agentSassionId", "skillId", "outStateId", "outStateDescription", "duration", "-180" };
        AdherenceRowItem aItem = normalizer.mapToItem(item);
        assertEquals(item[0], aItem.getAgentId());
        assertEquals(item[1], aItem.getStateIndex());
        assertEquals("1505131931", aItem.getStartDate());
        assertEquals(item[3], aItem.getAgentStateId());
        assertEquals(item[4], aItem.getAgentSessionId());
        assertEquals(item[5], aItem.getSkillId());
        assertEquals(item[6], aItem.getOutStateId());
        assertEquals(item[7], aItem.getOutStateDescription());
        assertEquals(item[8], aItem.getDuration());
        assertEquals("UTC-3", aItem.getTimeZone());
    }

    @Test
    public void prepareItems1() {
        AdherenceRowItem item1 = new AdherenceRowItem();
        item1.setAgentId("3");
        AdherenceRowItem item2 = new AdherenceRowItem();
        item2.setAgentId("1");
        AdherenceRowItem item3 = new AdherenceRowItem();
        item3.setAgentId("2");
        List<AdherenceRowItem> items = new  ArrayList<>(Arrays.asList(item1, item2, item3));
        AdherenceNormalizer normalizer = new AdherenceNormalizer();
        normalizer.prepareItems(items);
        assertEquals(items.size(), 0 );

    }

    @Test
    public void prepareItems2() {
        AdherenceRowItem item1 = new AdherenceRowItem();
        item1.setAgentId("3");
        item1.setStartDate("2");
        AdherenceRowItem item2 = new AdherenceRowItem();
        item2.setAgentId("1");
        item2.setStartDate("2");
        AdherenceRowItem item3 = new AdherenceRowItem();
        item3.setAgentId("2");
        item3.setStartDate("2");
        AdherenceRowItem item4 = new AdherenceRowItem();
        item4.setAgentId("3");
        item4.setStartDate("1");
        AdherenceRowItem item5 = new AdherenceRowItem();
        item5.setAgentId("3");
        item5.setStartDate("3");
        AdherenceRowItem item6 = new AdherenceRowItem();
        item6.setAgentId("2");
        item6.setStartDate("3");
        List<AdherenceRowItem> items =  new  ArrayList<>(Arrays.asList(item1, item2, item3, item4, item5, item6));
        AdherenceNormalizer normalizer = new AdherenceNormalizer();
        normalizer.prepareItems(items);
        assertEquals(items.size(), 3);
        assertEquals(items.get(0).getStateIndex(), "0");
        assertEquals(items.get(0).getAgentId(), "3");
        assertEquals(items.get(0).getDuration(), "1");
//        assertEquals(items.get(1).getStateIndex(), "1");
//        assertEquals(items.get(1).getAgentId(), "1");
//        assertEquals(items.get(1).getDuration(), "");
        assertEquals(items.get(1).getStateIndex(), "2");
        assertEquals(items.get(1).getAgentId(), "2");
        assertEquals(items.get(1).getDuration(), "1");
        assertEquals(items.get(2).getStateIndex(), "3");
        assertEquals(items.get(2).getAgentId(), "3");
        assertEquals(items.get(2).getDuration(), "1");
//        assertEquals(items.get(4).getStateIndex(), "4");
//        assertEquals(items.get(4).getAgentId(), "3");
//        assertEquals(items.get(4).getDuration(), "");
//        assertEquals(items.get(5).getStateIndex(), "5");
//        assertEquals(items.get(5).getAgentId(), "2");
//        assertEquals(items.get(5).getDuration(), "");
    }
}