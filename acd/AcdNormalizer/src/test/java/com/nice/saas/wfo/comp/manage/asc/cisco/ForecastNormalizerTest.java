package com.nice.saas.wfo.comp.manage.asc.cisco;

import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import com.nice.saas.wfo.comp.manager.asc.generic.ReportType;
import com.nice.saas.wfo.comp.manager.asc.generic.json.ForecastRowItem;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;

public class ForecastNormalizerTest {
    final static GenericLogger logger = new GenericLogger() {
        @Override
        public void info(String s) {
            System.out.println(s);
        }

        @Override
        public void error(String s, Throwable throwable, Object... objects) {
            System.out.println(s);
        }

        @Override
        public void warning(String s){System.out.println(s);
        }
    };
    @Test
    public void getIterator() {
        ForecastNormalizer normalizer = new ForecastNormalizer();
        String csv ="skillname|utctimezone|intervalduration|utcstartdatetime|offeredcalls|answeredcalls|abandonedcalls|averagehandletime|averageabandonmenttime|dials|connects|connectsaht|rightpartyconnects|rightpartyconnectsaht\nDials|0|0|0|0|0|0|0|0|0|0|0|0|0\nRPDials|0|0|0|0|0|0|0|0|0|0|0|0|0\n@dt@09-13-2017 03:36:45@dt@2017-09-14 09:36:45@dt@1800@dt@UTC+03:00@dt@";
        Iterator it =normalizer.getIterator(csv, logger);
        int count =0;
        while (it.hasNext()) {
            it.next();
            count ++;
        }
        assertEquals(count, 2);

    }



    @Test
    public void getReportType() {
        ForecastNormalizer normalizer = new ForecastNormalizer();
        assertEquals(normalizer.getReportType(), ReportType.Forecast);
    }

    @Test
    public void getValidMaxErrors() {
        ForecastNormalizer normalizer = new ForecastNormalizer();
        assertEquals(normalizer.getValidMaxErrors(), 0);
    }

    @Test
    public void getVersion() {
        ForecastNormalizer normalizer = new ForecastNormalizer();
        assertEquals(normalizer.getVersion(), 0);
    }



    @Test
    public void mapToItem() {
        ForecastNormalizer normalizer = new ForecastNormalizer();
        normalizer.intervalDuration = 1000;
        normalizer.startDateTime = "startDate";
        String[] item = new String[] {"skillName", "timeZone", "1000", "startDate",
                "10", "11", "12", "13.0", "14.0", "15", "16","17","18","19" };
        ForecastRowItem aItem = normalizer.mapToItem(item);
        assertEquals(item[0], aItem.getSkillName());
        assertEquals(null, aItem.getUTCTimeZone());
        assertEquals(item[2], String.valueOf(normalizer.intervalDuration));
        assertEquals(item[3], normalizer.startDateTime );
        assertEquals(item[4], String.valueOf(aItem.getOfferedCalls()));
        assertEquals(item[5], String.valueOf(aItem.getAnsweredCalls()));
        assertEquals(item[6], String.valueOf(aItem.getAbandonedCalls()));
        assertEquals(item[7], String.valueOf(aItem.getAverageHandleTime()));
        assertEquals(item[8], String.valueOf(aItem.getAverageAbandonmentTime()));
        assertEquals(item[9], String.valueOf(aItem.getDials()));
        assertEquals(item[10], String.valueOf(aItem.getConnects()));
        assertEquals(item[11], String.valueOf(aItem.getConnectsAht()));
        assertEquals(item[12], String.valueOf(aItem.getRightPartyConnects()));
        assertEquals(item[13], String.valueOf(aItem.getRightPartyConnectsAht()));
    }

}