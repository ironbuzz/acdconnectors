package com.nice.saas.wfo.comp.manage.asc.cisco;

import com.nice.saas.wfo.comp.manager.asc.generic.Normalizer;
import com.nice.saas.wfo.comp.manager.asc.generic.ReportType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Map;

import static org.junit.Assert.*;

public class NormalizerFactoryBaseTest {
    @Before
    public void setUp() throws Exception {


        f.init();
        f.acdType = "type1";
    }

    NormalizerFactoryBase f = new NormalizerFactoryBase(){};

    @Test
    public void init() throws Exception {
        Field field = f.getClass().getSuperclass().getDeclaredField("normalizers");
        field.setAccessible(true);
        Map<ReportType, Normalizer> normalizers = (Map<ReportType, Normalizer> )field.get(f);
        assertNotNull(normalizers);
        assertEquals(normalizers.size(), 2);
    }

    @Test
    public void getNormalizer() throws Exception {
        AdherenceNormalizer an = (AdherenceNormalizer)f.getNormalizer(ReportType.Adherence, 0);
        assertNotNull(an);
        ForecastNormalizer fn = (ForecastNormalizer)f.getNormalizer(ReportType.Forecast, 0);
        assertNotNull(fn);
    }

    @Test
    public void getAcdType() throws Exception {
        assertEquals(f.getAcdType(), "type1");
    }

}