package com.nice.saas.wfo.comp.manage.asc.cisco;

import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import com.nice.saas.wfo.comp.manager.asc.generic.ReportType;
import org.junit.Test;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;



public class NormalizerBaseTest {
    @Test
    public void getVersion() {
    }

    @Test
    public void normalize() {
    }

    @Test
    public void mapToItem() {
    }

    @Test
    public void prepareItems() {
    }

    @Test
    public void testNormalizerbase() {
        NormalizerBase<Integer> normalizerTest = new NormalizerBase<Integer>() {
            @Override
            public int getVersion() {
                return 0;
            }

            @Override
            protected Integer mapToItem(String[] line) {
                return line.length;
            }

            @Override
            protected void prepareItems(List items) {
                items.sort(new Comparator<Integer>() {
                    @Override
                    public int compare(Integer o1, Integer o2) {
                        return o1.compareTo(o2);
                    }
                });

            }

            @Override
            public ReportType getReportType() {
                return ReportType.Adherence;
            }

            @Override
            public Iterator getIterator(String s, GenericLogger genericLogger) {
                return null;
            }

            @Override
            public int getValidMaxErrors() {
                return 0;
            }

        };
        assertEquals(normalizerTest.getVersion(), 0);
        assertEquals(normalizerTest.getValidMaxErrors(), 0);

        assertEquals(normalizerTest.getIterator("q", null), null);
        assertEquals(normalizerTest.mapToItem(new String[]{"q", "w"}), java.util.Optional.of(2).get());
    }



}