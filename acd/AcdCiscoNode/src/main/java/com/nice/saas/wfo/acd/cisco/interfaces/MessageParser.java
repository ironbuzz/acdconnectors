package com.nice.saas.wfo.acd.cisco.interfaces;

import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;

/**
 * Message parser, parses raw message of type T to Message
 * @param <T>
 */
public interface MessageParser <T> {
    public Message parse(T rawMessage);
    void addLogger(GenericLogger logger);
}
