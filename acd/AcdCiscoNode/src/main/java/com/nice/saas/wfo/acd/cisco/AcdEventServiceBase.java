package com.nice.saas.wfo.acd.cisco;

import com.nice.saas.wfo.acd.cisco.common.GenericACDEventComparator;
import com.nice.saas.wfo.acd.cisco.interfaces.GenericAcdEventTranslator;
import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import com.nice.saas.wfo.acd.cisco.interfaces.MessageParser;
import com.nice.saas.wfo.acd.cisco.interfaces.Receiver;
import com.nice.saas.wfo.acd.rta.GenericACDEvent;
import com.nice.saas.wfo.acd.rta.GenericACDEventListener;
import com.nice.saas.wfo.acd.rta.GenericACDEventService;

import com.nice.saas.wfo.comp.manager.asc.generic.ACDErrorHandler;
import com.nice.saas.wfo.comp.manager.asc.generic.ConnectionError;
import com.nice.saas.wfo.comp.manager.asc.generic.GeneralError;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * ACD event service base class
 */
public abstract class AcdEventServiceBase implements GenericACDEventService, DisposableBean, ActionListener {

    // region private/protected fields

    private final String className = "AcdEventServiceBase";

    public AtomicBoolean stopped = new AtomicBoolean(false);
    @Value("${acd.rta.acdEventService.messageQueueCapacity}")
    private int messageQueueCapacity;
    protected BlockingQueue<ByteBuffer> messages;

    @Value("${acd.rta.acdEventService.eventExecutorPoolSize}")
    private int eventExecutorPoolSize;
    private ExecutorService eventsProcessorPool;
    private final ExecutorService receiverPool = Executors.newSingleThreadExecutor();
    private final ExecutorService senderPool = Executors.newSingleThreadExecutor();
    private final ExecutorService eventBulkProcessorPool = Executors.newSingleThreadExecutor();
    public final ScheduledExecutorService timer = Executors.newSingleThreadScheduledExecutor();
    private AtomicBoolean isStarted = new AtomicBoolean(false);
    @Value("${acd.rta.acdEventService.processEventsByBulks}")
    private boolean processEventsByBulks;
    private BlockingQueue<GenericACDEvent> events;
    @Value("${acd.rta.acdEventService.eventProcessBulkSize}")
    private int eventProcessBulkSize;
    private List<GenericACDEvent> processEventList = new ArrayList<>();
    private final GenericACDEventComparator genericACDEventComparator = new GenericACDEventComparator();
    private final ConcurrentMap<String, GenericACDEvent> eventMap = new ConcurrentHashMap<>();

    protected GenericLogger logger;

    public Receiver receiver;

    public MessageParser messageParser;

    public GenericAcdEventTranslator acdMessageTranslator;

    @Autowired
    protected ACDErrorHandler acdErrorHandler;

    private final List<GenericACDEventListener> acdEventListeners = new ArrayList<>();

    // endregion private/protected fields

    // region post construct

    @PostConstruct
    protected void init() {
        messages = new ArrayBlockingQueue<>(messageQueueCapacity);
        events = new ArrayBlockingQueue<>(messageQueueCapacity);
        eventsProcessorPool = Executors.newFixedThreadPool(eventExecutorPoolSize);
        receiver.addListener(this);
    }

    // endregion post construct

    // region GenericAcdEventService implementation

    @Override
    public void addEventListener(GenericACDEventListener eventListener) {
        acdEventListeners.add(eventListener);
    }

    @Override
    public void addLogger(GenericLogger logger) {
        this.logger = logger;
        this.receiver.addLogger(logger);
        this.acdMessageTranslator.addLogger(logger);
        this.messageParser.addLogger(logger);
    }


    @Override
    public void start() {
        final String functionName = "start()";

        try {
            if (isStarted.get() == true) {
                logger.warning(String.format("%s.%s: ACD service is already started, exiting", className, functionName));
                return;
            }
            isStarted.set(true);
            receiver.startListening();
            logger.info(String.format("%s.%s: Start receiver.startListening", className, functionName));
        } catch (Exception e) {
            isStarted.set(false);
            String errorString = String.format("%s.%s: Unable to start receiver.startListening", className, functionName);
            logger.error(errorString, e);
            acdErrorHandler.connectionError(new ConnectionError(errorString, e));
        }
        // start receiver pool
        startReceiveMessages();
        // start events processing pool
        startEventsProcessorPool();
        // start event bulks processor pool if processEventsByBulks is true
        if (processEventsByBulks == true) {
            startEventBulkProcessorPool();
        }
    }

    /**
     * Start receiver pool
     */
    private void startReceiveMessages(){
        final String functionName = "startReceiveMessages()";
        // receiving events/messages as raw byte data from server
        receiverPool.submit(() -> {
            logger.info(String.format("%s.%s: Starting receiver pool", className, functionName));
            while (!stopped.get()) {
                try {
                    List<ByteBuffer> buffers = receiver.receive();
                    if (buffers == null) {
                        continue;
                    }
                    buffers.forEach(item -> {
                        try {
                            messages.put(item);
                        } catch (InterruptedException e) {
                            logger.error(String.format("%s.%s: Error in receiver loop (InterruptedException)", className, functionName),e);
                        }
                    });
                } catch (Exception e) {
                    logger.error(String.format("%s.%s: Error in receiver loop", className, functionName),e);
                }
            }
        });
    }

    /**
     * Start events processing pool
     */
    private void startEventsProcessorPool(){
        final String functionName = "startEventsProcessorPool()";
        // processing of events/messages received from server
        eventsProcessorPool.submit(() -> {
            logger.info(String.format("%s.%s: Starting events processor pool", className, functionName));
            while (!stopped.get()) {
                Message message = null;
                ByteBuffer buffer = null;
                try {
                    buffer = messages.take();
                    message = messageParser.parse(buffer);
                    if (message == null) {
                        continue;
                    }
                } catch (Exception e) {
                    logger.error(String.format("%s.%s: Error in events processor loop", className, functionName),e);
                    continue;
                }

                GenericACDEvent event = null;
                try {
                    event = acdMessageTranslator.translate(message);
                } catch (GeneralError e) {
                    String errorMessage = String.format("%s.%s: Error during message translation. message ID is: %s", className, functionName, message.toString());
                    logger.error(errorMessage, e);
                    acdErrorHandler.generalError(e);
                }
                if (event != null) {

                    GenericACDEvent finalEvent = event;
                    try {
                        // check if new event is duplicate, if yes, don't process it
                        String agentId = event.getLoginId();
                        if (eventMap.containsKey(agentId)) {
                            if (genericACDEventComparator.compare(eventMap.get(agentId), event) == 0) {
                                continue;
                            }
                        }
                        eventMap.put(agentId, event);
                        if (processEventsByBulks == false) {
                            logger.info(String.format("%s.%s: Processing agent state event: agentId = %s, acdId = %s, event code: %s, agent state/reason: %s, timestamp: %s",
                                    className, functionName, event.getLoginId(), event.getAcdId(), event.getEventCode(), event.getReasonCode(), event.getEventUtcTime()));
                            acdEventListeners.forEach(item -> item.processEvent(finalEvent));

                        } else {
                            events.put(finalEvent);
                        }
                    } catch (Exception e) {
                        String errorMessage = String.format("%s.%s: Error during processEvent: %s", className, functionName, e.getMessage());
                        logger.error(errorMessage, e);
                        acdErrorHandler.generalError(new GeneralError(e.getMessage(), e));
                    }

                    // got and processed agent state event, continue to next messages
                    continue;
                }
                try {
                    eventsProcessorPoolCore(message);
                } catch (Exception e) {
                    isStarted.set(false);
                    String errorString = String.format("%s.%s: Error during service message processing. message ID is: %s", className, functionName, message.toString());
                    logger.error(errorString, e);
                    acdErrorHandler.generalError(new GeneralError(errorString, e));
                }
            }});
    }

    private void startEventBulkProcessorPool (){
        final String functionName = "startEventBulkProcessorPool()";
        // processing of event bulks prepared by event processor pool
        eventBulkProcessorPool.submit(() -> {
                    logger.info(String.format("%s.%s: Starting event bulk processor pool", className, functionName));
            while (!stopped.get()) {
                while ( processEventList.size() < eventProcessBulkSize) {
                    try {
                        GenericACDEvent event =events.take();
                        logger.info(String.format("%s.%s: Adding agent state event to bulk for processing: agentId = %s, acdId = %s, event code: %s, agent state/reason: %s, timestamp: %s",
                                    className, functionName, event.getLoginId(), event.getAcdId(), event.getEventCode(), event.getReasonCode(), event.getEventUtcTime()));
                        processEventList.add(event);
                    } catch (Exception e) {
                        String errorMessage = String.format("%s.%s: Error during preparing event bulk: %s", className, functionName, e.getMessage());
                        logger.error(errorMessage, e);
                        acdErrorHandler.generalError(new GeneralError(e.getMessage(), e));
                    }
                }
                logger.info(String.format("%s.%s: Sending event bulk for processing", className, functionName));
                acdEventListeners.forEach(item -> item.processEvents(processEventList));
                processEventList.clear();
            }
        });
    }


    // endregion GenericAcdEventService implementation

    // region protected abstract methods

    /**
     * Sender pool concrete implementation should be provided by child class
     */
    protected abstract void senderPoolCore();

    /**
     * Event processor pool concrete implementation should be provided by child class
     * @param message
     */
    protected  abstract void eventsProcessorPoolCore(Message message);

    // endregion protected abstract methods

    // region destroy implementation

    /**
     * Invoked by a BeanFactory on destruction of a singleton.
     *
     * @throws Exception in case of shutdown errors.
     *                   Exceptions will get logged but not rethrown to allow
     *                   other beans to release their resources too.
     */
    @Override
    public void destroy() {
        final String functionName = "destroy()";
        try {
            stopped.set(true);
            if (receiver != null) {
                receiver.stop();
            }
            logger.info(String.format("%s.%s: Terminating receiverPool executor service ...", className, functionName));
            shutdownAndAwaitTermination(receiverPool);
            logger.info(String.format("%s.%s: receiverPool executor service terminated", className, functionName));
            logger.info(String.format("%s.%s: Terminating senderPool executor service ...", className, functionName));
            shutdownAndAwaitTermination(senderPool);
            logger.info(String.format("%s.%s: senderPool executor service terminated", className, functionName));
            shutdownAndAwaitTermination(timer);
            logger.info(String.format("%s.%s: Terminating timer executor service ...", className, functionName));
            logger.info(String.format("%s.%s: timer executor service terminated", className, functionName));
            logger.info(String.format("%s.%s: Terminating eventsProcessorPool executor service ...", className, functionName));
            shutdownAndAwaitTermination(eventsProcessorPool);
            if (processEventsByBulks == true) {
                logger.info(String.format("%s.%s: Terminating eventBulkProcessorPool executor service ...", className, functionName));
                shutdownAndAwaitTermination(eventBulkProcessorPool);
            }
            logger.info(String.format("%s.%s: eventsProcessorPool executor service terminated", className, functionName));
        }
        catch (Exception e) {
          logger.error(String.format("%s.%s: Exception: ", className, functionName), e);
        } finally {
            isStarted.set(false);
        }

    }

    private void shutdownAndAwaitTermination(ExecutorService pool) {
        final String functionName = "shutdownAndAwaitTermination()";
        if (pool == null) {
            return;
        }
        pool.shutdownNow(); // Disable new tasks from being submitted
//        try {
//            // Wait a while for existing tasks to terminate
//            if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
//                pool.shutdownNow(); // Cancel currently executing tasks
//                // Wait a while for tasks to respond to being cancelled
//                if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
//                    logger.warning(String.format("%s.%s: Pool did not terminate", className, functionName));
//                }
//            }
//        } catch (InterruptedException ie) {
//            // (Re-)Cancel if current thread also interrupted
//            pool.shutdownNow();
//            // Preserve interrupt status
//            Thread.currentThread().interrupt();
//        }
    }

    // endregion

    // region action listener

    /**
     * Starts sender loop on client connected event
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        final String functionName = "actionPerformed()";
        switch (e.getActionCommand()) {
            case "connected":
                senderPool.submit(() -> {
                    try {
                        senderPoolCore();
                    } catch (Exception ex) {
                        logger.error(String.format("%s.%s: Error in the sender pool: ", className, functionName), ex);
                    }
                });
                break;
            case "disconnected":
                String errorString = String.format("%s.%s: TCP client disconnected, reconnection failed", className, functionName);
                ConnectionError error = new ConnectionError(errorString);
                logger.error(errorString, error);
                acdErrorHandler.connectionError(error);
                isStarted.set(false);
                break;
            case "failure":
                if (e.getSource() instanceof  ConnectionError) {
                    acdErrorHandler.connectionError((ConnectionError) e.getSource());
                } else if (e.getSource() instanceof  GeneralError) {
                    acdErrorHandler.generalError((GeneralError) e.getSource());
                }
                isStarted.set(false);
                break;
            default:
                break;
        }
    }

    // endregion
}
