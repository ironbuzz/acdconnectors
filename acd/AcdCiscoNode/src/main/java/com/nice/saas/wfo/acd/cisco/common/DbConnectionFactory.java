package com.nice.saas.wfo.acd.cisco.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnectionFactory {
    private static DbConnectionFactory instance;
    private static boolean recovery = false;
    private static Connection connection;
    public Connection getConnection() {
        return connection;
    }
    private DbConnectionFactory(String dbClassName, String url, String username, String password) throws ClassNotFoundException, SQLException {
        Class.forName(dbClassName);
        connection = DriverManager.getConnection(url, username, password);
    }

    public static synchronized void destroyInstance() {
        instance = null;
        recovery = true;
    }

    public static DbConnectionFactory getInstance(String dbClassName, String url, String username, String password) throws SQLException, ClassNotFoundException {
        if(instance == null){
            synchronized (DbConnectionFactory.class) {
                if(instance == null){
                    if(recovery){
                        System.gc();
                        try {
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {  e.printStackTrace(); }
                    }
                    instance = new DbConnectionFactory(dbClassName, url, username, password);
                }
            }
        }
        return instance;
    }
}
