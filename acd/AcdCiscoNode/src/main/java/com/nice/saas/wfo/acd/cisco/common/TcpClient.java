package com.nice.saas.wfo.acd.cisco.common;

import com.nice.saas.wfo.comp.manager.asc.generic.ConnectionError;
import com.nice.saas.wfo.comp.manager.asc.generic.GeneralError;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketOption;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;


/**
 * Abstract TCP client
 */
public abstract class TcpClient implements Runnable {

    private GenericLogger logger;

    // region constants

    /**
     * Default reconnection interval
     */
    private static final long DefaultReconnectInterval = 500;
    /**
     * Default read buffer size
     */
    private static final int DefaultReadBufferSize = 0x100000;
    /**
     * Default write buffer size
     */
    private static final int DefaultWriteBufferSize = 0x100000;
    /**
     * Default reconnect timeout
     */
    private static final int DefaultReconnectRetries = 20;
    /**
     * Defaul host address
     */
    private static final String DefaultHost = "127.0.0.1";
    /**
     * Default port number
     */
    private static final int DefaultPort = 8080;
    /**
     * The thread to run TCP client's event loop
     */
    private final Thread thread = new Thread(this);

    /**
     * Atomic boolean, can be used to stop the TCP client's event loop
     */
    private AtomicBoolean stopped = new AtomicBoolean(false);
    // endregion

    // region private members
    private final String className = "TcpClient";
    /**
     * Reconnection interval
     */
    private final long reconnectInterval;
    /**
     * Reconnection timeout (ms)
     */
    private final long reconnectRetryNumber;

    /**
     * Reconnection retry count
     */
    private AtomicLong reconnectionRetryCount = new AtomicLong(0);
    /**
     * Read buffer size
     */
    private final int readBufferSize;
    /**
     * Write buffer size
     */
    private final int writeBufferSize;
    /**
     * TCP server host name
     */
    private String host;
    /**
     * TCP server port number
     */
    private int port;
    /**
     * TCP server endpoint address (port and host combined)
     */
    private InetSocketAddress address;
    /**
     * Used to switch between operations (read, write, etc)
     */
    private Selector selector;
    /**
     * Socket channel for communication with TCP server
     */
    private SocketChannel channel;
    /**
     * Atomic boolean indicating the client connection status
     */
    private final AtomicBoolean connected = new AtomicBoolean(false);
    /**
     * TCP client read buffer
     */
    private ByteBuffer readBuffer;
    /**
     * TCP client write buffer
     */
    private ByteBuffer writeBuffer;

    private boolean isInitialRun = true;
    // endregion

    // region construction

    public TcpClient() {
        reconnectInterval = DefaultReconnectInterval;
        reconnectRetryNumber = DefaultReconnectRetries;
        readBufferSize = DefaultReadBufferSize;
        writeBufferSize = DefaultWriteBufferSize;
        this.host = DefaultHost;
        this.port = DefaultPort;
        init();
    }

    public TcpClient(String host, int port) {
        reconnectInterval = DefaultReconnectInterval;
        reconnectRetryNumber = DefaultReconnectRetries;
        readBufferSize = DefaultReadBufferSize;
        writeBufferSize = DefaultWriteBufferSize;
        this.host = host;
        this.port = port;
        init();
    }

    public TcpClient(String host, int port, long reconnectInterval, long reconnectRetryNumber) {
        this.reconnectInterval = reconnectInterval;
        this.reconnectRetryNumber = reconnectRetryNumber;
        readBufferSize = DefaultReadBufferSize;
        writeBufferSize = DefaultWriteBufferSize;
        this.host = host;
        this.port = port;
        init();
    }

    public TcpClient(String host, int port, long reconnectInterval, long reconnectRetryNumber, int readBufferSize, int writeBufferSize) {
        this.reconnectInterval = reconnectInterval;
        this.reconnectRetryNumber = reconnectRetryNumber;
        this.readBufferSize = readBufferSize;
        this.writeBufferSize = writeBufferSize;
        this.host = host;
        this.port = port;
        init();
    }

    private void init() {
        if (host == null) {
            host = DefaultHost;
        }
        if (port == 0) {
            port = DefaultPort;
        }
        address = new InetSocketAddress(host, port);
        readBuffer = ByteBuffer.allocateDirect(readBufferSize); // 1Mb
        writeBuffer = ByteBuffer.allocateDirect(writeBufferSize); // 1Mb
    }
    // endregion

    // region Runnable interface implementation

    /**
     * TCP client reconnection and events loop
     */
    public void run()  {

        final String functionName = "run()";

        //logger.info(String.format("%s.%s: Event loop is running", className, functionName));
        try {
            // if the thread was not interrupted and stopped is false, TCP client will try to reconnect continuously
            while(! Thread.interrupted() && (!stopped.get())) {
                try {
                    selector = Selector.open();
                    channel = SocketChannel.open();
                    configureChannel(channel);
                    channel.connect(address);
                    channel.register(selector, SelectionKey.OP_CONNECT);
                    isInitialRun = false;
                    // events loop (get events from opened channel and react accordingly)
                    while(!thread.isInterrupted() && channel.isOpen()) { // events multiplexing loop
                        if (selector.select() > 0) processSelectedKeys(selector.selectedKeys());
                    }
                } catch (Exception e) {
                        if (isInitialRun) {
                            throw new ConnectionError(String.format("%s.%s: Cannot connect TCP client, check network availability. Host: %s, port: %s", className, functionName, host, port));
                        }
                } finally {
                    connected.set(false);
                    writeBuffer.clear();
                    readBuffer.clear();
                    if (channel != null) {
                        channel.close();
                    }
                    if (selector != null) {
                        selector.close();
                    }
                  //  logger.info(String.format("%s.%s: Connection closed", className, functionName));
                }

                try {
                    if (reconnectionRetryCount.incrementAndGet() >= reconnectRetryNumber) {
                        onDisconnected();
                    } else {
                        logger.info(String.format("%s.%s: Reconnecting to: %s. Retry number %s from %s", className, functionName, address,reconnectionRetryCount.get(), reconnectRetryNumber ));
                    }
                    Thread.sleep(reconnectInterval);
                   // logger.info(String.format("%s.%s: Reconnecting to: %s", className, functionName, address));
                } catch (InterruptedException e) {
                   // logger.info(String.format("%s.%s: InterruptedException", className, functionName));
                    break;
                }
            }
        }
        catch (Exception e) {
            try {
                onFailure(e);
            } catch (Exception e1) {

            }
        }

       // logger.info(String.format("%s.%s: Event loop terminated", className, functionName));

    }

    public void addLogger(GenericLogger logger) {
        this.logger = logger;
    }
    // endregion

    // region abstract methods

    /**
     * Should be implemented by concrete TCP client class (process data read by TCP client)
     * @param buf: buffer containing data read
     * @throws Exception
     */
    protected abstract void onRead(ByteBuffer buf) throws Exception;

    /**
     * Should be implemented by concrete TCP client class (connection to server is established)
     * @throws Exception
     */
    protected abstract void onConnected() throws Exception;

    /**
     * Should be implemented by concrete TCP client class (disconnected from server)
     * @throws Exception
     */
    protected abstract void onDisconnected() throws Exception;

    /**
     * Should be implemented by concrete TCP client class (failure)
     * @throws Exception
     */
    protected abstract void onFailure(Exception e) throws Exception;

    // endregion

    // region private methods

    /**
     * Configure TCP client
     * @param channel
     * @throws IOException
     */
    private void configureChannel(SocketChannel channel) throws IOException {
        channel.configureBlocking(false);
        channel.socket().setSendBufferSize(0x100000); // 1Mb
        channel.socket().setReceiveBufferSize(0x100000); // 1Mb
        channel.socket().setKeepAlive(true);
        channel.socket().setReuseAddress(true);
        channel.socket().setSoLinger(false, 0);
        channel.socket().setSoTimeout(0);
        channel.socket().setTcpNoDelay(true);
    }

    /**
     * Process selected keys (act accordingly on connect/read/write etc)
     * @param keys
     * @throws Exception
     */
    private void processSelectedKeys(Set keys) throws Exception {
        Iterator itr = keys.iterator();
        while (itr.hasNext()) {
            SelectionKey key = (SelectionKey) itr.next();
            if (key.isReadable()) processRead(key);
            if (key.isWritable()) processWrite(key);
            if (key.isConnectable()) processConnect(key);
            if (key.isAcceptable()) ;
            itr.remove();
        }
    }


    /**
     * Is called when the connection established
     * @param key
     * @throws Exception
     */
    private void processConnect(SelectionKey key) throws Exception {
        final String functionName = "processConnect()";
        SocketChannel ch = (SocketChannel) key.channel();
        if (ch.finishConnect()) {
           // logger.info(String.format("Connected to %s", address));
            //logger.info(String.format("%s.%s: Connected to: %s", className, functionName, address));
            key.interestOps(key.interestOps() ^ SelectionKey.OP_CONNECT);
            key.interestOps(key.interestOps() | SelectionKey.OP_READ);
            connected.set(true);
            onConnected();
        }
    }


    /**
     * Is called when data is ready to be read
     * @param key
     * @throws Exception
     */
    private void processRead(SelectionKey key) throws Exception {
        final String functionName = "processRead()";
        ReadableByteChannel ch = (ReadableByteChannel)key.channel();

        Integer bytesOp = 0, bytesTotal = 0;
        while (readBuffer.hasRemaining() && (bytesOp = ch.read(readBuffer)) > 0) {
            bytesTotal += bytesOp;
        }

        if (bytesOp == -1) {
            //logger.info(String.format("%s.%s: Peer closed read channel", className, functionName));
            ch.close();
        }
        readBuffer.rewind();
        byte[] tempArray = new byte[bytesTotal];
        readBuffer.get(tempArray, 0, bytesTotal);
        ByteBuffer tempBuffer = ByteBuffer.wrap(tempArray);
        tempBuffer.rewind();
        onRead(tempBuffer);
       // logger.info(String.format("%s.%s: %d bytes read", className, functionName, bytesTotal));
        readBuffer.clear();
     }

    /**
     * Is called when data is ready to be written
     * @param key
     * @throws IOException
     */
    private void processWrite(SelectionKey key) throws IOException {
        final String functionName = "processWrite()";
        WritableByteChannel ch = (WritableByteChannel)key.channel();
        synchronized (writeBuffer) {
            writeBuffer.flip();

            int bytesOp = 0, bytesTotal = 0;
            while (writeBuffer.hasRemaining() && (bytesOp = ch.write(writeBuffer)) > 0) {
                bytesTotal += bytesOp;
            }


            if (writeBuffer.remaining() == 0) {
                key.interestOps(key.interestOps() ^ SelectionKey.OP_WRITE);
            }

            if (bytesTotal > 0) writeBuffer.notify();
            else if (bytesOp == -1) {
                //logger.info(String.format("%s.%s: Peer closed write channel", className, functionName));
                ch.close();
            }
            writeBuffer.compact();
        }
    }


    // endregion

    // region public methods

    /**
     * Start the client reconnection/events loop
     * @throws IOException
     */
    public void start() throws IOException {
        final String functionName = "start()";
        //logger.info(String.format("%s.%s: Starting event loop", className, functionName));
        thread.start();
    }

    /**
     * Stop the client reconnection/events loop
     */
    public void stop() {
        final String functionName = "stop()";
       // logger.info(String.format("%s.%s: Stopping event loop", className, functionName));
        stopped.set(true);
        thread.interrupt();
    }

    /**
     * IsConnected property
     * @return
     */
    public boolean isConnected() {
        return connected.get();
    }

    /**
     * Send data to TCP server
     * @param buffer
     * @throws InterruptedException
     * @throws IOException
     */
    public void send(ByteBuffer buffer) throws InterruptedException, IOException {
        final String functionName = "send()";
        if (!connected.get()) throw new IOException("Not connected");
        synchronized (writeBuffer) {
            // try direct write of what's in the buffer to free up space
            if (writeBuffer.remaining() < buffer.remaining()) {
                writeBuffer.flip();
                int bytesOp = 0, bytesTotal = 0;
                while (writeBuffer.hasRemaining() && (bytesOp = channel.write(writeBuffer)) > 0)  {
                    bytesTotal += bytesOp;
                }
                //logger.info(String.format("%s.%s: %d bytes sent", className, functionName, bytesTotal));
                writeBuffer.compact();
            }

            // if didn't help, wait till some space appears
            if (Thread.currentThread().getId() != thread.getId()) {
                while (writeBuffer.remaining() < buffer.remaining()) writeBuffer.wait();
            }
            else {
                if (writeBuffer.remaining() < buffer.remaining()) throw new IOException("Send buffer full");
            }
            writeBuffer.put(buffer);


            // try direct write to decrease the latency
            writeBuffer.flip();
            int bytesOp = 0, bytesTotal = 0;
            while (writeBuffer.hasRemaining() && (bytesOp = channel.write(writeBuffer)) > 0) bytesTotal += bytesOp;
           // logger.info(String.format("%s.%s: %d bytes sent", className, functionName, bytesTotal));
            writeBuffer.compact();

            if (writeBuffer.hasRemaining()) {
                SelectionKey key = channel.keyFor(selector);
                key.interestOps(key.interestOps() | SelectionKey.OP_WRITE);
                selector.wakeup();
            }
        }
    }

    //endregion
}
