package com.nice.saas.wfo.acd.cisco.common;

import com.nice.saas.wfo.acd.cisco.interfaces.Receiver;
import com.nice.saas.wfo.comp.manager.asc.generic.GeneralError;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.*;

/**
 * TCP receiver base class (should be extended by receivers based on TCP connection)
 */
public abstract class TcpReceiverBase implements Receiver, DisposableBean {

    // region constants

    /**
     * Blocking queue of buffers received from TCP client
     */
    protected BlockingQueue<ByteBuffer> buffers = new ArrayBlockingQueue<>(Constants.DefaultBuffersQueueCapacity);
    private final String className = "TcpReceiverBase";

    protected GenericLogger logger;

    /**
     * Buffers queue capacity (can be set in parameters file)
     * @param buffersQueueCapacity
     */
    @Value("${acd.rta.tcpReceiver.buffersQueueCapacity}")
    public void setBuffersQueueCapacity (int buffersQueueCapacity) {
        buffers = new ArrayBlockingQueue<>(buffersQueueCapacity);
    }

    /**
     * Current payload length (contains currently processed data of current message)
     */
    protected int currentPayloadLength;
    /**
     * Currently read data of current message
     */
    protected ByteBuffer buffer;

    /**
     * TCP receiver host (can be set in parameters file)
     */
    @Value("${acd.rta.tcpReceiver.host}")
    private String host;


    /**
     * TCP receiver port number (can be set in parameters file)
     */
    @Value("${acd.rta.tcpReceiver.port}")
    private int port;

    /**
     * Reconnect interval
     */
    @Value("${acd.rta.retry.intervalDurationMs}")
    private long reconnectInterval;

    /**
     * Reconnect retry number
     */
    @Value("${acd.rta.retry.iterations}")
    private long reconnectRetryNumber;



    // endregion

    // region private members


    /**
     * TCP client used by current receiver
     */
    private TcpClient client;
    private synchronized TcpClient getClient() {
        if (client == null) {

            client=  new TcpClient(host, port, reconnectInterval, reconnectRetryNumber) {
                @Override
                protected void onRead(ByteBuffer buffer) throws Exception
                {
                    buffers.put(buffer);
                }
                @Override
                protected void onConnected() throws Exception
                {
                    for (ActionListener listener : listeners) {
                        listener.actionPerformed(new ActionEvent(this, 1,"connected"));
                    }
                }

                @Override
                protected void onDisconnected() throws Exception {
                    for (ActionListener listener : listeners) {
                        listener.actionPerformed(new ActionEvent(this, 2,"disconnected"));
                    }
                }

                @Override
                protected void onFailure(Exception e) throws Exception {
                    for (ActionListener listener : listeners) {
                        listener.actionPerformed(new ActionEvent(e, 3,"failure"));
                    }
                }

            };
        }
        return client;
    }


    // endregion

    // region events

    /**
     * Event listeners container
     */
    private final List<ActionListener> listeners = new ArrayList<>();


    /**
     * Add listener to event listeners container
     * @param toAdd
     */
    public void addListener(ActionListener toAdd) {
        listeners.add(toAdd);
    }


    // endregion events

    // region construction


    // endregion



    // region public methods

    /**
     * Start listening on TCP lient host/port
     */
    @Override
    public void startListening() {
        final String functionName = "startListening()";
        try {
            getClient().start();
        } catch (IOException e) {
            throw new GeneralError(String.format("%s.%s: Exception: ", className, functionName), e);
        }
    }
    // endregion

    // region Receiver implementation


    /**
     * Receive data from TCP client
     * @return
     * Returns ByteBuffer representing separate message
     */
    @Override
    public abstract List<ByteBuffer> receive();

    /**
     * Send data buffer using TCP client
     * @param byteBuffer
     */
    @Override
     public void send(ByteBuffer byteBuffer) {
        final String functionName = "send()";
        try {
            getClient().send(byteBuffer);
        } catch (Exception e) {
            throw new GeneralError(String.format("%s.%s: Exception: ", className, functionName), e);
        }
    }

    @Override
    public void addLogger(GenericLogger logger) {
        this.logger = logger;
        this.getClient().addLogger(logger);
    }

    /**
     * Stop receiver and TCP client
     */
    @Override
    public void stop() {
        getClient().stop();
    }

    // endregion

    // region private methods

    /**
     * Concatenate 2 bute buffers
     * @param byteBuffer1
     * @param byteBuffer2
     * @param length1
     * @param length2
     * @return
     */
    protected ByteBuffer concatByteBuffers(ByteBuffer byteBuffer1, ByteBuffer byteBuffer2, int length1, int length2) {
        ByteBuffer newBuffer = ByteBuffer.allocateDirect(length1 + length2);
        byteBuffer1.rewind();
        byteBuffer2.rewind();
        newBuffer.put(byteBuffer1);
        newBuffer.put(byteBuffer2);
        newBuffer.rewind();
        return newBuffer;
    }
    // endregion

    // region destroy implementation

    /**
     * Destroy the receiver
     * @throws Exception
     */
    @Override
    public void destroy() throws Exception {
        listeners.clear();
    }

    // endregion
}
