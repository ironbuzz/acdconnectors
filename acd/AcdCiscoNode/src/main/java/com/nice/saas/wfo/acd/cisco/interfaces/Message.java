package com.nice.saas.wfo.acd.cisco.interfaces;

import java.nio.ByteBuffer;

/**
 * Message (can be serialized/deserialized)
 */
public interface Message {

    /**
     * Deserialize mesage from byte buffer
     * @param byteBuffer
     * @return
     */
    Message deserialize(ByteBuffer byteBuffer);

    /**
     * Serialize message
     * @return
     */
    ByteBuffer serialize();

    /**
     * convert to string
     * @return
     */
    String toString();
}
