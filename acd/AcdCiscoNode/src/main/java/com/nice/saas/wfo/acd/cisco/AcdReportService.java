package com.nice.saas.wfo.acd.cisco;

import com.nice.saas.wfo.acd.cisco.common.Constants;
import com.nice.saas.wfo.acd.cisco.common.CsvWriter;
import com.nice.saas.wfo.acd.cisco.common.DbConnectionFactory;
import com.nice.saas.wfo.acd.historicaldata.GenericReportService;
import com.nice.saas.wfo.comp.manager.asc.generic.ACDErrorHandler;
import com.nice.saas.wfo.comp.manager.asc.generic.ConnectionError;
import com.nice.saas.wfo.comp.manager.asc.generic.GeneralError;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

import java.sql.*;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Base class for every ACD report service
 */
public abstract class AcdReportService implements GenericReportService, DisposableBean {
    private final String className = this.getClass().getName();

    private DateTimeFormatter dateTimeFormatter =
            DateTimeFormatter.ofPattern(Constants.InformixDateTimeFormatterString).withZone(ZoneOffset.UTC);

    private DateTimeFormatter dateTimeFormatterNormalizer =
            DateTimeFormatter.ofPattern(Constants.NormalizerDateTimeFormatterString).withZone(ZoneOffset.UTC);

    private String acdType;
    /**
     * Logger declaration and setter
     */
    private GenericLogger logger;
    public void addLogger(GenericLogger logger){
        this.logger = logger;
    }

    /**
     * DB connection url, username, password
     */
    private String url;
    private String username;
    private String password;
    /**
     * DB class name
     */
    private String dbClassName;
    protected void setDbClassName(String dbClassName) {
        this.dbClassName = dbClassName;
    }

    /**
     * Report query
     */
    private String query;
    protected void setQuery(String query) {
        this.query = query;
    }

    /**
     * CSV separator
     */
    private final String csvSeparator = Constants.CsvSeparator;

    /**
     * DB data fetch size
     */
    @Value("${acd.reportService.fetchSize}")
    private int fetchSize;

    @Autowired
    protected ACDErrorHandler acdErrorHandler;

    private static  Connection connection;

    private static AtomicBoolean isResourceAvailable = new AtomicBoolean(true);

    @Override
    public void init(Map<String, String> initDetails) {
        final String functionName = "initDetails()";
        //informix: jdbc:informix-sqli://172.21.13.198:1504/db_cra:INFORMIXSERVER=cuccx115_uccx
        //MSSQL: jdbc:jtds:sqlserver://172.21.13.87:1433/ic115_hds
        if ((!initDetails.containsKey("url") || initDetails.get("url") == null || initDetails.get("url").length() == 0) ||
                (!initDetails.containsKey("host") || initDetails.get("host") == null || initDetails.get("host").length() == 0) ||
                (!initDetails.containsKey("port") || initDetails.get("port") == null || initDetails.get("port").length() == 0) ||
                (!initDetails.containsKey("schema") || initDetails.get("schema") == null || initDetails.get("schema").length() == 0) ||
                !initDetails.containsKey("server")  ||
                (!initDetails.containsKey("username") || initDetails.get("username") == null || initDetails.get("username").length() == 0) ||
                (!initDetails.containsKey("password") || initDetails.get("password") == null)) {
            String errorString = String.format("%s.%s: Wrong DB connection details. " , className, functionName);
            throw new GeneralError(GeneralError.ERROR_IDS.FATAL.ordinal(), errorString, new Exception("Wrong configuration"));
        }
        url = initDetails.get("url");
        username = initDetails.get("username");
        password = initDetails.get("password");
        String host = initDetails.get("host");
        String port = initDetails.get("port");
        String schema = initDetails.get("schema");
        String server = initDetails.get("server");


        url = url.replace("{host}", host)
                .replace("{port}", port)
                .replace("{schema}", schema)
                .replace("{server}", server);

        logger.info("INIT Connection template URL:" + url);
        logger.info("INIT Connection: HOST:|" + host + "|,port:|" + port+"|,server:|"+server+"|,schema:|"+schema+"|");

    }

        /**
         * Connect to DB
         * @return
         */
    @Override
    public boolean connect() {
        final String functionName = "connect()";
        try
        {
            connection = DbConnectionFactory.getInstance(dbClassName, url, username, password).getConnection();
            logger.info("Connection:" + connection.getCatalog());
        }
        catch (Exception e)
        {
            isResourceAvailable.set(false);
            String errorString = String.format("%s.%s: Failed to connect to DB. ", className, functionName);
            logger.error(errorString, e);
            acdErrorHandler.connectionError(new ConnectionError(errorString, e));
        }
        isResourceAvailable.set(true);
        return true;
    }

    /**
     * Get ACD type
     * @return
     */
    @Override
    public String getAcdType() {
        if (acdType== null) {
            return Constants.AcdCiscoUccxReportType;
        }
        return acdType;
    }

    public void setAcdType(String acdType) {
        this.acdType = acdType;
    }

    /**
     * get report data as CSV string
     * @param startTime
     * @param endTime
     * @return
     */
    @Override
    public String getReportData(Instant startTime, Instant endTime)  {
        final String functionName = "getReportData()";
        String startTimeString = null;
        String endTimeString = null;
        PreparedStatement stmt;
        String[] queries;
        List<String> params = new ArrayList<>();

        try {
            if (connection == null || connection.isClosed() ) {
                String warningString = String.format("%s.%s: Reconnecting to DB.", getClass().getName(), functionName);
                logger.info(warningString);
                connect();
            }
            startTimeString = dateTimeFormatter.format(startTime);
            endTimeString = dateTimeFormatter.format(endTime);
            queries = query.split("union");
            if (queries.length >= 1) {
                for (String queryItem : queries) {
                    int count = StringUtils.countOccurrencesOf(queryItem, "?");
                    for (int i = 0; i< count; ++i) {
                        if (i % 2 == 0) {
                            params.add(startTimeString);
                        } else {
                            params.add(endTimeString);
                        }
                    }
                }
            }
            stmt = connection.prepareStatement(query);
            stmt.setFetchSize(fetchSize);

            int index = 1;
            for (String param : params) {
                stmt.setString(index++, param);
            }

            // execute query
            ResultSet resultSet = stmt.executeQuery();
            // convert result set to CSV string
            String csvString = CsvWriter.convertToCsv(resultSet, csvSeparator);
            stmt.close();
            if (csvString == null) {
                return null;
            }

            // add start/ end datetime etc.
            String dtSeparator = "@dt@";
            StringBuilder sb = new StringBuilder(csvString);
            sb.append(dtSeparator);
            String normalizerStartTimeString = dateTimeFormatterNormalizer.format(startTime);
            sb.append(normalizerStartTimeString);
            sb.append(dtSeparator);
            sb.append(endTimeString);
            sb.append(dtSeparator);
            sb.append(Duration.between(startTime, endTime).toMinutes());
            sb.append(dtSeparator);
            sb.append("UTC");
            sb.append(dtSeparator);
            return sb.toString();
        } catch (SQLException e) {
            logger.error(e.toString(),e);
            String errorString =
                    String.format("%s.%s: Query execution failed. Query: %s, Params: startTime: %s, endTime: %s",  getClass().getName(), functionName, query, startTimeString, endTimeString );
            logger.error(errorString, e);
            DbConnectionFactory.destroyInstance();
            connection = null;
            throw new GeneralError(GeneralError.ERROR_IDS.SQL_ERROR.ordinal(), errorString, e);
        }
        catch (Exception e) {
            String errorString = String.format("%s.%s: Failed to get report data. Query: %s, Params: startTime: %s, endTime: %s",  className, functionName, query, startTimeString, endTimeString );
            logger.error(errorString, e);
            throw new GeneralError(GeneralError.ERROR_IDS.FATAL.ordinal(), errorString, e);
        }
    }


    /**
     * Get report version
     * @param reportType
     * @return
     */
    @Override
    public int getReportVersion(String reportType) {
        return 0;
    }

    @Override
    public void terminate() {
        final String functionName = "terminate()";
        try {
            destroy();
        } catch (Exception e) {
            logger.error(String.format("%s.%s: Exception: ", className, functionName), e);
        }
    }

    @Override
    public boolean isResourceAvailable() {
        return isResourceAvailable.get();
    }

    /**
     * Invoked by a BeanFactory on destruction of a singleton.
     *
     * @throws Exception in case of shutdown errors.
     *                   Exceptions will get logged but not rethrown to allow
     *                   other beans to release their resources too.
     */
    @Override
    public void destroy() throws Exception {
        if (connection != null && (!connection.isClosed())) {
            connection.close();
        }
    }

}
