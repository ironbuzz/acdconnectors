package com.nice.saas.wfo.acd.cisco.common;

/**
 * Constants
 */
public final class Constants {

    /**
     * Informix datetime format
     *
     * */
    public static final String InformixDateTimeFormatterString = "yyyy-MM-dd HH:mm:ss";
    /**
     * Normalizer datetime format
     *
     * */
    public static final String NormalizerDateTimeFormatterString = "MM-dd-yyyy HH:mm:ss";
    /**
     * Default buffers queue capacity (blocking queue in UccxReceiver) (can be overriden in bean configuration)
     */
    public static final int DefaultBuffersQueueCapacity = 500;


    /**
     * Default buffer size (UccxMessage serialize)
     */
    public static final int DefaultBufferSize = 0x100000;

    /**
     * ACD report ID for reports (UCCX)
     */
    public static final String AcdCiscoUccxReportType = "CISCO_UCCX";

    /**
     * ACD report ID for reports (UCCE)
     */
    public static final String AcdCiscoUcceReportType = "CISCO_UCCE";

    /**
     * CSV separator
     */
    public static final String CsvSeparator = "|";

    /**
     * Informix driver class name
     */
    public static final String InformixDriverClassName = "com.informix.jdbc.IfxDriver";

    /**
     * MS SQL driver class name
     */
    public static final String MsSqlDriverClassName = "net.sourceforge.jtds.jdbc.Driver";
}
