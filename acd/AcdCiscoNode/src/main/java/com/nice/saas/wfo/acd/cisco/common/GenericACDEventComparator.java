package com.nice.saas.wfo.acd.cisco.common;

import com.nice.saas.wfo.acd.rta.GenericACDEvent;

import java.util.Comparator;

public class GenericACDEventComparator implements Comparator<GenericACDEvent> {
    @Override
    public int compare(GenericACDEvent o1, GenericACDEvent o2) {
        if (o1 == null && o2 == null){
            return 0;
        }
        if (o1 == null && o2 != null || (o1 != null && o2 == null)) {
            return -1;
        }

        if (o1.getLoginId() != null) {
            if (!o1.getLoginId().equals(o2.getLoginId())) {
                return -1;
            }
        } else if (o2.getLoginId() != null) {
            return -1;
        }

        if (o1.getAcdId() != null) {
            if (!o1.getAcdId().equals(o2.getAcdId())) {
                    return -1;
                }
        } else if (o2.getAcdId() != null) {
            return -1;
        }



        if (o1.getEventCode() != null) {
            if (!o1.getEventCode().equals(o2.getEventCode())) {
                return -1;
            }
        } else if (o2.getEventCode() != null) {
            return -1;
        }

        if (o1.getReasonCode() != null) {
            if (!o1.getReasonCode().equals(o2.getReasonCode())) {
                return -1;
            }
        } else if (o2.getReasonCode() != null) {
            return -1;
        }

        return 0;
    }
}
