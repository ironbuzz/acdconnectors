package com.nice.saas.wfo.acd.cisco.common;

import java.sql.*;
import java.io.*;

public class CsvWriter {
    /**
     * Convert result set to CSV
     * @param rs - result set
     * @param separator - CSV values separator
     * @throws SQLException
     * @throws FileNotFoundException
     */
    public static String convertToCsv(ResultSet rs, String separator) throws SQLException, IOException {
        StringWriter stringWriter = new StringWriter();
        BufferedWriter bufferedWriter = new BufferedWriter(stringWriter) ;
        ResultSetMetaData meta = rs.getMetaData() ;
        int numberOfColumns = meta.getColumnCount() ;
        String dataHeaders = meta.getColumnName(1) ;
        for (int i = 2 ; i < numberOfColumns + 1 ; i ++ ) {
            dataHeaders += (separator+  meta.getColumnName(i).replaceAll("\"","\\\"") ) ;
        }
        bufferedWriter.write(dataHeaders);
        bufferedWriter.newLine();
        int rowCount = 0;
        while (rs.next()) {
            String row = rs.getString(1).replaceAll("\"","\\\"")  ;
            for (int i = 2 ; i < numberOfColumns + 1 ; i ++ ) {
                String str= rs.getString(i);
                if (str == null) {
                    str = "0";
                }
                row += (separator + str.replaceAll("\"","\\\"")) ;
            }
            bufferedWriter.write(row);
            bufferedWriter.newLine();
            rowCount++;
        }
        bufferedWriter.flush();
        bufferedWriter.close();
        if (rowCount == 0) {
            return null;
        }
        return stringWriter.toString();
    }
}
