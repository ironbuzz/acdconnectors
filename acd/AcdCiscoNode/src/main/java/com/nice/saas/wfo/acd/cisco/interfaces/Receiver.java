package com.nice.saas.wfo.acd.cisco.interfaces;

import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;

import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

public interface Receiver {
    void startListening ();
    List<ByteBuffer> receive();
    void send(ByteBuffer byteBuffer);
    void stop();
    void addListener(ActionListener toAdd);
    void addLogger(GenericLogger logger);
}
