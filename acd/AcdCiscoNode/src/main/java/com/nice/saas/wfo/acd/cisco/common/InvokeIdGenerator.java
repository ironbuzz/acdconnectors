package com.nice.saas.wfo.acd.cisco.common;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Generate invoke id for UCCX communication (it started from 0 and incremented for every message)
 */
public class InvokeIdGenerator {
    // initial value is 0
    private static AtomicInteger invokeId = new AtomicInteger(0);

    /**
     * generate new invoke id incrementing the previous value
     * @return invokeId
     */
    public static int generateInvokeId () {
        return invokeId.getAndIncrement();
    }

    /**
     * reset invoke id (set it to 0)
     */
    public static void reset() {
        invokeId.set(0);
    }
}
