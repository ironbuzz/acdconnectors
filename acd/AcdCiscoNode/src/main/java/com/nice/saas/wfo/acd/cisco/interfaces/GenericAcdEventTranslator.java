package com.nice.saas.wfo.acd.cisco.interfaces;


import com.nice.saas.wfo.acd.rta.GenericACDEvent;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;

/**
 * Translate message receiver from receiver to ACD event data structure
 */
public interface GenericAcdEventTranslator {
    GenericACDEvent translate(Message message);
    void addLogger(GenericLogger logger);
}
