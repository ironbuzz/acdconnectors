package com.nice.saas.wfo.acd.cisco.common;

import com.nice.saas.wfo.acd.rta.GenericACDEvent;
import org.junit.Test;

import static org.junit.Assert.*;

public class GenericACDEventComparatorTest {
    GenericACDEventComparator c = new GenericACDEventComparator();
    @Test
    public void compare1() throws Exception {
        GenericACDEvent e1 = new GenericACDEvent();
        e1.setLoginId("1");
        e1.setAcdId("2");
        e1.setReasonCode("3");
        e1.setEventCode("4");
        GenericACDEvent e2 = new GenericACDEvent();
        e2.setLoginId("1");
        e2.setAcdId("2");
        e2.setReasonCode("3");
        e2.setEventCode("4");

        assertEquals(c.compare(e1, e2), 0);
    }

    @Test
    public void compare2() throws Exception {
        GenericACDEvent e1 = null;

        GenericACDEvent e2 = new GenericACDEvent();
        e2.setLoginId("1");
        e2.setAcdId("2");
        e2.setReasonCode("3");
        e2.setEventCode("4");

        assertEquals(c.compare(e1, e2), -1);
    }

    public void compare3() throws Exception {
        GenericACDEvent e1 = new GenericACDEvent();

        GenericACDEvent e2 = null;


        assertEquals(c.compare(e1, e2), -1);
    }

    @Test
    public void compare4() throws Exception {
        GenericACDEvent e1 = null;

        GenericACDEvent e2 = null;


        assertEquals(c.compare(e1, e2), 0);
    }
    @Test
    public void compare5() throws Exception {
        GenericACDEvent e1 = new GenericACDEvent();
        e1.setLoginId("11");
        e1.setAcdId("2");
        e1.setReasonCode("3");
        e1.setEventCode("4");
        GenericACDEvent e2 = new GenericACDEvent();
        e2.setLoginId("1");
        e2.setAcdId("2");
        e2.setReasonCode("3");
        e2.setEventCode("4");

        assertEquals(c.compare(e1, e2), -1);
    }

    @Test
    public void compare6() throws Exception {
        GenericACDEvent e1 = new GenericACDEvent();
        e1.setLoginId("1");
        e1.setAcdId("22");
        e1.setReasonCode("3");
        e1.setEventCode("4");
        GenericACDEvent e2 = new GenericACDEvent();
        e2.setLoginId("1");
        e2.setAcdId("2");
        e2.setReasonCode("3");
        e2.setEventCode("4");

        assertEquals(c.compare(e1, e2), -1);
    }

    @Test
    public void compare7() throws Exception {
        GenericACDEvent e1 = new GenericACDEvent();
        e1.setLoginId("1");
        e1.setAcdId("2");
        e1.setReasonCode("33");
        e1.setEventCode("4");
        GenericACDEvent e2 = new GenericACDEvent();
        e2.setLoginId("1");
        e2.setAcdId("2");
        e2.setReasonCode("3");
        e2.setEventCode("4");

        assertEquals(c.compare(e1, e2), -1);
    }

    @Test
    public void compare8() throws Exception {
        GenericACDEvent e1 = new GenericACDEvent();
        e1.setLoginId("1");
        e1.setAcdId("2");
        e1.setReasonCode("3");
        e1.setEventCode("444");
        GenericACDEvent e2 = new GenericACDEvent();
        e2.setLoginId("1");
        e2.setAcdId("2");
        e2.setReasonCode("3");
        e2.setEventCode("4");

        assertEquals(c.compare(e1, e2), -1);
    }

    @Test
    public void compare9() throws Exception {
        GenericACDEvent e1 = new GenericACDEvent();
        e1.setLoginId(null);
        e1.setAcdId("2");
        e1.setReasonCode("3");
        e1.setEventCode("4");
        GenericACDEvent e2 = new GenericACDEvent();
        e2.setLoginId("1");
        e2.setAcdId("2");
        e2.setReasonCode("3");
        e2.setEventCode("4");

        assertEquals(c.compare(e1, e2), -1);
    }

    @Test
    public void compare10() throws Exception {
        GenericACDEvent e1 = new GenericACDEvent();
        e1.setLoginId("1");
        e1.setAcdId(null);
        e1.setReasonCode("3");
        e1.setEventCode("4");
        GenericACDEvent e2 = new GenericACDEvent();
        e2.setLoginId("1");
        e2.setAcdId("2");
        e2.setReasonCode("3");
        e2.setEventCode("4");

        assertEquals(c.compare(e1, e2), -1);
    }

    @Test
    public void compare11() throws Exception {
        GenericACDEvent e1 = new GenericACDEvent();
        e1.setLoginId("1");
        e1.setAcdId("2");
        e1.setReasonCode(null);
        e1.setEventCode("4");
        GenericACDEvent e2 = new GenericACDEvent();
        e2.setLoginId("1");
        e2.setAcdId("2");
        e2.setReasonCode("3");
        e2.setEventCode("4");

        assertEquals(c.compare(e1, e2), -1);
    }

    @Test
    public void compare12() throws Exception {
        GenericACDEvent e1 = new GenericACDEvent();
        e1.setLoginId("1");
        e1.setAcdId("2");
        e1.setReasonCode("3");
        e1.setEventCode(null);
        GenericACDEvent e2 = new GenericACDEvent();
        e2.setLoginId("1");
        e2.setAcdId("2");
        e2.setReasonCode("3");
        e2.setEventCode("4");

        assertEquals(c.compare(e1, e2), -1);
    }
}