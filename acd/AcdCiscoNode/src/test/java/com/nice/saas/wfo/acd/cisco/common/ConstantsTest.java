package com.nice.saas.wfo.acd.cisco.common;

import org.junit.Test;

import static org.junit.Assert.*;

public class ConstantsTest {

    @Test
    public void testConstants(){
        assertEquals(Constants.AcdCiscoUccxReportType, "CISCO_UCCX");
        assertEquals(Constants.AcdCiscoUcceReportType, "CISCO_UCCE");
        assertEquals(Constants.InformixDateTimeFormatterString, "yyyy-MM-dd HH:mm:ss");
        assertEquals(Constants.NormalizerDateTimeFormatterString, "MM-dd-yyyy HH:mm:ss");
        assertEquals(Constants.DefaultBuffersQueueCapacity, 500);
        assertEquals(Constants.CsvSeparator, "|");
        assertEquals(Constants.DefaultBufferSize, 0x100000);
    }

}