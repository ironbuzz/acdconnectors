package com.nice.saas.wfo.acd.cisco.common;


import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ProtocolFamily;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketOption;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.channels.spi.AbstractSelector;
import java.nio.channels.spi.SelectorProvider;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.assertEquals;

public class TcpClientTest {
    TcpClient tcpClientTest =  new TcpClient("127.0.0.1", 1111, 20, 30, 40, 50) {
        @Override
        protected  void onRead(ByteBuffer buf) throws Exception {

        }

        @Override
        protected  void onConnected() throws Exception {

        }

        @Override
        protected  void onDisconnected() throws Exception {

        }

        @Override
        protected  void onFailure(Exception e) throws Exception {

        }
    };
    @Before
    public void setUp() {

    }

    @Test
    public void run() throws NoSuchFieldException, IllegalAccessException {
        Field field = tcpClientTest.getClass().getSuperclass().getDeclaredField("DefaultReconnectInterval");
        field.setAccessible(true);
        assertEquals(field.get(tcpClientTest), (long)500);
        field = tcpClientTest.getClass().getSuperclass().getDeclaredField("DefaultReadBufferSize");
        field.setAccessible(true);
        assertEquals(field.get(tcpClientTest), 0x100000);
        field = tcpClientTest.getClass().getSuperclass().getDeclaredField("DefaultWriteBufferSize");
        field.setAccessible(true);
        assertEquals(field.get(tcpClientTest), 0x100000);
        field = tcpClientTest.getClass().getSuperclass().getDeclaredField("DefaultReconnectRetries");
        field.setAccessible(true);
        assertEquals(field.get(tcpClientTest), 20);
        field = tcpClientTest.getClass().getSuperclass().getDeclaredField("DefaultHost");
        field.setAccessible(true);
        assertEquals(field.get(tcpClientTest), "127.0.0.1");
        field = tcpClientTest.getClass().getSuperclass().getDeclaredField("DefaultPort");
        field.setAccessible(true);
        assertEquals(field.get(tcpClientTest), 8080);

        field = tcpClientTest.getClass().getSuperclass().getDeclaredField("host");
        field.setAccessible(true);
        assertEquals(field.get(tcpClientTest), "127.0.0.1");
        field = tcpClientTest.getClass().getSuperclass().getDeclaredField("port");
        field.setAccessible(true);
        assertEquals(field.get(tcpClientTest), 1111);
        field = tcpClientTest.getClass().getSuperclass().getDeclaredField("reconnectInterval");
        field.setAccessible(true);
        assertEquals(field.get(tcpClientTest), (long)20);
        field = tcpClientTest.getClass().getSuperclass().getDeclaredField("reconnectRetryNumber");
        field.setAccessible(true);
        assertEquals(field.get(tcpClientTest), (long)30);
        field = tcpClientTest.getClass().getSuperclass().getDeclaredField("readBufferSize");
        field.setAccessible(true);
        assertEquals(field.get(tcpClientTest), 40);
        field = tcpClientTest.getClass().getSuperclass().getDeclaredField("writeBufferSize");
        field.setAccessible(true);
        assertEquals(field.get(tcpClientTest), 50);
    }

    @Test
    public void construct1() throws NoSuchFieldException, IllegalAccessException {
        TcpClient t = new TcpClient() {
            @Override
            protected  void onRead(ByteBuffer buf) throws Exception {

            }

            @Override
            protected  void onConnected() throws Exception {

            }

            @Override
            protected  void onDisconnected() throws Exception {

            }

            @Override
            protected  void onFailure(Exception e) throws Exception {

            }
        };
        Field field = t.getClass().getSuperclass().getDeclaredField("reconnectInterval");
        field.setAccessible(true);
        assertEquals(field.get(t), (long)500);

    }

    @Test
    public void construct2() throws NoSuchFieldException, IllegalAccessException {
        TcpClient t = new TcpClient("127.0.0.1", 200) {
            @Override
            protected  void onRead(ByteBuffer buf) throws Exception {

            }

            @Override
            protected  void onConnected() throws Exception {

            }

            @Override
            protected  void onDisconnected() throws Exception {

            }

            @Override
            protected  void onFailure(Exception e) throws Exception {

            }
        };
        Field field = t.getClass().getSuperclass().getDeclaredField("reconnectInterval");
        field.setAccessible(true);
        assertEquals(field.get(t), (long)500);
        field = t.getClass().getSuperclass().getDeclaredField("host");
        field.setAccessible(true);
        assertEquals(field.get(t),"127.0.0.1");
        field = t.getClass().getSuperclass().getDeclaredField("port");
        field.setAccessible(true);
        assertEquals(field.get(t),200);

    }

    @Test
    public void processWrite() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        TcpClient t = new TcpClient("127.0.0.1", 200) {
            @Override
            protected  void onRead(ByteBuffer buf) throws Exception {

            }

            @Override
            protected  void onConnected() throws Exception {

            }

            @Override
            protected  void onDisconnected() throws Exception {

            }

            @Override
            protected  void onFailure(Exception e) throws Exception {

            }
        };
        Method method = t.getClass().getSuperclass().getDeclaredMethod("processWrite", SelectionKey.class);
        method.setAccessible(true);
        SelectionKey skey = Mockito.mock(SelectionKey.class);
        Mockito.when(skey.readyOps()).thenReturn(1);

        method.invoke(t, skey);

    }

    @Test
    public void processSelectedKeys() throws NoSuchMethodException, IOException, InvocationTargetException, IllegalAccessException {
        TcpClient t = new TcpClient("127.0.0.1", 200) {
            @Override
            protected  void onRead(ByteBuffer buf) throws Exception {

            }

            @Override
            protected  void onConnected() throws Exception {

            }

            @Override
            protected  void onDisconnected() throws Exception {

            }

            @Override
            protected  void onFailure(Exception e) throws Exception {

            }
        };
        Method method = t.getClass().getSuperclass().getDeclaredMethod("processSelectedKeys",Set.class);
        method.setAccessible(true);
       Set keys = Mockito.mock(Set.class);
       SelectionKey wKey = Mockito.mock(SelectionKey.class);
        Mockito.when(wKey.readyOps()).thenReturn(1);
       keys.add(wKey );
        Iterator it = Mockito.mock(Iterator.class);
        Mockito.when(keys.iterator()).thenReturn(it);
        Mockito.when(it.next()).thenReturn(true).thenReturn(false);
        method.invoke(t, keys);

    }

    @Test
    public void onConnect() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, IOException {
        TcpClient t = new TcpClient("127.0.0.1", 200) {
            @Override
            protected  void onRead(ByteBuffer buf) throws Exception {

            }

            @Override
            protected  void onConnected() throws Exception {

            }

            @Override
            protected  void onDisconnected() throws Exception {

            }

            @Override
            protected  void onFailure(Exception e) throws Exception {

            }
        };
        Method method = t.getClass().getSuperclass().getDeclaredMethod("processConnect", SelectionKey.class);
        method.setAccessible(true);
        SelectionKey skey = Mockito.mock(SelectionKey.class);
        Mockito.when(skey.readyOps()).thenReturn(1);
        SocketChannel chMock = Mockito.mock(SocketChannel.class);
        Mockito.when(chMock.finishConnect()).thenReturn(true);
        Mockito.when(skey.channel()).thenReturn(chMock);

        method.invoke(t, skey);
  }

    @Test
    public void configureChannel () throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        TcpClient t = new TcpClient("127.0.0.1", 200) {
            @Override
            protected  void onRead(ByteBuffer buf) throws Exception {

            }

            @Override
            protected  void onConnected() throws Exception {

            }

            @Override
            protected  void onDisconnected() throws Exception {

            }

            @Override
            protected  void onFailure(Exception e) throws Exception {

            }
        };
        SelectorProvider p = new SelectorProvider(){
            @Override
            public DatagramChannel openDatagramChannel() throws IOException {
                return null;
            }

            @Override
            public DatagramChannel openDatagramChannel(ProtocolFamily family) throws IOException {
                return null;
            }

            @Override
            public Pipe openPipe() throws IOException {
                return null;
            }

            @Override
            public AbstractSelector openSelector() throws IOException {
                return null;
            }

            @Override
            public ServerSocketChannel openServerSocketChannel() throws IOException {
                return null;
            }

            @Override
            public SocketChannel openSocketChannel() throws IOException {
                return null;
            }
        };
        SocketChannel c = new SocketChannel(p) {
            @Override
            public SocketChannel bind(SocketAddress local) throws IOException {
                return null;
            }

            @Override
            public <T> SocketChannel setOption(SocketOption<T> name, T value) throws IOException {
                return null;
            }

            @Override
            public SocketChannel shutdownInput() throws IOException {
                return null;
            }

            @Override
            public SocketChannel shutdownOutput() throws IOException {
                return null;
            }

            @Override
            public Socket socket() {
                return new Socket();
            }

            @Override
            public boolean isConnected() {
                return false;
            }

            @Override
            public boolean isConnectionPending() {
                return false;
            }

            @Override
            public boolean connect(SocketAddress remote) throws IOException {
                return false;
            }

            @Override
            public boolean finishConnect() throws IOException {
                return false;
            }

            @Override
            public SocketAddress getRemoteAddress() throws IOException {
                return null;
            }

            @Override
            public int read(ByteBuffer dst) throws IOException {
                return 0;
            }

            @Override
            public long read(ByteBuffer[] dsts, int offset, int length) throws IOException {
                return 0;
            }

            @Override
            public int write(ByteBuffer src) throws IOException {
                return 0;
            }

            @Override
            public long write(ByteBuffer[] srcs, int offset, int length) throws IOException {
                return 0;
            }

            @Override
            public SocketAddress getLocalAddress() throws IOException {
                return null;
            }

            @Override
            public <T> T getOption(SocketOption<T> name) throws IOException {
                return null;
            }

            @Override
            public Set<SocketOption<?>> supportedOptions() {
                return null;
            }

            @Override
            protected void implCloseSelectableChannel() throws IOException {

            }

            @Override
            protected void implConfigureBlocking(boolean block) throws IOException {

            }
        };
        Class[] cArg = new Class[1];
        cArg[0] = SocketChannel.class;
        Method m = t.getClass().getSuperclass().getDeclaredMethod("configureChannel", cArg);
        m.setAccessible(true);
        m.invoke(t, c);
    }

    @Test
    public void processRead() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        TcpClient t = new TcpClient("127.0.0.1", 200) {
            @Override
            protected  void onRead(ByteBuffer buf) throws Exception {

            }

            @Override
            protected  void onConnected() throws Exception {

            }

            @Override
            protected  void onDisconnected() throws Exception {

            }

            @Override
            protected  void onFailure(Exception e) throws Exception {

            }
        };
        SelectorProvider p = new SelectorProvider(){
            @Override
            public DatagramChannel openDatagramChannel() throws IOException {
                return null;
            }

            @Override
            public DatagramChannel openDatagramChannel(ProtocolFamily family) throws IOException {
                return null;
            }

            @Override
            public Pipe openPipe() throws IOException {
                return null;
            }

            @Override
            public AbstractSelector openSelector() throws IOException {
                return null;
            }

            @Override
            public ServerSocketChannel openServerSocketChannel() throws IOException {
                return null;
            }

            @Override
            public SocketChannel openSocketChannel() throws IOException {
                return null;
            }
        };
        SocketChannel c = new SocketChannel(p) {
            @Override
            public SocketChannel bind(SocketAddress local) throws IOException {
                return null;
            }

            @Override
            public <T> SocketChannel setOption(SocketOption<T> name, T value) throws IOException {
                return null;
            }

            @Override
            public SocketChannel shutdownInput() throws IOException {
                return null;
            }

            @Override
            public SocketChannel shutdownOutput() throws IOException {
                return null;
            }

            @Override
            public Socket socket() {
                return new Socket();
            }

            @Override
            public boolean isConnected() {
                return false;
            }

            @Override
            public boolean isConnectionPending() {
                return false;
            }

            @Override
            public boolean connect(SocketAddress remote) throws IOException {
                return false;
            }

            @Override
            public boolean finishConnect() throws IOException {
                return false;
            }

            @Override
            public SocketAddress getRemoteAddress() throws IOException {
                return null;
            }

            @Override
            public int read(ByteBuffer dst) throws IOException {
                return 0;
            }

            @Override
            public long read(ByteBuffer[] dsts, int offset, int length) throws IOException {
                return 0;
            }

            @Override
            public int write(ByteBuffer src) throws IOException {
                return 0;
            }

            @Override
            public long write(ByteBuffer[] srcs, int offset, int length) throws IOException {
                return 0;
            }

            @Override
            public SocketAddress getLocalAddress() throws IOException {
                return null;
            }

            @Override
            public <T> T getOption(SocketOption<T> name) throws IOException {
                return null;
            }

            @Override
            public Set<SocketOption<?>> supportedOptions() {
                return null;
            }

            @Override
            protected void implCloseSelectableChannel() throws IOException {

            }

            @Override
            protected void implConfigureBlocking(boolean block) throws IOException {

            }
        };
        Class[] cArg = new Class[1];
        cArg[0] = SelectionKey.class;
        SelectionKey wKey = Mockito.mock(SelectionKey.class);
        Mockito.when(wKey.readyOps()).thenReturn(1);
        Mockito.when(wKey.channel()).thenReturn(c );

        Method m = t.getClass().getSuperclass().getDeclaredMethod("processRead", cArg);
        m.setAccessible(true);
        m.invoke(t, wKey);
    }

    @Test(expected = NullPointerException.class)
    public void send() throws IOException, InterruptedException, NoSuchFieldException, IllegalAccessException {
        TcpClient t = new TcpClient("127.0.0.1", 200) {
            @Override
            protected  void onRead(ByteBuffer buf) throws Exception {

            }

            @Override
            protected  void onConnected() throws Exception {

            }

            @Override
            protected  void onDisconnected() throws Exception {

            }

            @Override
            protected  void onFailure(Exception e) throws Exception {

            }
        };
        SelectorProvider p = new SelectorProvider(){
            @Override
            public DatagramChannel openDatagramChannel() throws IOException {
                return null;
            }

            @Override
            public DatagramChannel openDatagramChannel(ProtocolFamily family) throws IOException {
                return null;
            }

            @Override
            public Pipe openPipe() throws IOException {
                return null;
            }

            @Override
            public AbstractSelector openSelector() throws IOException {
                return null;
            }

            @Override
            public ServerSocketChannel openServerSocketChannel() throws IOException {
                return null;
            }

            @Override
            public SocketChannel openSocketChannel() throws IOException {
                return null;
            }
        };
        SocketChannel c = new SocketChannel(p) {
            @Override
            public SocketChannel bind(SocketAddress local) throws IOException {
                return null;
            }

            @Override
            public <T> SocketChannel setOption(SocketOption<T> name, T value) throws IOException {
                return null;
            }

            @Override
            public SocketChannel shutdownInput() throws IOException {
                return null;
            }

            @Override
            public SocketChannel shutdownOutput() throws IOException {
                return null;
            }

            @Override
            public Socket socket() {
                return new Socket();
            }

            @Override
            public boolean isConnected() {
                return false;
            }

            @Override
            public boolean isConnectionPending() {
                return false;
            }

            @Override
            public boolean connect(SocketAddress remote) throws IOException {
                return false;
            }

            @Override
            public boolean finishConnect() throws IOException {
                return false;
            }

            @Override
            public SocketAddress getRemoteAddress() throws IOException {
                return null;
            }

            @Override
            public int read(ByteBuffer dst) throws IOException {
                return 0;
            }

            @Override
            public long read(ByteBuffer[] dsts, int offset, int length) throws IOException {
                return 0;
            }

            @Override
            public int write(ByteBuffer src) throws IOException {
                return 0;
            }

            @Override
            public long write(ByteBuffer[] srcs, int offset, int length) throws IOException {
                return 0;
            }

            @Override
            public SocketAddress getLocalAddress() throws IOException {
                return null;
            }

            @Override
            public <T> T getOption(SocketOption<T> name) throws IOException {
                return null;
            }

            @Override
            public Set<SocketOption<?>> supportedOptions() {
                return null;
            }

            @Override
            protected void implCloseSelectableChannel() throws IOException {

            }

            @Override
            protected void implConfigureBlocking(boolean block) throws IOException {

            }
        };
        Field field = t.getClass().getSuperclass().getDeclaredField("connected");
        field.setAccessible(true);
        field.set(t,new AtomicBoolean(true));
        field = t.getClass().getSuperclass().getDeclaredField("channel");
        field.setAccessible(true);
        field.set(t, c);
        SelectionKey key = new SelectionKey() {
            @Override
            public SelectableChannel channel() {
                return null;
            }

            @Override
            public Selector selector() {
                return null;
            }

            @Override
            public boolean isValid() {
                return false;
            }

            @Override
            public void cancel() {

            }

            @Override
            public int interestOps() {
                return 0;
            }

            @Override
            public SelectionKey interestOps(int ops) {
                return null;
            }

            @Override
            public int readyOps() {
                return 0;
            }
        };

        SelectionKey skey = Mockito.mock(SelectionKey.class);
        field = t.getClass().getSuperclass().getDeclaredField("selector");
        field.setAccessible(true);
     //   Selector se = Mockito.mock(Selector.class);
       // field.set(t, se);
       // Mockito.when(c.keyFor(se)).thenReturn(skey);
        t.send(ByteBuffer.allocateDirect(10));
    }

    @Test
    public void run1() throws NoSuchFieldException, IOException, InterruptedException, IllegalAccessException {
        TcpClient t = new TcpClient("127.0.0.1", 200) {
            @Override
            protected  void onRead(ByteBuffer buf) throws Exception {

            }

            @Override
            protected  void onConnected() throws Exception {

            }

            @Override
            protected  void onDisconnected() throws Exception {

            }

            @Override
            protected  void onFailure(Exception e) throws Exception {

            }
        };
        SelectorProvider p = new SelectorProvider(){
            @Override
            public DatagramChannel openDatagramChannel() throws IOException {
                return null;
            }

            @Override
            public DatagramChannel openDatagramChannel(ProtocolFamily family) throws IOException {
                return null;
            }

            @Override
            public Pipe openPipe() throws IOException {
                return null;
            }

            @Override
            public AbstractSelector openSelector() throws IOException {
                return null;
            }

            @Override
            public ServerSocketChannel openServerSocketChannel() throws IOException {
                return null;
            }

            @Override
            public SocketChannel openSocketChannel() throws IOException {
                return null;
            }
        };
        SocketChannel c = new SocketChannel(p) {
            @Override
            public SocketChannel bind(SocketAddress local) throws IOException {
                return null;
            }

            @Override
            public <T> SocketChannel setOption(SocketOption<T> name, T value) throws IOException {
                return null;
            }

            @Override
            public SocketChannel shutdownInput() throws IOException {
                return null;
            }

            @Override
            public SocketChannel shutdownOutput() throws IOException {
                return null;
            }

            @Override
            public Socket socket() {
                return new Socket();
            }

            @Override
            public boolean isConnected() {
                return false;
            }

            @Override
            public boolean isConnectionPending() {
                return false;
            }

            @Override
            public boolean connect(SocketAddress remote) throws IOException {
                return false;
            }

            @Override
            public boolean finishConnect() throws IOException {
                return false;
            }

            @Override
            public SocketAddress getRemoteAddress() throws IOException {
                return null;
            }

            @Override
            public int read(ByteBuffer dst) throws IOException {
                return 0;
            }

            @Override
            public long read(ByteBuffer[] dsts, int offset, int length) throws IOException {
                return 0;
            }

            @Override
            public int write(ByteBuffer src) throws IOException {
                return 0;
            }

            @Override
            public long write(ByteBuffer[] srcs, int offset, int length) throws IOException {
                return 0;
            }

            @Override
            public SocketAddress getLocalAddress() throws IOException {
                return null;
            }

            @Override
            public <T> T getOption(SocketOption<T> name) throws IOException {
                return null;
            }

            @Override
            public Set<SocketOption<?>> supportedOptions() {
                return null;
            }

            @Override
            protected void implCloseSelectableChannel() throws IOException {

            }

            @Override
            protected void implConfigureBlocking(boolean block) throws IOException {

            }
        };
        Field field = t.getClass().getSuperclass().getDeclaredField("connected");
        field.setAccessible(true);
        field.set(t,new AtomicBoolean(true));
        field = t.getClass().getSuperclass().getDeclaredField("channel");
        field.setAccessible(true);
        field.set(t, c);
        SelectionKey key = new SelectionKey() {
            @Override
            public SelectableChannel channel() {
                return null;
            }

            @Override
            public Selector selector() {
                return null;
            }

            @Override
            public boolean isValid() {
                return false;
            }

            @Override
            public void cancel() {

            }

            @Override
            public int interestOps() {
                return 0;
            }

            @Override
            public SelectionKey interestOps(int ops) {
                return null;
            }

            @Override
            public int readyOps() {
                return 0;
            }
        };

        SelectionKey skey = Mockito.mock(SelectionKey.class);
        field = t.getClass().getSuperclass().getDeclaredField("selector");
        field.setAccessible(true);
        //   Selector se = Mockito.mock(Selector.class);
        // field.set(t, se);
        // Mockito.when(c.keyFor(se)).thenReturn(skey);
        t.run();
    }

}