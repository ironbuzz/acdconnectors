package com.nice.saas.wfo.acd.cisco.common;


import org.mockito.Mockito;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import org.junit.Test;

import static org.junit.Assert.*;

public class CsvWriterTest {
    @Test
    public void convertToCsv() throws java.sql.SQLException, IOException {
        ResultSet resultSetMock = Mockito.mock(ResultSet.class);
        Mockito.when(resultSetMock.getMetaData()).thenReturn(new ResultSetMetaData() {
            @Override
            public int getColumnCount() throws SQLException {
                return 2;
            }

            @Override
            public boolean isAutoIncrement(int column) throws SQLException {
                return false;
            }

            @Override
            public boolean isCaseSensitive(int column) throws SQLException {
                return false;
            }

            @Override
            public boolean isSearchable(int column) throws SQLException {
                return false;
            }

            @Override
            public boolean isCurrency(int column) throws SQLException {
                return false;
            }

            @Override
            public int isNullable(int column) throws SQLException {
                return 0;
            }

            @Override
            public boolean isSigned(int column) throws SQLException {
                return false;
            }

            @Override
            public int getColumnDisplaySize(int column) throws SQLException {
                return 0;
            }

            @Override
            public String getColumnLabel(int column) throws SQLException {
                if (column == 1) {
                    return "Name";
                } else {
                    return "SecondName";
                }
            }

            @Override
            public String getColumnName(int column) throws SQLException {
                if (column == 1) {
                    return "Name";
                } else {
                    return "SecondName";
                }
            }

            @Override
            public String getSchemaName(int column) throws SQLException {
                return null;
            }

            @Override
            public int getPrecision(int column) throws SQLException {
                return 0;
            }

            @Override
            public int getScale(int column) throws SQLException {
                return 0;
            }

            @Override
            public String getTableName(int column) throws SQLException {
                return null;
            }

            @Override
            public String getCatalogName(int column) throws SQLException {
                return null;
            }

            @Override
            public int getColumnType(int column) throws SQLException {
                return 0;
            }

            @Override
            public String getColumnTypeName(int column) throws SQLException {
                return null;
            }

            @Override
            public boolean isReadOnly(int column) throws SQLException {
                return false;
            }

            @Override
            public boolean isWritable(int column) throws SQLException {
                return false;
            }

            @Override
            public boolean isDefinitelyWritable(int column) throws SQLException {
                return false;
            }

            @Override
            public String getColumnClassName(int column) throws SQLException {
                return null;
            }

            @Override
            public <T> T unwrap(Class<T> iface) throws SQLException {
                return null;
            }

            @Override
            public boolean isWrapperFor(Class<?> iface) throws SQLException {
                return false;
            }
        });
        Mockito.when(resultSetMock.getString(1)).thenReturn("Vasya");
        Mockito.when(resultSetMock.getString(2)).thenReturn("Ivanov");
        Mockito.when(resultSetMock.next()).thenReturn(true).thenReturn(false);
       String csv = CsvWriter.convertToCsv(resultSetMock, "|");
       String newLine = System.getProperty("line.separator");
       assertEquals(csv, "Name|SecondName" + newLine +"Vasya|Ivanov" + newLine);

    }

}