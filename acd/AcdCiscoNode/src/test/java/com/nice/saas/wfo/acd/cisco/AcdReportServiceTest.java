package com.nice.saas.wfo.acd.cisco;

import com.nice.saas.wfo.acd.cisco.common.Constants;
import com.nice.saas.wfo.comp.manager.asc.generic.*;
import org.junit.Test;
import org.mockito.Mockito;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;


/**
 * //informix: jdbc:informix-sqli://172.21.13.198:1504/db_cra:INFORMIXSERVER=cuccx115_uccx
 * //MSSQL: jdbc:jtds:sqlserver://172.21.13.87:1433/ic115_hds
 */
public class AcdReportServiceTest {

    private GenericLogger logger = Mockito.mock(GenericLogger.class);

    AcdReportService rs = new AcdReportService() {
        @Override
        public void addLogger(GenericLogger logger) {
            super.addLogger(logger);
        }
    };

    private String getURLTemplate(String reportType) {
        switch (reportType) {
            case Constants.AcdCiscoUcceReportType:
                return "jdbc:jtds:sqlserver://{host}:{port}/{schema}";
            case Constants.AcdCiscoUccxReportType:
                return "jdbc:informix-sqli://{host}:{port}/{schema}:INFORMIXSERVER={server}";
            default:
                throw new RuntimeException("unknown report type!!!");
        }
    }

    private void getReportService(String reportType) {
        String acdType = "";
        switch (reportType) {
            case Constants.AcdCiscoUcceReportType:
                acdType = Constants.AcdCiscoUcceReportType;
                break;
            case Constants.AcdCiscoUccxReportType:
                acdType = Constants.AcdCiscoUccxReportType;
                break;
            default:
                throw new RuntimeException("unknown report type!!!");
        }
        rs.setAcdType(acdType);
        rs.addLogger(logger);
    }


    @Test
    public void initUCCX() throws Exception {

        String acdType = Constants.AcdCiscoUccxReportType;
        getReportService(acdType);
        Map<String, String> params = new HashMap<>();
        String expected = "jdbc:informix-sqli://172.21.13.198:1504/db_cra:INFORMIXSERVER=cuccx115_uccx";

        params.put("url", getURLTemplate(acdType));
        params.put("host", "172.21.13.198");
        params.put("port", "1504");
        params.put("schema", "db_cra");
        params.put("server", "cuccx115_uccx");
        params.put("username", "dummy");
        params.put("password", "dummy");
        rs.init(params);
        Field field = rs.getClass().getSuperclass().getDeclaredField("url");
        field.setAccessible(true);
        assertEquals(field.get(rs), expected);
    }

    @Test
    public void initUCCE() throws Exception {
        String acdType = Constants.AcdCiscoUcceReportType;
        getReportService(acdType);
        Map<String, String> params = new HashMap<>();
        String expected = "jdbc:jtds:sqlserver://172.21.13.198:1504/ic115_hds";

        params.put("url", getURLTemplate(acdType));
        params.put("host", "172.21.13.198");
        params.put("port", "1504");
        params.put("schema", "ic115_hds");
        params.put("server", "");
        params.put("username", "dummy");
        params.put("password", "dummy");
        rs.init(params);
        Field field = rs.getClass().getSuperclass().getDeclaredField("url");
        field.setAccessible(true);
        assertEquals(field.get(rs), expected);
    }

    // missing parameter in UCCX

    @Test(expected = GeneralError.class)
    public void initUCCXMissingHost() throws Exception {
        String acdType = Constants.AcdCiscoUccxReportType;
        getReportService(acdType);
        Map<String, String> params = new HashMap<>();
        String expected = "jdbc:informix-sqli://172.21.13.198:1504/db_cra:INFORMIXSERVER=cuccx115_uccx";

        params.put("url", getURLTemplate(acdType));
        params.put("host", "");
        params.put("port", "1504");
        params.put("schema", "db_cra");
        params.put("server", "cuccx115_uccx");
        params.put("username", "dummy");
        params.put("password", "dummy");
        rs.init(params);
        Field field = rs.getClass().getSuperclass().getDeclaredField("url");
        field.setAccessible(true);
        assertEquals(field.get(rs), expected);
    }

    @Test(expected = GeneralError.class)
    public void initUCCXMissingPort() throws Exception {
        String acdType = Constants.AcdCiscoUccxReportType;
        getReportService(acdType);
        Map<String, String> params = new HashMap<>();
        String expected = "jdbc:informix-sqli://172.21.13.198:1504/db_cra:INFORMIXSERVER=cuccx115_uccx";

        params.put("url", getURLTemplate(acdType));
        params.put("host", "172.21.13.198");
        params.put("port", "");
        params.put("schema", "db_cra");
        params.put("server", "cuccx115_uccx");
        params.put("username", "dummy");
        params.put("password", "dummy");
        rs.init(params);
        Field field = rs.getClass().getSuperclass().getDeclaredField("url");
        field.setAccessible(true);
        assertEquals(field.get(rs), expected);
    }

    @Test(expected = GeneralError.class)
    public void initUCCXMissingSchema() throws Exception {
        String acdType = Constants.AcdCiscoUccxReportType;
        getReportService(acdType);
        Map<String, String> params = new HashMap<>();
        String expected = "jdbc:informix-sqli://172.21.13.198:1504/db_cra:INFORMIXSERVER=cuccx115_uccx";

        params.put("url", getURLTemplate(acdType));
        params.put("host", "172.21.13.198");
        params.put("port", "1504");
        params.put("schema", "");
        params.put("server", "cuccx115_uccx");
        params.put("username", "dummy");
        params.put("password", "dummy");
        rs.init(params);
        Field field = rs.getClass().getSuperclass().getDeclaredField("url");
        field.setAccessible(true);
        assertEquals(field.get(rs), expected);
    }

    @Test(expected = GeneralError.class)
    public void initUCCXMissingServer() throws Exception {
        String acdType = Constants.AcdCiscoUccxReportType;
        getReportService(acdType);
        Map<String, String> params = new HashMap<>();
        String expected = "jdbc:informix-sqli://172.21.13.198:1504/db_cra:INFORMIXSERVER=cuccx115_uccx";

        params.put("url", getURLTemplate(acdType));
        params.put("host", "172.21.13.198");
        params.put("port", "1504");
        params.put("schema", "");
        params.put("server", "cuccx115_uccx");
        params.put("username", "dummy");
        params.put("password", "dummy");
        rs.init(params);
        Field field = rs.getClass().getSuperclass().getDeclaredField("url");
        field.setAccessible(true);
        assertEquals(field.get(rs), expected);
    }

    // missing parameter in UCCE

    @Test(expected = GeneralError.class)
    public void initUCCEMissingHost() throws Exception {
        String acdType = Constants.AcdCiscoUcceReportType;
        getReportService(acdType);
        Map<String, String> params = new HashMap<>();
        String expected = "jdbc:jtds:sqlserver://172.21.13.198:1504/ic115_hds";

        params.put("url", getURLTemplate(acdType));
        params.put("host", "");
        params.put("port", "1504");
        params.put("schema", "ic115_hds");
        params.put("server", "");
        params.put("username", "dummy");
        params.put("password", "dummy");
        rs.init(params);
        Field field = rs.getClass().getSuperclass().getDeclaredField("url");
        field.setAccessible(true);
        assertEquals(field.get(rs), expected);
    }

    @Test(expected = GeneralError.class)
    public void initUCCEMissingPort() throws Exception {
        String acdType = Constants.AcdCiscoUcceReportType;
        getReportService(acdType);
        Map<String, String> params = new HashMap<>();
        String expected = "jdbc:jtds:sqlserver://172.21.13.198:1504/ic115_hds";

        params.put("url", getURLTemplate(acdType));
        params.put("host", "172.21.13.198");
        params.put("port", "");
        params.put("schema", "ic115_hds");
        params.put("server", "");
        params.put("username", "dummy");
        params.put("password", "dummy");
        rs.init(params);
        Field field = rs.getClass().getSuperclass().getDeclaredField("url");
        field.setAccessible(true);
        assertEquals(field.get(rs), expected);
    }

    @Test(expected = GeneralError.class)
    public void initUCCEMissingSchema() throws Exception {
        String acdType = Constants.AcdCiscoUcceReportType;
        getReportService(acdType);
        Map<String, String> params = new HashMap<>();
        String expected = "jdbc:jtds:sqlserver://172.21.13.198:1504/ic115_hds";

        params.put("url", getURLTemplate(acdType));
        params.put("host", "172.21.13.198");
        params.put("port", "1504");
        params.put("schema", "");
        params.put("server", "");
        params.put("username", "dummy");
        params.put("password", "dummy");
        rs.init(params);
        Field field = rs.getClass().getSuperclass().getDeclaredField("url");
        field.setAccessible(true);
        assertEquals(field.get(rs), expected);
    }

    @Test
    public void connect () {
        String acdType = Constants.AcdCiscoUccxReportType;
        getReportService(acdType);
        Map<String, String> params = new HashMap<>();


        params.put("url", getURLTemplate(acdType));
        params.put("host", "127.0.0.1");
        params.put("port", "1504");
        params.put("schema", "db_cra");
        params.put("server", "cuccx115_uccx");
        params.put("username", "dummy");
        params.put("password", "dummy");
        rs.init(params);
        rs.acdErrorHandler= new ACDErrorHandler(){
            public void generalError(GeneralError error) {
                System.out.println("Error" + error.toString());

            }

            public void connectionError(ConnectionError error) {
                System.out.println("ConnectionError" + error.toString());

            }
        };
        rs.connect();
    }

    @Test
    public void getAcdType() throws NoSuchFieldException, IllegalAccessException {
        String acdType = Constants.AcdCiscoUccxReportType;
        getReportService(acdType);
        Map<String, String> params = new HashMap<>();


        params.put("url", getURLTemplate(acdType));
        params.put("host", "127.0.0.1");
        params.put("port", "1504");
        params.put("schema", "db_cra");
        params.put("server", "cuccx115_uccx");
        params.put("username", "dummy");
        params.put("password", "dummy");
        rs.init(params);
        assertEquals(rs.getAcdType(), Constants.AcdCiscoUccxReportType);
        Field field = rs.getClass().getSuperclass().getDeclaredField("acdType");
        field.setAccessible(true);
        field.set(rs, "vasya");
        assertEquals(rs.getAcdType(), "vasya");
    }

    @Test(expected = GeneralError.class)
    public void getReportData(){
        String acdType = Constants.AcdCiscoUccxReportType;
        getReportService(acdType);
        Map<String, String> params = new HashMap<>();


        params.put("url", getURLTemplate(acdType));
        params.put("host", "127.0.0.1");
        params.put("port", "1504");
        params.put("schema", "db_cra");
        params.put("server", "cuccx115_uccx");
        params.put("username", "dummy");
        params.put("password", "dummy");
        rs.init(params);

        rs.getReportData(Instant.now().minus(180, ChronoUnit.MINUTES), Instant.now());
    }

    @Test(expected = GeneralError.class)
    public void getReportData1(){
        String acdType = Constants.AcdCiscoUccxReportType;
        getReportService(acdType);
        Map<String, String> params = new HashMap<>();


        params.put("url", getURLTemplate(acdType));
        params.put("host", "127.0.0.1");
        params.put("port", "1504");
        params.put("schema", "db_cra");
        params.put("server", "cuccx115_uccx");
        params.put("username", "dummy");
        params.put("password", "dummy");
        rs.init(params);
        rs.acdErrorHandler= new ACDErrorHandler(){
            public void generalError(GeneralError error) {
                System.out.println("Error" + error.toString());

            }

            public void connectionError(ConnectionError error) {
                System.out.println("ConnectionError" + error.toString());

            }
        };

        rs.getReportData(Instant.now().minus(180, ChronoUnit.MINUTES), Instant.now());
    }

    @Test(expected = GeneralError.class)
    public void getReportData2() {
        String acdType = Constants.AcdCiscoUccxReportType;
        getReportService(acdType);
        Map<String, String> params = new HashMap<>();


        params.put("url", getURLTemplate(acdType));
        params.put("host", "127.0.0.1");
        params.put("port", "1504");
        params.put("schema", "db_cra");
        params.put("server", "cuccx115_uccx");
        params.put("username", "dummy");
        params.put("password", "dummy");
        rs.init(params);
        rs.setQuery("q?qqq?");
        rs.setDbClassName("com.informix.jdbc.IfxDriver");
        rs.acdErrorHandler= new ACDErrorHandler(){
            public void generalError(GeneralError error) {
                System.out.println("Error" + error.toString());

            }

            public void connectionError(ConnectionError error) {
                System.out.println("ConnectionError" + error.toString());

            }
        };

        AcdReportService mrs = Mockito.mock(AcdReportService.class);
        Mockito.when(mrs.connect()).thenReturn(true);

        Instant stop = Instant.now();
        Instant start = stop.minus(180, ChronoUnit.MINUTES);
        Mockito.when(mrs.getReportData(start, stop)).thenReturn(rs.getReportData(start, stop));
        mrs.getReportData(start, stop);
    }

    @Test
    public void getReportVersion (){
        assertEquals(rs.getReportVersion("Forecast"), 0);
    }

    @Test
    public void isResourceAvailable (){

        rs.isResourceAvailable();
    }

    @Test
    public void terminate(){
        rs.terminate();
    }
}