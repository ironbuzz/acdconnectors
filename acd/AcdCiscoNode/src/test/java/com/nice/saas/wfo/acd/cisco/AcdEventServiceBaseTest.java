package com.nice.saas.wfo.acd.cisco;

import com.nice.saas.wfo.acd.cisco.common.TcpReceiverBase;
import com.nice.saas.wfo.acd.cisco.interfaces.GenericAcdEventTranslator;
import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import com.nice.saas.wfo.acd.cisco.interfaces.MessageParser;
import com.nice.saas.wfo.acd.cisco.interfaces.Receiver;
import com.nice.saas.wfo.acd.rta.GenericACDEvent;
import com.nice.saas.wfo.acd.rta.GenericACDEventListener;
import com.nice.saas.wfo.comp.manager.asc.generic.ACDErrorHandler;
import com.nice.saas.wfo.comp.manager.asc.generic.ConnectionError;
import com.nice.saas.wfo.comp.manager.asc.generic.GeneralError;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import org.junit.Test;
import org.mockito.Mockito;

import java.awt.event.ActionEvent;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.*;

public class AcdEventServiceBaseTest {
    GenericLogger logger = new GenericLogger() {
        @Override
        public void info(String s) {

        }

        @Override
        public void error(String s, Throwable throwable, Object... objects) {

        }

        @Override
        public void warning(String s) {

        }
    };
    AcdEventServiceBase service = new AcdEventServiceBase() {


        @Override
        protected void senderPoolCore() {

        }

        @Override
        protected void eventsProcessorPoolCore(Message message) {

        }
    };
    TcpReceiverBase tb = new TcpReceiverBase() {
        private GenericLogger logger;

        @Override
        public List<ByteBuffer> receive() {
            return null;
        }

        @Override
        public void addLogger(GenericLogger logger) {
            this.logger = logger;
        }
    };
    MessageParser parser = new MessageParser() {
        private GenericLogger logger;
        @Override
        public Message parse(Object rawMessage) {
            return null;
        }

        @Override
        public void addLogger(GenericLogger logger) {
            this.logger= logger;
        }
    };
    GenericAcdEventTranslator translator = new GenericAcdEventTranslator() {
        private GenericLogger logger;
        @Override
        public GenericACDEvent translate(Message message) {
            return null;
        }

        @Override
        public void addLogger(GenericLogger logger) {
            this.logger= logger;
        }
    };
    @Test
    public void init() throws Exception {
        service.receiver = tb;

        Field field = service.getClass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 100);
        field = service.getClass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 100);

        service.init();
        assertNotNull(service.receiver);
        assertNotNull(service.messages);
        field = service.getClass().getSuperclass().getDeclaredField("eventsProcessorPool");
        field.setAccessible(true);
        assertNotNull(field.get(service));
    }

    @Test
    public void addEventListener() throws Exception {
        service.addEventListener(new GenericACDEventListener() {
            @Override
            public void processEvents(List<GenericACDEvent> list) {

            }

            @Override
            public void processEvent(GenericACDEvent genericACDEvent) {

            }
        });
        Field field = service.getClass().getSuperclass().getDeclaredField("acdEventListeners");
        field.setAccessible(true);
        List<GenericACDEventListener> acdEventListeners = (List<GenericACDEventListener>) field.get(service);
        assertNotNull(acdEventListeners);
        assertEquals(acdEventListeners.size(), 1);
    }

    @Test
    public void addLogger() throws Exception {
        service.receiver = tb;
        service.messageParser = parser;
        service.acdMessageTranslator = translator;

        service.addLogger(logger);
        assertNotNull(service.logger);
        Field field = tb.getClass().getDeclaredField("logger");
        field.setAccessible(true);
        assertNotNull(field.get(tb));
        field = parser.getClass().getDeclaredField("logger");
        field.setAccessible(true);
        assertNotNull(field.get(parser));
        field = translator.getClass().getDeclaredField("logger");
        field.setAccessible(true);
        assertNotNull(field.get(translator));
    }

    @Test
    public void start() throws Exception {

        service.receiver = tb;
        service.messageParser = parser;
        service.acdMessageTranslator = translator;
        service.addLogger(logger);
        Field field = service.getClass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 100);
        field = service.getClass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 100);
        service.init();
        service.start();

    }

    @Test
    public void destroy() throws Exception {
        service.receiver = tb;
        service.messageParser = parser;
        service.acdMessageTranslator = translator;
        service.addLogger(logger);
        Field field = service.getClass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 100);
        field = service.getClass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 100);
        service.init();
        service.destroy();
        field = service.getClass().getSuperclass().getDeclaredField("isStarted");
        field.setAccessible(true);
        assertFalse(((AtomicBoolean)field.get(service)).get());
    }

    @Test
    public void actionPerformed() throws Exception {
        service.receiver = tb;
        service.messageParser = parser;
        service.acdMessageTranslator = translator;
        service.addLogger(logger);
        Field field = service.getClass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 100);
        field = service.getClass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 100);
        service.init();
        service.acdErrorHandler= new ACDErrorHandler(){
            public void generalError(GeneralError error) {
                System.out.println("Error" + error.toString());

            }

             public void connectionError(ConnectionError error) {
                System.out.println("ConnectionError" + error.toString());

            }
        };
        service.actionPerformed(new ActionEvent(this, 1, "connected"));
        service.actionPerformed(new ActionEvent(this, 1, "disconnected"));
        field = service.getClass().getSuperclass().getDeclaredField("isStarted");
        field.setAccessible(true);
        assertFalse(((AtomicBoolean)field.get(service)).get());
        service.actionPerformed(new ActionEvent(new ConnectionError("q"), 1, "failure"));
        field = service.getClass().getSuperclass().getDeclaredField("isStarted");
        field.setAccessible(true);
        assertFalse(((AtomicBoolean)field.get(service)).get());

        service.actionPerformed(new ActionEvent(new GeneralError("q"), 1, "failure"));
        field = service.getClass().getSuperclass().getDeclaredField("isStarted");
        field.setAccessible(true);
        assertFalse(((AtomicBoolean)field.get(service)).get());
    }

    @Test(expected = NullPointerException.class)
    public void start1() throws NoSuchFieldException, IllegalAccessException {
        service.receiver = tb;
        service.messageParser = parser;
        service.acdMessageTranslator = translator;
        service.addLogger(logger);
        service.receiver=null;
        service.start();
    }

    @Test
    public void startReceiveMessages() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = service.getClass().getSuperclass().getDeclaredMethod("startReceiveMessages");
        method.setAccessible(true);
        Receiver r =Mockito.mock(TcpReceiverBase.class);
        ByteBuffer b = ByteBuffer.allocateDirect(2);
        List<ByteBuffer> bl = new ArrayList<ByteBuffer>();
        bl.add(b);
        service.receiver = r;
        Mockito.when(r.receive()).thenReturn(bl);
        method.invoke(service);
    }

    @Test
    public void startEventBulkProcessorPool() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = service.getClass().getSuperclass().getDeclaredMethod("startEventBulkProcessorPool");
        method.setAccessible(true);
        BlockingQueue<GenericACDEvent> events = new ArrayBlockingQueue<GenericACDEvent>(12);
        GenericACDEvent ev =new GenericACDEvent();

        events.add(ev);
        method.invoke(service);
    }

}