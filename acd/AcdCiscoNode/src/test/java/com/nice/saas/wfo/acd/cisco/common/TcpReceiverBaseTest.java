package com.nice.saas.wfo.acd.cisco.common;


import com.nice.saas.wfo.comp.manager.asc.generic.GeneralError;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import org.junit.Test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import static org.junit.Assert.*;

public class TcpReceiverBaseTest {


    @Test
    public void addListener() throws NoSuchFieldException, IllegalAccessException {
        tb.addListener(new ActionListener() {
            @Override
            public  void actionPerformed(ActionEvent e) {

            }
        });
        Field field = tb.getClass().getSuperclass().getDeclaredField("listeners");
        field.setAccessible(true);
        assertEquals(((List<ActionListener>)field.get(tb)).size(), 1);
    }

    @Test
    public void destroy() throws Exception {
        tb.addListener(new ActionListener() {
            @Override
            public  void actionPerformed(ActionEvent e) {

            }
        });
        Field field = tb.getClass().getSuperclass().getDeclaredField("listeners");
        field.setAccessible(true);
        assertEquals(((List<ActionListener>)field.get(tb)).size(), 1);
        tb.destroy();
        assertEquals(((List<ActionListener>)field.get(tb)).size(), 0);
    }

    TcpReceiverBase tb = new TcpReceiverBase() {
        private GenericLogger logger;

        @Override
        public List<ByteBuffer> receive() {
            return null;
        }


    };
    @Test
    public void setBuffersQueueCapacity() throws NoSuchFieldException, IllegalAccessException {
        tb.setBuffersQueueCapacity(2);
        Field field= tb.getClass().getSuperclass().getDeclaredField("buffers");
        field.setAccessible(true);
        assertEquals(((BlockingQueue<ByteBuffer>)field.get(tb)).remainingCapacity(), 2);
    }

    @Test
    public void concatByteBuffers() {
        byte[] ar1 =new byte[] {1,2};
        byte[] ar2 =new byte[] {3,4, 5};
        ByteBuffer b1 = ByteBuffer.wrap(ar1);
        ByteBuffer b2 = ByteBuffer.wrap(ar2);
        ByteBuffer newb = tb.concatByteBuffers(b1, b2, 2, 3);
        assertEquals(newb.remaining(), 5);
        byte[] arr = new byte[5];
        newb.get(arr, 0, 5);
        assertEquals(arr.length , 5);
        assertEquals(arr[0], 1);
        assertEquals(arr[1], 2);
        assertEquals(arr[2], 3);
        assertEquals(arr[3], 4);
        assertEquals(arr[4], 5);

    }

    @Test
    public void addLogger() throws NoSuchMethodException {
        GenericLogger logger = new GenericLogger() {
            @Override
            public void info(String s) {

            }

            @Override
            public void error(String s, Throwable throwable, Object... objects) {

            }

            @Override
            public void warning(String s) {

            }
        };
        tb.addLogger(logger);
        assertNotNull(tb.logger);

    }

    TcpClient tcpClientTest =  new TcpClient("127.0.0.1", 1111, 20, 30, 40, 50) {
        @Override
        protected  void onRead(ByteBuffer buf) throws Exception {

        }

        @Override
        protected  void onConnected() throws Exception {

        }

        @Override
        protected  void onDisconnected() throws Exception {

        }

        @Override
        protected  void onFailure(Exception e) throws Exception {

        }
    };

    @Test(expected = GeneralError.class)
    public void send() throws NoSuchFieldException, IllegalAccessException {
        Field field = tb.getClass().getSuperclass().getDeclaredField("client");
        field.setAccessible(true);
        field.set(tb,tcpClientTest );
        tb.send(ByteBuffer.allocateDirect(10));
    }



}