package com.nice.saas.wfo.acd.cisco.common;

import org.junit.Test;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.util.concurrent.atomic.AtomicInteger;


public class InvokeIdGeneratorTest {
    @Test
    public void generateInvokeId() throws NoSuchFieldException, IllegalAccessException {
        InvokeIdGenerator i = new InvokeIdGenerator();
        InvokeIdGenerator.reset();
        Field field = i.getClass().getDeclaredField("invokeId");
        field.setAccessible(true);
        assertEquals(((AtomicInteger)field.get(i)).get(), 0);
        int res = InvokeIdGenerator.generateInvokeId();
        assertEquals(res, 0);
        res = InvokeIdGenerator.generateInvokeId();
        assertEquals(res, 1);
        res =InvokeIdGenerator.generateInvokeId();
        assertEquals(res, 2);
    }


    @Test
    public void reset() {
        InvokeIdGenerator.reset();
        int res = InvokeIdGenerator.generateInvokeId();
        assertEquals(res, 0);
    }

}