package com.nice.saas.wfo.acd.cisco.uc.messages;

import com.nice.saas.wfo.acd.cisco.common.InvokeIdGenerator;
import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;

import java.nio.ByteBuffer;


/**
 * HeartbeatReqUcMessage - heartbeat request message
 */
public class HeartbeatReqUcMessage extends UcMessage {

    public HeartbeatReqUcMessage() {
        messageTypeId = ConstantsUc.UcMessageTypeIds.get(HeartbeatReqUcMessage.class);

        fixedFields.put(ConstantsUc.UcMessageFields.InvokeId, InvokeIdGenerator.generateInvokeId());
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.InvokeId, Integer.class);
    }

    /**
     * Deserialize is not supported
     * @param byteBuffer
     * @return
     */
    @Override
    public Message deserialize(ByteBuffer byteBuffer) {
        throw new UnsupportedOperationException();
    }
}