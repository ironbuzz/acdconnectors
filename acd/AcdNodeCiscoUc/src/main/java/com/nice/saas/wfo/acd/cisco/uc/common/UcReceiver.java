package com.nice.saas.wfo.acd.cisco.uc.common;

import com.nice.saas.wfo.acd.cisco.common.TcpReceiverBase;
import com.nice.saas.wfo.comp.manager.asc.generic.GeneralError;
import org.springframework.stereotype.Component;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * UCCX receiver listens on TCP socket and translates received byte data to UC raw (byte buffer) messages
 */
@Component(value = "UcReceiver")
public class UcReceiver extends TcpReceiverBase {
    // region constants
    private final String className = "UcReceiver";

    // endregion

    // region Receiver implementation


    /**
     * Listen on TCP socket and translate received byte data to UC raw (byte buffer) messages
     * @return
     */
    // Returns ByteBuffer representing separate UC message
    @Override
    public List<ByteBuffer> receive() {
        final String functionName = "receive()";
        List<ByteBuffer> retArray = new ArrayList<>();
        try {
            if (buffer == null) {
                buffer = buffers.take();
            }
            while (currentPayloadLength == 0) {
                if (buffer.remaining() >= 4) {
                    currentPayloadLength = buffer.getInt(0) + 8; // UC message body length + header length (8 bytes)
                } else {
                    ByteBuffer nextBuffer = buffers.take();
                    buffer = concatByteBuffers(buffer, nextBuffer, buffer.remaining(), nextBuffer.remaining());
                }
            }
            while (buffer.remaining() < currentPayloadLength) {
                ByteBuffer nextBuffer = buffers.take();
                buffer = concatByteBuffers(buffer, nextBuffer, buffer.remaining(), nextBuffer.remaining());
            }
            while (buffer != null && (buffer.remaining() >= currentPayloadLength) && (buffer.remaining() > 0)) {
                byte[] tempArray = new byte[currentPayloadLength];
                buffer.get(tempArray, 0, tempArray.length);
                retArray.add(ByteBuffer.wrap(tempArray));
                if (buffer.remaining() > 0) {
                    // create new buffer and put the rest to it
                    ByteBuffer tempBuffer = ByteBuffer.allocateDirect(buffer.remaining());
                    tempBuffer.put(buffer);
                    buffer = tempBuffer;
                    buffer.rewind();
                    currentPayloadLength = buffer.getInt(buffer.position()) + 8;
                } else {
                    currentPayloadLength = 0;
                    buffer = null;
                }
            }
        }
        catch (Exception e) {
            throw new GeneralError(String.format("%s.%s: Exception: ", className, functionName), e);
        }

        return retArray;
    }

    // endregion

}
