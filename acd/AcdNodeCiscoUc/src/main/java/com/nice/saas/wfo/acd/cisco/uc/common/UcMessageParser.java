package com.nice.saas.wfo.acd.cisco.uc.common;

import com.nice.saas.wfo.acd.cisco.interfaces.*;
import com.nice.saas.wfo.acd.cisco.uc.messages.*;
import com.nice.saas.wfo.comp.manager.asc.generic.*;
import org.springframework.stereotype.Component;


import java.nio.ByteBuffer;

/**
 * UC message parser (translates raw byte buffer to UC message)
 */
@Component(value = "UcMessageParser")
public class UcMessageParser implements MessageParser<ByteBuffer> {
    private final String className = "UcMessageParser";

    private GenericLogger logger;

    /**
     * Translates raw byte buffer to UC message
     * @param rawMessage
     * @return
     */
    @Override
    public Message parse(ByteBuffer rawMessage) {
        final String functionName = "parse()";
        UcMessage ucMessage = null;
        int bodyLength = rawMessage.getInt();
        int messageTypeId = rawMessage.getInt();
        try {
            ucMessage = (UcMessage) ConstantsUc.UcMessageTypes.get(messageTypeId).getConstructor().newInstance();
            if (ucMessage == null) {
                return null;
            }
            ucMessage.bodyLength = bodyLength;
            ucMessage =  (UcMessage) ucMessage.deserialize(rawMessage);

        } catch (Exception e) {
            throw new GeneralError(String.format("%s.%s: Exception: ", className, functionName), e);
        }
        return ucMessage;
    }

    @Override
    public void addLogger(GenericLogger logger) {
        this.logger = logger;
    }
}
