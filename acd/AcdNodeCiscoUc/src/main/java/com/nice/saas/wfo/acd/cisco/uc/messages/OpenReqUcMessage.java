package com.nice.saas.wfo.acd.cisco.uc.messages;


import com.nice.saas.wfo.acd.cisco.common.InvokeIdGenerator;
import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.ByteBuffer;

/**
 * OpenReqUcMessage - sent by client to UC server to open the communication
 */
@Component
public class OpenReqUcMessage extends UcMessage {

    @Value("${cisco.uc.acd.rta.openReqUcMessage.versionNumber}")
    public void setVersionNumber(int versionNumber) {
        fixedFields.put(ConstantsUc.UcMessageFields.VersionNumber, versionNumber);
    }

    @Value("${cisco.uc.acd.rta.openReqUcMessage.uccxIdleTimeout}")
    public void setUcIdleTimeout(int uccxIdleTimeout) {
        fixedFields.put(ConstantsUc.UcMessageFields.IdleTimeout, uccxIdleTimeout);
    }

    public int getUcIdleTimeout() {

        return (int) fixedFields.get(ConstantsUc.UcMessageFields.IdleTimeout);
    }

    @Value("${cisco.uc.acd.rta.openReqUcMessage.servicesRequested}")
    public void setServicesRequested(int servicesRequested) {
        fixedFields.put(ConstantsUc.UcMessageFields.ServicesRequested, servicesRequested);
    }

    @Value("${cisco.uc.acd.rta.openReqUcMessage.agentStateMask}")
    public void setAgentStateMask(String agentStateMask) {
        fixedFields.put(ConstantsUc.UcMessageFields.AgentStateMask, Integer.valueOf(agentStateMask));
    }

    /**
     *
     */
    public OpenReqUcMessage() {
        messageTypeId = ConstantsUc.UcMessageTypeIds.get(OpenReqUcMessage.class);

        fixedFields.put(ConstantsUc.UcMessageFields.InvokeId, InvokeIdGenerator.generateInvokeId());
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.InvokeId, Integer.class);

        fixedFields.put(ConstantsUc.UcMessageFields.VersionNumber, ConstantsUc.DefaultUcVersionNumber);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.VersionNumber, Integer.class);

        fixedFields.put(ConstantsUc.UcMessageFields.IdleTimeout, ConstantsUc.DefaultUcIdleTimeout);  // sec
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.IdleTimeout, Integer.class);

        fixedFields.put(ConstantsUc.UcMessageFields.Reserved, ConstantsUc.DefaultPeripheralId);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.Reserved, Integer.class);

        fixedFields.put(ConstantsUc.UcMessageFields.ServicesRequested, ConstantsUc.DefaultServicesRequested);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.ServicesRequested, Integer.class);

        fixedFields.put(ConstantsUc.UcMessageFields.CallMsgMask, 0);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.CallMsgMask, Integer.class);

        fixedFields.put(ConstantsUc.UcMessageFields.AgentStateMask, ConstantsUc.DefaultAgentStateMask);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.AgentStateMask, Integer.class);

        fixedFields.put(ConstantsUc.UcMessageFields.ConfigMsgMask, 0);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.ConfigMsgMask, Integer.class);

        byte[] arr = new byte[12];
        fixedFields.put(ConstantsUc.UcMessageFields.UnusedBlock1, arr);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.UnusedBlock1, null);
        fixedFieldsLength.put(ConstantsUc.UcMessageFields.UnusedBlock1, 12);

        floatingFields.put(ConstantsUc.UcMessageFields.ClientId, new byte[] {(byte) 1, (byte) 0});
        floatingFieldsTypes.put(ConstantsUc.UcMessageFields.ClientId, null);
        floatingFieldsLength.put(ConstantsUc.UcMessageFields.ClientId, (byte)2);

        floatingFields.put(ConstantsUc.UcMessageFields.ClientPassword, new byte[1]);
        floatingFieldsTypes.put(ConstantsUc.UcMessageFields.ClientPassword, null);
        floatingFieldsLength.put(ConstantsUc.UcMessageFields.ClientPassword, (byte)1);
    }

    @Override
    public Message deserialize(ByteBuffer byteBuffer) {
        throw new UnsupportedOperationException();
    }
}
