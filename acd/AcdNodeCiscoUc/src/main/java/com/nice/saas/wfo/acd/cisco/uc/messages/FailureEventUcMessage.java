package com.nice.saas.wfo.acd.cisco.uc.messages;

import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;

import java.nio.ByteBuffer;

/**
 * FailureEventUcMessage - sent by UC server when error occurs
 */
public class FailureEventUcMessage extends UcMessage {

    public FailureEventUcMessage() {
        messageTypeId = ConstantsUc.UcMessageTypeIds.get(FailureEventUcMessage.class);

        fixedFields.put(ConstantsUc.UcMessageFields.Status, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.Status, Integer.class);

        floatingFields.put(ConstantsUc.UcMessageFields.Text, null);
        floatingFieldsTypes.put(ConstantsUc.UcMessageFields.Text, String.class);
    }

    /**
     * Serialize is not supported
     * @return
     */
    @Override
    public ByteBuffer serialize() {
        throw new UnsupportedOperationException();
    }

}
