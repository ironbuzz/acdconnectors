package com.nice.saas.wfo.acd.cisco.uc.messages;

import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;

import java.nio.ByteBuffer;

/**
 * CloseConfUcMessage - is sent by UC server in response to CloseConfUccxReq message
 */
public class CloseConfUcMessage extends UcMessage {

    public CloseConfUcMessage() {
        messageTypeId = ConstantsUc.UcMessageTypeIds.get(CloseConfUcMessage.class);

        fixedFields.put(ConstantsUc.UcMessageFields.InvokeId, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.InvokeId, Integer.class);
    }

    @Override
    public ByteBuffer serialize() {
        throw new UnsupportedOperationException();
    }

}
