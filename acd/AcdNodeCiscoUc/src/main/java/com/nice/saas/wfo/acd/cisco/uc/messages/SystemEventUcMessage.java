package com.nice.saas.wfo.acd.cisco.uc.messages;

import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;

import java.nio.ByteBuffer;

public class SystemEventUcMessage extends UcMessage {

    public SystemEventUcMessage() {
        messageTypeId = ConstantsUc.UcMessageTypeIds.get(SystemEventUcMessage.class);

        fixedFields.put(ConstantsUc.UcMessageFields.Status, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.Status, Integer.class);

        fixedFields.put(ConstantsUc.UcMessageFields.Time, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.Time, Integer.class);

        fixedFields.put(ConstantsUc.UcMessageFields.SystemEventId, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.SystemEventId, Integer.class);

        fixedFields.put(ConstantsUc.UcMessageFields.SystemEventArg1, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.SystemEventArg1, Integer.class);

        fixedFields.put(ConstantsUc.UcMessageFields.SystemEventArg2, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.SystemEventArg2, Integer.class);

        fixedFields.put(ConstantsUc.UcMessageFields.SystemEventArg3, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.SystemEventArg3, Integer.class);

        fixedFields.put(ConstantsUc.UcMessageFields.EventDeviceType, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.EventDeviceType, Short.class);

        floatingFields.put(ConstantsUc.UcMessageFields.Text, null);
        floatingFieldsTypes.put(ConstantsUc.UcMessageFields.Text, String.class);
    }

    @Override
    public ByteBuffer serialize() {
        throw new UnsupportedOperationException();
    }
}
