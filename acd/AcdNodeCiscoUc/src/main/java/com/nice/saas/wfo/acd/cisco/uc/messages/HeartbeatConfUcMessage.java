package com.nice.saas.wfo.acd.cisco.uc.messages;

import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;

import java.nio.ByteBuffer;

/**
 * HeartbeatConfUcMessage - sent by UC server in response to HeartbeatReq message sent by client
 */
public class HeartbeatConfUcMessage extends UcMessage {

    public HeartbeatConfUcMessage() {
        messageTypeId = ConstantsUc.UcMessageTypeIds.get(HeartbeatConfUcMessage.class);

        fixedFields.put(ConstantsUc.UcMessageFields.InvokeId, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.InvokeId, Integer.class);
    }

    /**
     * Serialize is not supported
     * @return
     */
    @Override
    public ByteBuffer serialize() {
        throw new UnsupportedOperationException();
    }

}
