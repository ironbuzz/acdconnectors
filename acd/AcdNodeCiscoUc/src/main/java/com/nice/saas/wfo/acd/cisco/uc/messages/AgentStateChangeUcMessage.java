package com.nice.saas.wfo.acd.cisco.uc.messages;

import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;

import java.nio.ByteBuffer;

/**
 * AgentStateChangeUcMessage - indicates agent state change
 */
public abstract class AgentStateChangeUcMessage extends UcMessage {
    public AgentStateChangeUcMessage() {
        messageTypeId = ConstantsUc.UcMessageTypeIds.get(AgentStateChangeUcMessage.class);

        fixedFields.put(ConstantsUc.UcMessageFields.UnusedBlock1, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.UnusedBlock1, null);
        fixedFieldsLength.put(ConstantsUc.UcMessageFields.UnusedBlock1, 30);

        // agent state
        fixedFields.put(ConstantsUc.UcMessageFields.AgentState, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.AgentState, Short.class);

        // agent state reason
        fixedFields.put(ConstantsUc.UcMessageFields.EventReasonCode, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.EventReasonCode, Short.class);

        fixedFields.put(ConstantsUc.UcMessageFields.UnusedBlock2, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.UnusedBlock2, null);

        // agent ID
        floatingFields.put(ConstantsUc.UcMessageFields.AgentId, null);

    }

    /**
     * Serialize is not supported
     * @return
     */
    @Override
    public ByteBuffer serialize() {
        throw new UnsupportedOperationException();
    }
}
