package com.nice.saas.wfo.acd.cisco.uc.messages;


import com.nice.saas.wfo.acd.cisco.common.Constants;
import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;
import com.nice.saas.wfo.comp.manager.asc.generic.GeneralError;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Base class for UC messages
 */
public abstract class UcMessage implements Message {
    private final String className = "UcMessage";
    /**
     * UC message type ID
     */
    // fixedFields
    protected int messageTypeId;

    public int getMessageTypeId() {
        return messageTypeId;
    }

    /**
     * Body length
     */
    public int bodyLength;

    /**
     * Fixed fields map
     */
    public final HashMap<ConstantsUc.UcMessageFields, Object> fixedFields = new LinkedHashMap<>();
    /**
     * Fixed fields types map
     */
    public final HashMap<ConstantsUc.UcMessageFields, Class<?>> fixedFieldsTypes = new LinkedHashMap<>();
    /**
     * Fixed fields length map
     */
    final HashMap<ConstantsUc.UcMessageFields, Integer> fixedFieldsLength = new LinkedHashMap<>();
    /**
     * Floating fields map
     */
    public final HashMap<ConstantsUc.UcMessageFields, Object> floatingFields = new LinkedHashMap<>();
    /**
     * Floating fields types map
     */
    final HashMap<ConstantsUc.UcMessageFields, Class<?>> floatingFieldsTypes = new LinkedHashMap<>();
    /**
     * Floating fields length map
     */
    final HashMap<ConstantsUc.UcMessageFields, Byte> floatingFieldsLength = new LinkedHashMap<>();

    /**
     * Deserialize from byte buffer
     * @param byteBuffer
     * @return
     */
    @Override
    public Message deserialize(ByteBuffer byteBuffer) {
        final String functionName = "deserialize()";
        try {
            // deserialize fixed fields
            for (ConstantsUc.UcMessageFields key : fixedFields.keySet()) {
                Class<?> fieldType = fixedFieldsTypes.get(key);
                Integer length = fixedFieldsLength.get(key);
                if (fieldType == null) {
                    byte[] tempArray = new byte[length];
                    byteBuffer.get(tempArray, 0, length);
                    fixedFields.putIfAbsent(key, tempArray);
                } else {
                    if (fieldType == Integer.class) {
                        fixedFields.putIfAbsent(key, byteBuffer.getInt());
                    } else if (fieldType == Short.class) {
                        fixedFields.putIfAbsent(key, byteBuffer.getShort());
                    } else if (fieldType == String.class) {
                        byte[] tempArray = new byte[length];
                        byteBuffer.get(tempArray, 0, length);
                        fixedFields.putIfAbsent(key, new String(tempArray, Charset.forName("UTF-8")));
                    }
                }
            }

            // deserialize floating fields

            if (floatingFields.size() == 0) {
                return this;
            }

            while (byteBuffer.hasRemaining()) {
                byte fieldDataId = byteBuffer.get();
                ConstantsUc.UcMessageFields fieldId = ConstantsUc.UcFloatingFieldsTypeIds.get(fieldDataId);

                // if the field is applicable, get its length
                int fieldDataLength = byteBuffer.get() & 0xff;

                // get data for this field
                byte[] tempArray = new byte[fieldDataLength];
                byteBuffer.get(tempArray, 0, fieldDataLength);
                // check if the field is applicable for current message
                if (!floatingFields.containsKey(fieldId)) {
                    continue;
                }
                String deserializedString = new String(tempArray, Charset.forName("UTF-8"));
                deserializedString = deserializedString.replace("\0", "");
                floatingFields.putIfAbsent(fieldId, deserializedString);
            }
        } catch (Exception e) {
            throw new GeneralError(String.format("%s.%s: Exception: ", className, functionName), e);
        }
        return this;
    }

    /**
     * Serialize to byte buffer
     * @return
     */
    @Override
    public ByteBuffer serialize() {
        final String functionName = "serialize()";
        try {
            ByteBuffer byteBuffer = ByteBuffer.allocateDirect(Constants.DefaultBufferSize);
            byteBuffer.putInt(messageTypeId);
            int totalLength = 4;
            // serialize fixed fields
            for (ConstantsUc.UcMessageFields key : fixedFields.keySet()) {
                Class<?> fieldType = fixedFieldsTypes.get(key);
                Integer length = fixedFieldsLength.get(key);
                if (fieldType == null) {
                    byte[] tempArray = (byte[]) fixedFields.get(key);
                    byteBuffer.put(tempArray);
                    totalLength += length;
                } else {
                    if (fieldType == Integer.class) {
                        byteBuffer.putInt((int) fixedFields.get(key));
                        totalLength += 4;
                    } else if (fieldType == Short.class) {
                        byteBuffer.putInt((short) fixedFields.get(key));
                        totalLength += 2;
                    } else if (fieldType == String.class) {
                        byte[] tempArray = ((String) fixedFields.get(key)).getBytes(Charset.forName("UTF-8"));
                        byteBuffer.put(tempArray);
                        totalLength += tempArray.length;
                    }
                }
            }

            // serialize floating fields

            for (ConstantsUc.UcMessageFields key : floatingFields.keySet()) {
                Class<?> fieldType = floatingFieldsTypes.get(key);
                Byte fieldDataId = ConstantsUc.UcFloatingFieldsNames.get(key);
                Byte fieldDataLength = floatingFieldsLength.get(key);
                byteBuffer.put(fieldDataId);
                byteBuffer.put(fieldDataLength);
                totalLength += 2;

                if (fieldType == null) {
                    byte[] tempArray = (byte[]) floatingFields.get(key);
                    byteBuffer.put(tempArray);
                    totalLength += fieldDataLength;
                } else {
                    if (fieldType == Integer.class) {
                        byteBuffer.putInt((int) floatingFields.get(key));
                        totalLength += 4;
                    } else if (fieldType == Short.class) {
                        byteBuffer.putInt((short) floatingFields.get(key));
                        totalLength += 2;
                    } else if (fieldType == Byte.class) {
                        byteBuffer.put((byte) floatingFields.get(key));
                        totalLength++;
                    } else if (fieldType == String.class) {
                        byte[] tempArray = ((String) floatingFields.get(key)).getBytes(Charset.forName("UTF-8"));
                        byteBuffer.put(tempArray);
                        totalLength += tempArray.length;
                    }
                }
            }
            int messageLength = totalLength + 4;
            ByteBuffer retBuffer = ByteBuffer.allocateDirect(messageLength);
            byte[] tempArray = new byte[totalLength];
            byteBuffer.rewind();
            byteBuffer.get(tempArray, 0, totalLength);
            retBuffer.putInt(messageLength - 8); // body length (whole message length -8)
            retBuffer.put(tempArray);
            retBuffer.rewind();
            return retBuffer;
        } catch (Exception e) {
            throw new GeneralError(String.format("%s.%s: Exception: ", className, functionName), e);
        }
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MessageTypeId = ");
        sb.append(messageTypeId);
        sb.append("; ");
        sb.append("BodyLength = ");
        sb.append(bodyLength);
        sb.append("; ");
        fixedFields.forEach((key, value) -> {
            if (key.toString().contains("UnusedBlock")) {
                return;
            }
            sb.append(key);
            sb.append("= ");
            sb.append(value);
            sb.append("; ");
        });
        floatingFields.forEach((key, value) -> {
            sb.append(key);
            sb.append("= ");
            sb.append(value);
            sb.append("; ");
        });
        return sb.toString();
    }
}
