package com.nice.saas.wfo.acd.cisco.uc.messages;

import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;

import java.nio.ByteBuffer;

/**
 * OpenConfUcMessage - sent by UC server in response to OpenReq message
 */
public class OpenConfUcMessage extends UcMessage {
    public OpenConfUcMessage() {
        messageTypeId = ConstantsUc.UcMessageTypeIds.get(OpenConfUcMessage.class);

        fixedFields.put(ConstantsUc.UcMessageFields.InvokeId, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.InvokeId, Integer.class);

        //
        fixedFields.put(ConstantsUc.UcMessageFields.ServicesGranted, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.ServicesGranted, Integer.class);

        fixedFields.put(ConstantsUc.UcMessageFields.UnusedBlock1, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.UnusedBlock1, null);
        fixedFieldsLength.put(ConstantsUc.UcMessageFields.UnusedBlock1, 12);

        fixedFields.put(ConstantsUc.UcMessageFields.UcOnline, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.UcOnline, Short.class);
    }

    @Override
    public ByteBuffer serialize() {
        throw new UnsupportedOperationException();
    }

}
