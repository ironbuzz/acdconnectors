package com.nice.saas.wfo.acd.cisco.uc.common;

import com.nice.saas.wfo.acd.cisco.common.Constants;
import com.nice.saas.wfo.acd.cisco.interfaces.GenericAcdEventTranslator;
import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import com.nice.saas.wfo.acd.cisco.uc.messages.*;
import com.nice.saas.wfo.acd.rta.GenericACDEvent;
import com.nice.saas.wfo.comp.manager.asc.generic.GeneralError;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import org.springframework.stereotype.Component;

import java.time.Instant;

/**
 * ACD - UC translator (translates ACD message to GenericAcdEvent)
 */
public abstract class AcdEventUcTranslator implements GenericAcdEventTranslator {
    private final String className = "AcdEventUcTranslator";

    private GenericLogger logger;

    /**
     * Translate UC message to GenericAcdEvent
     * @param message
     * @return
     */
    @Override
    public GenericACDEvent translate(Message message) {
        final String functionName = "translate()";
        GenericACDEvent event = new GenericACDEvent();
        try {
            if (!(message instanceof AgentStateChangeUcMessage)) {
                return null;
            }
            AgentStateChangeUcMessage agentStateChangeUcMessage = (AgentStateChangeUcMessage) message;

            event.setEventCode(agentStateChangeUcMessage.fixedFields.get(ConstantsUc.UcMessageFields.AgentState).toString());
            event.setLoginId(agentStateChangeUcMessage.floatingFields.get(ConstantsUc.UcMessageFields.AgentId).toString());
            event.setReasonCode(agentStateChangeUcMessage.fixedFields.get(ConstantsUc.UcMessageFields.EventReasonCode).toString());
            event.setEventUtcTime(Instant.now());
        } catch (Exception e) {
            throw new GeneralError(String.format("%s.%s: Exception: ", className, functionName), e);
        }
        return event;
    }

    @Override
    public void addLogger(GenericLogger logger) {
        this.logger = logger;
    }
}
