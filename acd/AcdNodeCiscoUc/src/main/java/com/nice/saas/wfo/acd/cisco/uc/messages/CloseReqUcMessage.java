package com.nice.saas.wfo.acd.cisco.uc.messages;


import com.nice.saas.wfo.acd.cisco.common.InvokeIdGenerator;
import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;

import java.nio.ByteBuffer;


/**
 * CloseReqUcMessage - request to UC server (is sent by client indicating the need to close the communication)
 */
public class CloseReqUcMessage extends UcMessage {

    public CloseReqUcMessage() {
        messageTypeId = ConstantsUc.UcMessageTypeIds.get(CloseReqUcMessage.class);

        fixedFields.put(ConstantsUc.UcMessageFields.InvokeId, InvokeIdGenerator.generateInvokeId());
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.InvokeId, Integer.class);

        fixedFields.put(ConstantsUc.UcMessageFields.Status, 0); // the reason to close the communication (we use 0 only)
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.Status, Integer.class);
    }

    /**
     * Deserialize is not supported
     * @param byteBuffer
     * @return
     */
    @Override
    public Message deserialize(ByteBuffer byteBuffer) {
        throw new UnsupportedOperationException();
    }
}
