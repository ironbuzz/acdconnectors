package com.nice.saas.wfo.acd.cisco.uc.common;

import com.nice.saas.wfo.acd.cisco.uc.messages.*;


import java.util.HashMap;
import java.util.Map;

public class ConstantsUc {

    /**
     * Default UC protocol version number (can be overriden in bean configuration)
     */
    public static final int DefaultUcVersionNumber = 19;
    /**
     * Defaul UC idle timeout (can be overriden in bean configuration)
     */
    public static final int DefaultUcIdleTimeout = 60; //sec
    /**
     * Default services requested (can be overriden in bean configuration)
     */
    public static final int DefaultServicesRequested = 0x00000010;
    /**
     * Default agent state mask (can be overriden in bean configuration)
     */
    public static final int DefaultAgentStateMask = 0x00000001|0x00000002|0x00000004|0x00000008|0x00000010|0x00000020|0x00000040|0x00000080|0x00000100|0x00000200; //1023
    public static final int DefaultPeripheralId = 0xFFFFFFFF;

    /**
     * Field names of fields that are in use in UC protocol
     */
    public enum UcMessageFields {
        UnusedBlock1 ("unusedBlock1"),
        UnusedBlock2 ("unusedBlock2"),
        Reserved ("reserved"),
        AgentId ("agentId"),
        EventReasonCode ("eventEeasonCode"),
        AgentState ("agentState"),
        InvokeId ("invokeId"),
        VersionNumber ("versionNumber"),
        IdleTimeout ("idleTimeout"),
        ServicesRequested ("servicesRequested"),
        CallMsgMask ("callMsgMask"),
        AgentStateMask ("agentStateMask"),
        ConfigMsgMask ("configMsgMask"),
        ClientId ("clientId"),
        ClientPassword ("clientPassword"),
        ServicesGranted ("servicesGranted"),
        UcOnline ("UcOnline"),
        Status ("status"),
        Text ("text"),
        Time ("time"),
        SystemEventId("systemEventId"),
        SystemEventArg1("systemEventArg1"),
        SystemEventArg2("systemEventArg2"),
        SystemEventArg3("systemEventArg3"),
        EventDeviceType("eventDeviceType");

        private String value;

        UcMessageFields(String value) {
            this.value = value;
        }
    }

    /**
     * UC message type -> UC message id map
     */
    public static final Map<Class<?>, Integer> UcMessageTypeIds = new HashMap<Class<?>, Integer>()
    {{
        put(AgentStateChangeUcMessage.class, 30);
        put(SystemEventUcMessage.class, 31);
        put(OpenReqUcMessage.class, 3);
        put(OpenConfUcMessage.class, 4);
        put(HeartbeatReqUcMessage.class, 5);
        put(HeartbeatConfUcMessage.class, 6);
        put(CloseReqUcMessage.class, 7);
        put(CloseConfUcMessage.class, 8);
        put(FailureConfUcMessage.class, 1);
        put(FailureEventUcMessage.class, 2);
    }};

    /**
     * UC message id -> UC message type map
     */
    public static final Map<Integer, Class<?>> UcMessageTypes = new HashMap<Integer, Class<?>>() {{
        //put(ConstantsUc.UcMessageTypeIds.get(AgentStateChangeUcMessage.class), AgentStateChangeUcMessage.class);
        put(ConstantsUc.UcMessageTypeIds.get(SystemEventUcMessage.class), SystemEventUcMessage.class);
        put(ConstantsUc.UcMessageTypeIds.get(OpenReqUcMessage.class), OpenReqUcMessage.class);
        put(ConstantsUc.UcMessageTypeIds.get(OpenConfUcMessage.class), OpenConfUcMessage.class);
        put(ConstantsUc.UcMessageTypeIds.get(HeartbeatReqUcMessage.class), HeartbeatReqUcMessage.class);
        put(ConstantsUc.UcMessageTypeIds.get(HeartbeatConfUcMessage.class), HeartbeatConfUcMessage.class);
        put(ConstantsUc.UcMessageTypeIds.get(CloseReqUcMessage.class), CloseReqUcMessage.class);
        put(ConstantsUc.UcMessageTypeIds.get(CloseConfUcMessage.class), CloseConfUcMessage.class);
        put(ConstantsUc.UcMessageTypeIds.get(FailureConfUcMessage.class), FailureConfUcMessage.class);
        put(ConstantsUc.UcMessageTypeIds.get(FailureEventUcMessage.class), FailureEventUcMessage.class);
    }};


    /**
     * UC floating field id -> field map
     */
    public static final Map<Byte, UcMessageFields> UcFloatingFieldsTypeIds = new HashMap<Byte, UcMessageFields>()
    {{
        put((byte) 1, UcMessageFields.ClientId);
        put((byte) 2, UcMessageFields.ClientPassword);
        put((byte) 7, UcMessageFields.Text);
    }};

    /**
     * UC floating fields field -> field id map
     */
    public static final Map<UcMessageFields, Byte> UcFloatingFieldsNames = new HashMap<UcMessageFields, Byte>()
    {{
        put(UcMessageFields.ClientId, (byte) 1);
        put(UcMessageFields.ClientPassword, (byte) 2);
        put(UcMessageFields.Text, (byte) 7);
    }};

}
