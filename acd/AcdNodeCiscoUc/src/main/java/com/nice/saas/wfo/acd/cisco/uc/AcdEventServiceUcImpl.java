package com.nice.saas.wfo.acd.cisco.uc;

import com.nice.saas.wfo.acd.cisco.AcdEventServiceBase;
import com.nice.saas.wfo.acd.cisco.interfaces.GenericAcdEventTranslator;
import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import com.nice.saas.wfo.acd.cisco.interfaces.MessageParser;
import com.nice.saas.wfo.acd.cisco.interfaces.Receiver;
import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;
import com.nice.saas.wfo.acd.cisco.uc.messages.*;
import com.nice.saas.wfo.comp.manager.asc.generic.GeneralError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.PostConstruct;
import java.nio.ByteBuffer;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * UC event service implementation
 */
public abstract class AcdEventServiceUcImpl extends AcdEventServiceBase {

    // region private fields
    private final String className = "AcdEventServiceUcImpl";

    private AtomicBoolean gotUcOpenConfMessage = new AtomicBoolean(false);
    private AtomicInteger heartbeatLastSentId = new AtomicInteger(0);
    private AtomicInteger heartbeatLastReceivedId = new AtomicInteger(0);
    private AtomicInteger ucErrorEventsCount = new AtomicInteger(0);
    private int ucIdleTimeout;

    // endregion private fields

    // region autowired fields

    /**
     * UC receiver setter
     * @param receiver
     */
    @Autowired
    @Qualifier("UcReceiver")
    private void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    /**
     * UC message parser setter
     * @param messageParser
     */
    @Autowired
    @Qualifier("UcMessageParser")
    private void setMessageParser (MessageParser messageParser) {
        this.messageParser = messageParser;
    }


    /**
     * UC message - to AcdEvent translator setter
     * @param acdMessageTranslator
     */
    @Autowired
    @Qualifier("AcdEventUcTranslator")
    private void setAcdMessageTranslator (GenericAcdEventTranslator acdMessageTranslator) {
        this.acdMessageTranslator = acdMessageTranslator;
    }


    @Autowired
    private OpenReqUcMessage openReqUcMessage;

    // endregion autowired fields

    // region post construct


    @PostConstruct
    public void init() {
        super.init();
        ucIdleTimeout = openReqUcMessage.getUcIdleTimeout();
    }

    // endregion post construct

    // region base class overrides

    /**
     * UC sender pool implementation
     */
    @Override
    protected void senderPoolCore() {
        final String functionName = "senderPoolCore()";
        try {
            // send OpenConf message ti initialize UC communications
            ucErrorEventsCount.set(0);
            initUcCommunication();
            if (ucErrorEventsCount.get() > 20) {
                return;
            }
            // start to send heartbeat messages (if haven't received confirmations for 10 heartbeat messages, re-initialize UC communications
            keepAliveLoop();

        } catch (Exception e) {
            logger.error(String.format("%s.%s: Error: ", className, functionName), e);
        }
    }

    /**
     * UC events processor pool implementation
     * @param message
     */
    @Override
    protected void eventsProcessorPoolCore(Message message)  {
        final String functionName = "eventsProcessorPoolCore()";
        // check if uc sequential error messages count is greater than 20 - if yes, stop the application
        if (ucErrorEventsCount.get() > 20) {
            String errorMessage = String.format("%s.%s: UC error messages count reached 20", className, functionName);
            logger.warning(errorMessage);
            acdErrorHandler.generalError(new GeneralError(errorMessage));
        }
        // got some message different from ACD event, process it
        if (message instanceof FailureEventUcMessage) {
            FailureEventUcMessage failureEventUcMessage = (FailureEventUcMessage) message;
            logger.warning(String.format("%s.%s: FailureEventUcMessage received. Status: %s , Text: %s",
                    className, functionName, failureEventUcMessage.fixedFields.get(ConstantsUc.UcMessageFields.Status),
                    failureEventUcMessage.floatingFields.get(ConstantsUc.UcMessageFields.Text)));
            ucErrorEventsCount.getAndIncrement();
        } else if (message instanceof FailureConfUcMessage) {
            FailureConfUcMessage failureEventUcMessage = (FailureConfUcMessage) message;
            logger.warning(String.format("%s.%s: FailureConfUcMessage received. InvokeId: %s, Status: %s , Text: %s",
                    className, functionName, failureEventUcMessage.fixedFields.get(ConstantsUc.UcMessageFields.InvokeId),
                    failureEventUcMessage.fixedFields.get(ConstantsUc.UcMessageFields.Status),
                    failureEventUcMessage.floatingFields.get(ConstantsUc.UcMessageFields.Text)));
            ucErrorEventsCount.getAndIncrement();
        } else if (message instanceof OpenConfUcMessage) {
            OpenConfUcMessage openConfUcMessage = (OpenConfUcMessage) message;
            gotUcOpenConfMessage.set(true);
            logger.info(String.format("%s.%s: OpenConfUcMessage received. InvokeId: %s, Services granted: %s , UC online: %s",
                    className, functionName, openConfUcMessage.fixedFields.get(ConstantsUc.UcMessageFields.InvokeId),
                    openConfUcMessage.fixedFields.get(ConstantsUc.UcMessageFields.ServicesGranted),
                    openConfUcMessage.fixedFields.get(ConstantsUc.UcMessageFields.UcOnline)));
            ucErrorEventsCount.set(0);
        } else if (message instanceof HeartbeatConfUcMessage) {
            HeartbeatConfUcMessage heartbeatConfUcMessage = (HeartbeatConfUcMessage) message;
            heartbeatLastReceivedId.set((int) heartbeatConfUcMessage.fixedFields.get(ConstantsUc.UcMessageFields.InvokeId));
            logger.info(String.format("%s.%s: HeartbeatConfUcMessage received. InvokeId: %s",
                    className, functionName, heartbeatConfUcMessage.fixedFields.get(ConstantsUc.UcMessageFields.InvokeId)));
            ucErrorEventsCount.set(0);
        } else if (message instanceof CloseConfUcMessage) {
            CloseConfUcMessage closeConfUcMessage = (CloseConfUcMessage) message;
            logger.info(String.format("%s.%s: CloseConfUcMessage received. InvokeId: %s",
                    className, functionName, closeConfUcMessage.fixedFields.get(ConstantsUc.UcMessageFields.InvokeId)));
            ucErrorEventsCount.set(0);
        } else if (message instanceof  SystemEventUcMessage) {
            SystemEventUcMessage systemEventUcMessage = (SystemEventUcMessage) message;
            logger.info(String.format("%s.%s: SystemEventUcMessage received. Status: %s, SystemEventId: %s, SystemEventArg1: %s, SystemEventArg2: %s, SystemEventArg3: %s, EventDeviceType: %s, Text: %s",
                    className, functionName,
                    systemEventUcMessage.fixedFields.get(ConstantsUc.UcMessageFields.Status),
                    systemEventUcMessage.fixedFields.get(ConstantsUc.UcMessageFields.SystemEventId),
                    systemEventUcMessage.fixedFields.get(ConstantsUc.UcMessageFields.SystemEventArg1),
                    systemEventUcMessage.fixedFields.get(ConstantsUc.UcMessageFields.SystemEventArg2),
                    systemEventUcMessage.fixedFields.get(ConstantsUc.UcMessageFields.SystemEventArg3),
                    systemEventUcMessage.fixedFields.get(ConstantsUc.UcMessageFields.EventDeviceType),
                    systemEventUcMessage.floatingFields.get(ConstantsUc.UcMessageFields.Text)));
        }
    }


    // endregion base class overrides

    // region private methods

    /**
     * Initialize UC communication by sending OpenReq message to server
     * @throws ExecutionException
     * @throws InterruptedException
     */
    private void initUcCommunication() throws ExecutionException, InterruptedException {
        final String functionName = "initUcCommunication()";

        while (!gotUcOpenConfMessage.get() && (!stopped.get())) {
            try {
                if (ucErrorEventsCount.get() > 20) {
                    String errorMessage = String.format("%s.%s: UC errors count reached 20, stopping the sending loop", className, functionName);
                    logger.warning(errorMessage);
                    break;
                }
                ByteBuffer openReqUcMessageBuffer = openReqUcMessage.serialize();

                logger.info(String.format("%s.%s: Sending OpenReq message", className, functionName));
                receiver.send(openReqUcMessageBuffer);


                final Future<?> handle = timer.schedule(() -> {
                    if (!gotUcOpenConfMessage.get()) {
                        logger.warning(String.format("%s.%s: Haven't received UcOpenConfMessage during %s s, resending", className, functionName, ucIdleTimeout));
                    }
                }, ucIdleTimeout /4, TimeUnit.SECONDS);
                try {
                    handle.get(ucIdleTimeout /4, TimeUnit.SECONDS);
                }
                catch (TimeoutException te) {}
            }
            catch (Exception e) {
                logger.error(String.format("%s.%s: Exception: ", className, functionName), e);
                ucErrorEventsCount.getAndIncrement();
            }
        }
    }


    /**
     * Keep alive loop (sending HeartbeatReq message)
     * @throws ExecutionException
     * @throws InterruptedException
     */
    private void keepAliveLoop() throws ExecutionException, InterruptedException {
        final String functionName = "keepAliveLoop()";

        while (heartbeatLastSentId.get() - heartbeatLastReceivedId.get() < 10 && (!stopped.get())) {
            try {
                if (ucErrorEventsCount.get() > 20) {
                    String errorMessage = String.format("%s.%s: UC errors count reached 20, stopping the sending loop", className, functionName);
                    logger.warning(errorMessage);
                    break;
                }
                // if last send id not equal to last received, send open req message
                if (heartbeatLastSentId.get() != heartbeatLastReceivedId.get()) {

                    ByteBuffer openReqUcMessageBuffer = openReqUcMessage.serialize();

                    logger.info(String.format("%s.%s: Sending OpenReq message", className, functionName));
                    receiver.send(openReqUcMessageBuffer);
                }

                UcMessage heartbeatReqUcMessage = new HeartbeatReqUcMessage();
                ByteBuffer heartbeatReqUcMessageBuffer = heartbeatReqUcMessage.serialize();

                logger.info(String.format("%s.%s: Sending HeatbeatReq message", className, functionName));
                receiver.send(heartbeatReqUcMessageBuffer);
                heartbeatLastSentId.set((int)heartbeatReqUcMessage.fixedFields.get(ConstantsUc.UcMessageFields.InvokeId));

                final Future<?> handle = timer.schedule(() -> {

                }, ucIdleTimeout / 4, TimeUnit.SECONDS);
                try {
                    handle.get(ucIdleTimeout / 4, TimeUnit.SECONDS);
                } catch (TimeoutException te) {}
            } catch (Exception e) {
                logger.error(String.format("%s.%s: Exception: ", className, functionName), e);
                ucErrorEventsCount.getAndIncrement();
            }
        }
        // if while loop exited (it means that 10 sequential confirmations were not received from UC server)
        // set gotUcConfMessage to false (to re-start initUcCommunication loop)
        gotUcOpenConfMessage.set(false);
    }

    /**
     * Send request to close communication with UC server (on destroy)
     */
    private void closeUcCommunication() {
        final String functionName = "closeUcCommunication()";
        CloseReqUcMessage closeReqUcMessage = new CloseReqUcMessage();
        ByteBuffer closeReqUcMessageBuffer = closeReqUcMessage.serialize();
        if (logger != null) {
            logger.info(String.format("%s.%s: Sending CloseReq message", className, functionName));
        }
        if (receiver != null) {
            receiver.send(closeReqUcMessageBuffer);
        }
    }

    // endregion

}
