package com.nice.saas.wfo.acd.cisco.uc.messages;

import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;

import java.nio.ByteBuffer;

/**
 * FailureConfUcMessage - sent by UC server when error occured while processing request from client
 */
public class FailureConfUcMessage extends UcMessage {

    public FailureConfUcMessage() {
        messageTypeId = ConstantsUc.UcMessageTypeIds.get(FailureConfUcMessage.class);

        fixedFields.put(ConstantsUc.UcMessageFields.InvokeId, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.InvokeId, Integer.class);

        fixedFields.put(ConstantsUc.UcMessageFields.Status, null);
        fixedFieldsTypes.put(ConstantsUc.UcMessageFields.Status, Integer.class);

        floatingFields.put(ConstantsUc.UcMessageFields.Text, null);
        floatingFieldsTypes.put(ConstantsUc.UcMessageFields.Text, String.class);
    }

    /**
     * Serialize is not supported
     * @return
     */
    @Override
    public ByteBuffer serialize() {
        throw new UnsupportedOperationException();
    }

}
