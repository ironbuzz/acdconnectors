package com.nice.saas.wfo.acd.cisco.uc.common;

import com.nice.saas.wfo.acd.cisco.common.Constants;
import com.nice.saas.wfo.acd.cisco.uc.messages.AgentStateChangeUcMessage;
import com.nice.saas.wfo.acd.rta.GenericACDEvent;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class AcdEventUcTranslatorTest {
    class AcdEventUcTranslator1  extends AcdEventUcTranslator {

    }
    GenericLogger logger = new GenericLogger() {
        @Override
        public void info(String s) {

        }

        @Override
        public void error(String s, Throwable throwable, Object... objects) {

        }

        @Override
        public void warning(String s) {

        }
    };

    @Test
    public void  translate() {
        AgentStateChangeUcMessage agentStateChangeUccxMessage = new AgentStateChangeUcMessage(){};
        agentStateChangeUccxMessage.floatingFields.put(ConstantsUc.UcMessageFields.AgentId, "1");
        agentStateChangeUccxMessage.fixedFields.put(ConstantsUc.UcMessageFields.EventReasonCode, "2");
        agentStateChangeUccxMessage.fixedFields.put(ConstantsUc.UcMessageFields.AgentState, "3");
        AcdEventUcTranslator1 t= new AcdEventUcTranslator1();
        GenericACDEvent event =  t.translate(agentStateChangeUccxMessage);
       // assertEquals(event.getAcdId(), Constants.AcdCiscoUccxReportType );
        assertEquals(event.getReasonCode(), "2" );
        assertEquals(event.getEventCode(), "3" );
        assertEquals(event.getLoginId(), "1" );
    }

    @Test
    public void addLogger() throws NoSuchFieldException, IllegalAccessException {
        AcdEventUcTranslator1 t= new AcdEventUcTranslator1();
        t.addLogger(logger);
        Field field = t.getClass().getSuperclass().getDeclaredField("logger");
        field.setAccessible(true);
        assertNotNull(field.get(t));
    }


}