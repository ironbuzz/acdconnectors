package com.nice.saas.wfo.acd.cisco.uc.messages;

import com.nice.saas.wfo.acd.cisco.common.InvokeIdGenerator;
import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;

import static org.junit.Assert.*;

public class CloseConfUcMessageTest {
    CloseConfUcMessage message;
    @Before
    public void  setUp() {
        message = new CloseConfUcMessage();
        InvokeIdGenerator.reset();
    }

    @After
    public void  tearDown() {
    }

    @Test(expected = UnsupportedOperationException.class)
    public void  serialize() {
        ByteBuffer buffer = message.serialize();
    }

    @Test
    public void  getMessageTypeId() {
        assertNotNull(message);
        assertEquals(message.messageTypeId, 8);
        assertEquals(message.getMessageTypeId(), 8);
        assertEquals(message.fixedFields.size(), 1);
        assertEquals(message.fixedFieldsTypes.size(), 1);
        assertNull(message.fixedFields.get(ConstantsUc.UcMessageFields.InvokeId));
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.InvokeId));
        Class<?> fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.InvokeId);
        assertEquals(fieldType, Integer.class);
    }

    @Test
    public void  deserialize() {
        ByteBuffer buffer = ByteBuffer.allocateDirect(4);

        buffer.putInt(3);
        buffer.rewind();
        message = (CloseConfUcMessage)message.deserialize(buffer);
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.InvokeId), 3);
    }


}