package com.nice.saas.wfo.acd.cisco.uc.messages;

import com.nice.saas.wfo.acd.cisco.common.InvokeIdGenerator;
import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import static org.junit.Assert.*;

public class SystemEventUcMessageTest {
    SystemEventUcMessage message;
    @Before
    public void  setUp() {
        message = new SystemEventUcMessage();
        InvokeIdGenerator.reset();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void  serialize() {
        ByteBuffer buffer = message.serialize();
    }

    @Test
    public void  getMessageTypeId() {
        assertNotNull(message);
        assertEquals(message.messageTypeId, 31);
        assertEquals(message.getMessageTypeId(), 31);
        assertEquals(message.fixedFields.size(), 7);
        assertEquals(message.fixedFieldsTypes.size(), 7);
        assertEquals(message.floatingFields.size(), 1);
        assertEquals(message.floatingFieldsTypes.size(), 1);

        assertNull(message.fixedFields.get(ConstantsUc.UcMessageFields.Status));
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.Status));
        Class<?> fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.Status);
        assertEquals(fieldType, Integer.class);

        assertNull(message.fixedFields.get(ConstantsUc.UcMessageFields.Time));
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.Time));
        fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.Time);
        assertEquals(fieldType, Integer.class);

        assertNull(message.fixedFields.get(ConstantsUc.UcMessageFields.SystemEventId));
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.SystemEventId));
        fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.SystemEventId);
        assertEquals(fieldType, Integer.class);

        assertNull(message.fixedFields.get(ConstantsUc.UcMessageFields.SystemEventArg1));
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.SystemEventArg1));
        fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.SystemEventArg1);
        assertEquals(fieldType, Integer.class);

        assertNull(message.fixedFields.get(ConstantsUc.UcMessageFields.SystemEventArg2));
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.SystemEventArg2));
        fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.SystemEventArg2);
        assertEquals(fieldType, Integer.class);

        assertNull(message.fixedFields.get(ConstantsUc.UcMessageFields.SystemEventArg3));
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.SystemEventArg3));
        fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.SystemEventArg3);
        assertEquals(fieldType, Integer.class);

        assertNull(message.fixedFields.get(ConstantsUc.UcMessageFields.EventDeviceType));
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.EventDeviceType));
        fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.EventDeviceType);
        assertEquals(fieldType, Short.class);


        assertNull(message.floatingFields.get(ConstantsUc.UcMessageFields.Text));
        assertNotNull(message.floatingFieldsTypes.get(ConstantsUc.UcMessageFields.Text));
        fieldType = message.floatingFieldsTypes.get(ConstantsUc.UcMessageFields.Text);
        assertEquals(fieldType, String.class);
    }

    @Test
    public void  deserialize() {
        ByteBuffer buffer = ByteBuffer.allocateDirect(33);

        buffer.putInt(1);
        buffer.putInt(2);
        buffer.putInt(3);
        buffer.putInt(4);
        buffer.putInt(5);
        buffer.putInt(6);
        buffer.putShort((short)7);

        buffer.put((byte) 7);
        buffer.put((byte) 5);
        String str = "Vasya";
        byte[] tempArray = str.getBytes(Charset.forName("UTF-8"));
        buffer.put(tempArray);
        buffer.rewind();
        message = (SystemEventUcMessage) message.deserialize(buffer);

        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.Status), 1);
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.Time), 2);
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.SystemEventId), 3);
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.SystemEventArg1), 4);
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.SystemEventArg2), 5);
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.SystemEventArg3), 6);
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.EventDeviceType), (short)7);
        assertEquals(message.floatingFields.get(ConstantsUc.UcMessageFields.Text), "Vasya");
    }
}