package com.nice.saas.wfo.acd.cisco.uc.common;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static org.junit.Assert.assertEquals;

public class UcReceiverTest {
    UcReceiver receiver;
    Field buffersField;
    BlockingQueue<ByteBuffer> buffers;
    String[] strArr;
    List<ByteBuffer> bufferList;
    List<byte[]> underlyingByteArrays = new ArrayList<>();
    @Before
    public void  setUp() throws NoSuchFieldException {
        receiver = new UcReceiver();
        buffersField = receiver.getClass().getSuperclass().getDeclaredField("buffers");
        buffersField.setAccessible(true);
        buffers = new ArrayBlockingQueue<>(100);
        bufferList =  new ArrayList<>();
    }

    @After
    public void  tearDown() throws IllegalAccessException {

        buffers.addAll(bufferList);
        buffersField.set(receiver, buffers);
        List<ByteBuffer> results = new ArrayList<>();
        while (buffers.size() > 0) {

            results.addAll(receiver.receive());
        }
        List<byte[]> resList = new ArrayList<>();
        for (ByteBuffer res : results) {
            resList.add(res.array());
        }

        assertEquals(resList.size(), underlyingByteArrays.size());
        for (int i=0; i< resList.size(); i++) {
            for (int j=0; j< resList.get(i).length; j++) {
                assertEquals(resList.get(i)[j], underlyingByteArrays.get(i)[j]);
            }
        }
    }

    @Test
    public void  receive1() throws NoSuchFieldException, IllegalAccessException {

        ByteBuffer buffer = ByteBuffer.allocateDirect(14);
        buffer.putInt(6);
        buffer.putInt(1);
        byte[] arr = new byte[]{(byte)0, (byte) 1,  (byte) 2, (byte) 3, (byte) 4, (byte) 5};
        buffer.put(arr);

        buffer.rewind();
        byte[] underlyingArr=new byte[buffer.remaining()];
        buffer.get(underlyingArr, 0, buffer.remaining());
        buffer.rewind();
        underlyingByteArrays.add(underlyingArr);
        bufferList.add(buffer);

    }

    @Test
    public void  receive2() throws NoSuchFieldException, IllegalAccessException {

        ByteBuffer buffer = ByteBuffer.allocateDirect(14);
        buffer.putInt(6);
        buffer.putInt(1);
        byte[] arr = new byte[]{(byte)0, (byte) 1,  (byte) 2, (byte) 3, (byte) 4, (byte) 5};
        buffer.put(arr);
        buffer.rewind();
        byte[] underlyingArr=new byte[buffer.remaining()];
        buffer.get(underlyingArr, 0, buffer.remaining());
        buffer.rewind();
        underlyingByteArrays.add(underlyingArr);
        bufferList.add(buffer);

        buffer = ByteBuffer.allocateDirect(16);
        buffer.putInt(8);
        buffer.putInt(1);
        arr = new byte[]{(byte)0, (byte) 1,  (byte) 2, (byte) 3, (byte) 4, (byte) 5, (byte) 6, (byte)7};
        buffer.put(arr);
        buffer.rewind();
        underlyingArr=new byte[buffer.remaining()];
        buffer.get(underlyingArr, 0, buffer.remaining());
        buffer.rewind();
        underlyingByteArrays.add(underlyingArr);
        bufferList.add(buffer);

    }

    @Test
    public void  receive3() throws NoSuchFieldException, IllegalAccessException {

        ByteBuffer buffer = ByteBuffer.allocateDirect(30);
        buffer.putInt(6);
        buffer.putInt(1);
        byte[] arr = new byte[]{(byte)0, (byte) 1,  (byte) 2, (byte) 3, (byte) 4, (byte) 5};
        buffer.put(arr);
        buffer.rewind();
        byte[] underlyingArr=new byte[14];
        buffer.get(underlyingArr, 0, 14);
        buffer.rewind();
        underlyingByteArrays.add(underlyingArr);
        buffer.position(14);


        buffer.putInt(8);
        buffer.putInt(1);
        arr = new byte[]{(byte)0, (byte) 1,  (byte) 2, (byte) 3, (byte) 4, (byte) 5, (byte) 6, (byte)7};
        buffer.put(arr);
        buffer.position(14);
        underlyingArr=new byte[16];
        buffer.get(underlyingArr, 0, 16);
        buffer.rewind();
        underlyingByteArrays.add(underlyingArr);
        bufferList.add(buffer);

    }


    @Test
    public void  receive4() throws NoSuchFieldException, IllegalAccessException {

        underlyingByteArrays.add(new byte[]{(byte) 0,(byte) 0,(byte) 0,(byte) 6, (byte) 0,(byte) 0,(byte) 0,(byte) 1, (byte)0, (byte) 1,  (byte) 2, (byte) 3, (byte) 4, (byte) 5});
        underlyingByteArrays.add(new byte[]{(byte) 0,(byte) 0,(byte) 0,(byte) 8, (byte) 0,(byte) 0,(byte) 0,(byte) 1, (byte)0, (byte) 1,  (byte) 2, (byte) 3, (byte) 4, (byte) 5, (byte) 6, (byte)7});


        ByteBuffer buffer = ByteBuffer.allocateDirect(12);
        buffer.putInt(6);
        buffer.putInt(1);
        byte[] arr = new byte[]{(byte)0, (byte) 1,  (byte) 2, (byte) 3};
        buffer.put(arr);
        buffer.rewind();
        bufferList.add(buffer);

        buffer = ByteBuffer.allocateDirect(2);
        arr = new byte[]{(byte)4, (byte) 5, };
        buffer.put(arr);
        buffer.rewind();
        bufferList.add(buffer);

        buffer = ByteBuffer.allocateDirect(9);
        buffer.putInt(8);
        buffer.putInt(1);
        arr = new byte[]{(byte)0 };
        buffer.put(arr);
        buffer.rewind();
        bufferList.add(buffer);

        buffer = ByteBuffer.allocateDirect(7);

        arr = new byte[]{(byte) 1,  (byte) 2, (byte) 3, (byte) 4, (byte) 5, (byte) 6, (byte)7 };
        buffer.put(arr);
        buffer.rewind();
        bufferList.add(buffer);

    }

    @Test
    public void  receive5() throws NoSuchFieldException, IllegalAccessException {

        underlyingByteArrays.add(new byte[]{(byte) 0,(byte) 0,(byte) 0,(byte) 6, (byte) 0,(byte) 0,(byte) 0,(byte) 1, (byte)0, (byte) 1,  (byte) 2, (byte) 3, (byte) 4, (byte) 5});
        underlyingByteArrays.add(new byte[]{(byte) 0,(byte) 0,(byte) 0,(byte) 8, (byte) 0,(byte) 0,(byte) 0,(byte) 1, (byte)0, (byte) 1,  (byte) 2, (byte) 3, (byte) 4, (byte) 5, (byte) 6, (byte)7});

        ByteBuffer buffer = ByteBuffer.allocateDirect(2);

        byte[] arr = new byte[]{(byte)0, (byte) 0};
        buffer.put(arr);
        buffer.rewind();
        bufferList.add(buffer);

        buffer = ByteBuffer.allocateDirect(1);
        arr = new byte[]{(byte)0};
        buffer.put(arr);
        buffer.rewind();
        bufferList.add(buffer);

        buffer = ByteBuffer.allocateDirect(1);
        arr = new byte[]{(byte)6};
        buffer.put(arr);
        buffer.rewind();
        bufferList.add(buffer);

        buffer = ByteBuffer.allocateDirect(8);
        buffer.putInt(1);
        arr = new byte[]{(byte)0, (byte) 1,  (byte) 2, (byte) 3};
        buffer.put(arr);
        buffer.rewind();
        bufferList.add(buffer);

        buffer = ByteBuffer.allocateDirect(6);
        arr = new byte[]{(byte)4, (byte) 5, };
        buffer.put(arr);
        buffer.putInt(8);
        buffer.rewind();
        bufferList.add(buffer);

        buffer = ByteBuffer.allocateDirect(5);

        buffer.putInt(1);
        arr = new byte[]{(byte)0 };
        buffer.put(arr);
        buffer.rewind();
        bufferList.add(buffer);

        buffer = ByteBuffer.allocateDirect(7);

        arr = new byte[]{(byte) 1,  (byte) 2, (byte) 3, (byte) 4, (byte) 5, (byte) 6, (byte)7 };
        buffer.put(arr);
        buffer.rewind();
        bufferList.add(buffer);

    }


}