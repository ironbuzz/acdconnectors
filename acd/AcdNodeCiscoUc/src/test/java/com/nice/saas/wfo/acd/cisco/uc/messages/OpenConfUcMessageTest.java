package com.nice.saas.wfo.acd.cisco.uc.messages;

import com.nice.saas.wfo.acd.cisco.common.InvokeIdGenerator;
import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;

import static org.junit.Assert.*;

public class OpenConfUcMessageTest {
    OpenConfUcMessage message;
    @Before
    public void  setUp() {
        message = new OpenConfUcMessage();
        InvokeIdGenerator.reset();
    }

    @After
    public void  tearDown() {
    }

    @Test(expected = UnsupportedOperationException.class)
    public void  serialize() {
        ByteBuffer buffer = message.serialize();
    }

    @Test
    public void  getMessageTypeId() {
        assertNotNull(message);
        assertEquals(message.messageTypeId, 4);
        assertEquals(message.getMessageTypeId(), 4);
        assertEquals(message.fixedFields.size(), 4);
        assertEquals(message.fixedFieldsTypes.size(), 4);
        assertNull(message.fixedFields.get(ConstantsUc.UcMessageFields.InvokeId));
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.InvokeId));
        Class<?> fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.InvokeId);
        assertEquals(fieldType, Integer.class);

        assertNull(message.fixedFields.get(ConstantsUc.UcMessageFields.ServicesGranted));
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.ServicesGranted));
        fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.ServicesGranted);
        assertEquals(fieldType, Integer.class);

        assertNull(message.fixedFields.get(ConstantsUc.UcMessageFields.UcOnline));
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.UcOnline));
        fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.UcOnline);
        assertEquals(fieldType, Short.class);
    }

    @Test
    public void  deserialize() {
        ByteBuffer buffer = ByteBuffer.allocateDirect(22);

        buffer.putInt(3);
        buffer.putInt(4);
        buffer.put(new byte[12]);
        buffer.putShort((short)5);

        buffer.rewind();
        message = (OpenConfUcMessage) message.deserialize(buffer);
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.InvokeId), 3);
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.ServicesGranted), 4);
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.UcOnline), (short)5);
    }

}