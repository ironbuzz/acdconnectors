package com.nice.saas.wfo.acd.cisco.uc.messages;

import com.nice.saas.wfo.acd.cisco.common.InvokeIdGenerator;
import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class HeartbeatReqUcMessageTest {
    HeartbeatReqUcMessage message;
    @Before
    public void  setUp() {
        message = new HeartbeatReqUcMessage();
        InvokeIdGenerator.reset();
    }

    @After
    public void  tearDown() {
    }

    @Test(expected = UnsupportedOperationException.class)
    public void  deserialize() {
        ByteBuffer buffer = ByteBuffer.allocateDirect(4);

        buffer.putInt(3);
        buffer.rewind();
        message.deserialize(buffer);
    }

    @Test
    public void  getMessageTypeId() {
        assertNotNull(message);
        assertEquals(message.messageTypeId, 5);
        assertEquals(message.getMessageTypeId(), 5);
        assertEquals(message.fixedFields.size(), 1);
        assertEquals(message.fixedFieldsTypes.size(), 1);
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.InvokeId), 0);
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.InvokeId));
        Class<?> fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.InvokeId);
        assertEquals(fieldType, Integer.class);
    }

    @Test
    public void  serialize() {
        ByteBuffer buffer = message.serialize();
        assertNotNull(buffer);
        assertEquals(buffer.position(), 0);
        assertEquals(buffer.remaining(), 12);
        assertEquals(buffer.getInt() , 4);
        assertEquals(buffer.getInt() , 5);
        assertEquals(buffer.getInt() , 0);
    }

}