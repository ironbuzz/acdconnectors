package com.nice.saas.wfo.acd.cisco.uc.messages;

import com.nice.saas.wfo.acd.cisco.common.InvokeIdGenerator;
import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import static org.junit.Assert.*;

public class FailureEventUcMessageTest {
    FailureEventUcMessage message;
    @Before
    public void  setUp() {
        message = new FailureEventUcMessage();
        InvokeIdGenerator.reset();
    }

    @After
    public void  tearDown() {
    }

    @Test(expected = UnsupportedOperationException.class)
    public void  serialize() {
        ByteBuffer buffer = message.serialize();
    }

    @Test
    public void  getMessageTypeId() {
        assertNotNull(message);
        assertEquals(message.messageTypeId, 2);
        assertEquals(message.getMessageTypeId(), 2);
        assertEquals(message.fixedFields.size(), 1);
        assertEquals(message.fixedFieldsTypes.size(), 1);
        assertEquals(message.floatingFields.size(), 1);
        assertEquals(message.floatingFieldsTypes.size(), 1);

        assertNull(message.fixedFields.get(ConstantsUc.UcMessageFields.Status));
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.Status));
        Class<?> fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.Status);
        assertEquals(fieldType, Integer.class);

        assertNull(message.floatingFields.get(ConstantsUc.UcMessageFields.Text));
        assertNotNull(message.floatingFieldsTypes.get(ConstantsUc.UcMessageFields.Text));
        fieldType = message.floatingFieldsTypes.get(ConstantsUc.UcMessageFields.Text);
        assertEquals(fieldType, String.class);
    }

    @Test
    public void  deserialize() {
        ByteBuffer buffer = ByteBuffer.allocateDirect(11);

        buffer.putInt(1);

        buffer.put((byte) 7);
        buffer.put((byte) 5);
        String str = "Vasya";
        byte[] tempArray = str.getBytes(Charset.forName("UTF-8"));
        buffer.put(tempArray);
        buffer.rewind();
        message = (FailureEventUcMessage)message.deserialize(buffer);

        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.Status), 1);
        assertEquals(message.floatingFields.get(ConstantsUc.UcMessageFields.Text), "Vasya");
    }

}