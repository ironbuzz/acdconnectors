package com.nice.saas.wfo.acd.cisco.uc.messages;

import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.LinkedHashMap;

import static org.junit.Assert.*;

public class UcMessageTest {


    public class UcMessageChild extends UcMessage {

    }
    UcMessageChild testMessage;

    @Before
    public void  setUp() {
        testMessage = new UcMessageChild();
    }


    public void  tearDown() {
    }

    @Test
    public void  assertEmpty() throws NoSuchFieldException {
        assertNotNull(testMessage);
        assertNotNull(testMessage.fixedFields);
        assertTrue(testMessage.fixedFields instanceof HashMap);
        assertTrue(testMessage.fixedFields instanceof LinkedHashMap);
        assertNotNull(testMessage.fixedFieldsTypes);
        assertTrue(testMessage.fixedFieldsTypes instanceof HashMap);
        assertTrue(testMessage.fixedFieldsTypes instanceof LinkedHashMap);
        assertNotNull(testMessage.fixedFieldsLength);
        assertTrue(testMessage.fixedFieldsLength instanceof HashMap);
        assertTrue(testMessage.fixedFieldsLength instanceof LinkedHashMap);
        assertNotNull(testMessage.floatingFields);
        assertTrue(testMessage.floatingFields instanceof HashMap);
        assertTrue(testMessage.floatingFields instanceof LinkedHashMap);
        assertNotNull(testMessage.floatingFieldsTypes);
        assertTrue(testMessage.floatingFieldsTypes instanceof HashMap);
        assertTrue(testMessage.floatingFieldsTypes instanceof LinkedHashMap);
        assertNotNull(testMessage.floatingFieldsLength);
        assertTrue(testMessage.floatingFieldsLength instanceof HashMap);
        assertTrue(testMessage.floatingFieldsLength instanceof LinkedHashMap);

     }
    @Test
    public void  toStringTest() {
        UcMessage message = new UcMessage(){};
        ByteBuffer buffer = ByteBuffer.allocateDirect(65);

        buffer.put(new byte[30]);
        buffer.putShort((short)2);
        buffer.putShort((short)3);

        buffer.put(new byte[24]);
        buffer.put((byte)0xc2);
        buffer.put((byte) 5);
        String str = "Vasya";
        byte[] tempArray = str.getBytes(Charset.forName("UTF-8"));
        buffer.put(tempArray);
        buffer.rewind();
        message = (UcMessage)message.deserialize(buffer);
        assertEquals(message.toString(), "MessageTypeId = 0; BodyLength = 0; ");
    }

    @Test
    public void  getMessageTypeIdNotInitialized() {
        assertNotNull(testMessage);
        // messagerTypeId is not initialized (0)
        assertEquals(testMessage.messageTypeId, 0);
        assertEquals(testMessage.getMessageTypeId(), 0);
    }

    @Test
    public void  getMessageTypeIdInitialized() {
        assertNotNull(testMessage);
        // messagerTypeId is not initialized (0)
        testMessage.messageTypeId = 10;
        assertEquals(testMessage.messageTypeId, 10);
        assertEquals(testMessage.getMessageTypeId(), 10);
    }



    @Test
    public void  deserializeEmpty() throws NoSuchFieldException {
        testMessage = (UcMessageChild) testMessage.deserialize(null);
         assertEmpty();

    }

    @Test
    public void  serializeEmpty() {
        ByteBuffer buffer =testMessage.serialize();
         assertNotNull(buffer);
         assertTrue(buffer.isDirect());
         assertEquals(buffer.remaining(), 8);
         assertEquals(buffer.position(), 0);

    }

}