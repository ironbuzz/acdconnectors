package com.nice.saas.wfo.acd.cisco.uc;

import com.nice.saas.wfo.acd.cisco.AcdEventServiceBase;
import com.nice.saas.wfo.acd.cisco.common.TcpReceiverBase;
import com.nice.saas.wfo.acd.cisco.interfaces.GenericAcdEventTranslator;
import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import com.nice.saas.wfo.acd.cisco.interfaces.MessageParser;
import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;
import com.nice.saas.wfo.acd.cisco.uc.messages.*;
import com.nice.saas.wfo.acd.rta.GenericACDEvent;
import com.nice.saas.wfo.acd.rta.GenericACDEventListener;
import com.nice.saas.wfo.comp.manager.asc.generic.ACDErrorHandler;
import com.nice.saas.wfo.comp.manager.asc.generic.ConnectionError;
import com.nice.saas.wfo.comp.manager.asc.generic.GeneralError;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import org.junit.Test;

import java.awt.event.ActionEvent;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.*;

public class AcdEventServiceUcImplTest {
    GenericLogger logger = new GenericLogger() {
        @Override
        public void info(String s) {

        }

        @Override
        public void error(String s, Throwable throwable, Object... objects) {

        }

        @Override
        public void warning(String s) {

        }
    };
    AcdEventServiceUcImpl service = new AcdEventServiceUcImpl() {




    };
    TcpReceiverBase tb = new TcpReceiverBase() {
        private GenericLogger logger;

        @Override
        public List<ByteBuffer> receive() {
            return null;
        }

        @Override
        public void addLogger(GenericLogger logger) {
            this.logger = logger;
        }
    };
    MessageParser parser = new MessageParser() {
        private GenericLogger logger;
        @Override
        public Message parse(Object rawMessage) {
            return null;
        }

        @Override
        public void addLogger(GenericLogger logger) {
            this.logger= logger;
        }
    };
    GenericAcdEventTranslator translator = new GenericAcdEventTranslator() {
        private GenericLogger logger;
        @Override
        public GenericACDEvent translate(Message message) {
            return null;
        }

        @Override
        public void addLogger(GenericLogger logger) {
            this.logger= logger;
        }
    };
    @Test
    public void init() throws Exception {
        service.receiver = tb;

        Field field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 100);
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 100);
        field = service.getClass().getSuperclass().getDeclaredField("openReqUcMessage");
        field.setAccessible(true);
        field.set(service, new OpenReqUcMessage());
        service.init();
        assertNotNull(service.receiver);

        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("eventsProcessorPool");
        field.setAccessible(true);
        assertNotNull(field.get(service));
    }

    @Test
    public void addEventListener() throws Exception {
        service.addEventListener(new GenericACDEventListener() {
            @Override
            public void processEvents(List<GenericACDEvent> list) {

            }

            @Override
            public void processEvent(GenericACDEvent genericACDEvent) {

            }
        });
        Field field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("acdEventListeners");
        field.setAccessible(true);
        List<GenericACDEventListener> acdEventListeners = (List<GenericACDEventListener>) field.get(service);
        assertNotNull(acdEventListeners);
        assertEquals(acdEventListeners.size(), 1);
    }

    @Test
    public void addLogger() throws Exception {
        service.receiver = tb;
        service.messageParser = parser;
        service.acdMessageTranslator = translator;

        service.addLogger(logger);
        Field field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("logger");
        field.setAccessible(true);
        assertNotNull(field.get(service));

        field = tb.getClass().getDeclaredField("logger");
        field.setAccessible(true);
        assertNotNull(field.get(tb));
        field = parser.getClass().getDeclaredField("logger");
        field.setAccessible(true);
        assertNotNull(field.get(parser));
        field = translator.getClass().getDeclaredField("logger");
        field.setAccessible(true);
        assertNotNull(field.get(translator));
    }

    @Test
    public void start() throws Exception {

        service.receiver = tb;
        service.messageParser = parser;
        service.acdMessageTranslator = translator;
        service.addLogger(logger);
        Field field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 100);
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 100);
        field = service.getClass().getSuperclass().getDeclaredField("openReqUcMessage");
        field.setAccessible(true);
        field.set(service, new OpenReqUcMessage());
        service.init();
        service.start();
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("isStarted");
        field.setAccessible(true);
        assertTrue(((AtomicBoolean)field.get(service)).get());
        service.start();
    }

    @Test
    public void destroy() throws Exception {
        service.receiver = tb;
        service.messageParser = parser;
        service.acdMessageTranslator = translator;
        service.addLogger(logger);
        Field field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 100);
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 100);
        field = service.getClass().getSuperclass().getDeclaredField("openReqUcMessage");
        field.setAccessible(true);
        field.set(service, new OpenReqUcMessage());
        service.init();
        service.destroy();
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("isStarted");
        field.setAccessible(true);
        assertFalse(((AtomicBoolean)field.get(service)).get());
    }

    @Test
    public void actionPerformed() throws Exception {
        service.receiver = tb;
        service.messageParser = parser;
        service.acdMessageTranslator = translator;
        service.addLogger(logger);
        Field field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("messageQueueCapacity");
        field.setAccessible(true);
        field.set(service, 100);
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("eventExecutorPoolSize");
        field.setAccessible(true);
        field.set(service, 100);
        field = service.getClass().getSuperclass().getDeclaredField("openReqUcMessage");
        field.setAccessible(true);
        field.set(service, new OpenReqUcMessage());
        service.init();
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("acdErrorHandler");
        field.setAccessible(true);
        field.set(service, new ACDErrorHandler(){
            public void generalError(GeneralError error) {
                System.out.println("Error" + error.toString());

            }

            public void connectionError(ConnectionError error) {
                System.out.println("ConnectionError" + error.toString());

            }
        });
        service.actionPerformed(new ActionEvent(this, 1, "connected"));
        service.actionPerformed(new ActionEvent(this, 1, "disconnected"));
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("isStarted");
        field.setAccessible(true);
        assertFalse(((AtomicBoolean)field.get(service)).get());
        service.actionPerformed(new ActionEvent(this, 1, "failure"));
        field = service.getClass().getSuperclass().getSuperclass().getDeclaredField("isStarted");
        field.setAccessible(true);
        assertFalse(((AtomicBoolean)field.get(service)).get());
    }

    @Test
    public void eventsProcessorLoopCore1() throws NoSuchFieldException, IllegalAccessException {
        service.receiver = tb;
        service.messageParser = parser;
        service.acdMessageTranslator = translator;
        service.addLogger(logger);
        service.eventsProcessorPoolCore(new FailureEventUcMessage());
        Field field = service.getClass().getSuperclass().getDeclaredField("ucErrorEventsCount");
        field.setAccessible(true);
        assertEquals(((AtomicInteger)field.get(service)).get(), 1);
    }

    @Test
    public void eventsProcessorLoopCore2() throws NoSuchFieldException, IllegalAccessException {
        service.receiver = tb;
        service.messageParser = parser;
        service.acdMessageTranslator = translator;
        service.addLogger(logger);
        service.eventsProcessorPoolCore(new FailureConfUcMessage());
        Field field = service.getClass().getSuperclass().getDeclaredField("ucErrorEventsCount");
        field.setAccessible(true);
        assertEquals(((AtomicInteger)field.get(service)).get(), 1);
    }

    @Test
    public void eventsProcessorLoopCore3() throws NoSuchFieldException, IllegalAccessException {
        service.receiver = tb;
        service.messageParser = parser;
        service.acdMessageTranslator = translator;
        service.addLogger(logger);
        service.eventsProcessorPoolCore(new OpenConfUcMessage());
        Field field = service.getClass().getSuperclass().getDeclaredField("ucErrorEventsCount");
        field.setAccessible(true);
        assertEquals(((AtomicInteger)field.get(service)).get(), 0);
    }
    @Test
    public void eventsProcessorLoopCore4() throws NoSuchFieldException, IllegalAccessException {
        service.receiver = tb;
        service.messageParser = parser;
        service.acdMessageTranslator = translator;
        service.addLogger(logger);
        HeartbeatConfUcMessage h = new HeartbeatConfUcMessage();
        h.fixedFields.put(ConstantsUc.UcMessageFields.InvokeId, 1);
        service.eventsProcessorPoolCore(h);
        Field field = service.getClass().getSuperclass().getDeclaredField("ucErrorEventsCount");
        field.setAccessible(true);
        assertEquals(((AtomicInteger)field.get(service)).get(), 0);
    }

    @Test
    public void eventsProcessorLoopCore5() throws NoSuchFieldException, IllegalAccessException {
        service.receiver = tb;
        service.messageParser = parser;
        service.acdMessageTranslator = translator;
        service.addLogger(logger);
        service.eventsProcessorPoolCore(new CloseConfUcMessage());
        Field field = service.getClass().getSuperclass().getDeclaredField("ucErrorEventsCount");
        field.setAccessible(true);
        assertEquals(((AtomicInteger)field.get(service)).get(), 0);
    }

    @Test
    public void eventsProcessorLoopCore6() throws NoSuchFieldException, IllegalAccessException {
        service.receiver = tb;
        service.messageParser = parser;
        service.acdMessageTranslator = translator;
        service.addLogger(logger);
        service.eventsProcessorPoolCore(new SystemEventUcMessage());
        Field field = service.getClass().getSuperclass().getDeclaredField("ucErrorEventsCount");
        field.setAccessible(true);
        assertEquals(((AtomicInteger)field.get(service)).get(), 0);
    }

    @Test
    public void senderPoolCore() throws NoSuchFieldException, IllegalAccessException {
        service.receiver = tb;
        service.messageParser = parser;
        service.acdMessageTranslator = translator;
        service.addLogger(logger);
        service.senderPoolCore();
        Field field = service.getClass().getSuperclass().getDeclaredField("ucErrorEventsCount");
        field.setAccessible(true);
        assertEquals(((AtomicInteger)field.get(service)).get(), 21);
    }
    @Test
    public void keepAliveLoop() throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        service.receiver = tb;
        service.messageParser = parser;
        service.acdMessageTranslator = translator;
        service.addLogger(logger);
        Method method = service.getClass().getSuperclass().getDeclaredMethod("keepAliveLoop");
        method.setAccessible(true);
        method.invoke(service);
        Field field = service.getClass().getSuperclass().getDeclaredField("ucErrorEventsCount");
        field.setAccessible(true);
        assertEquals(((AtomicInteger)field.get(service)).get(), 21);
    }

    @Test(expected = InvocationTargetException.class)
    public void closeCommunication() throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        service.receiver = tb;
        service.messageParser = parser;
        service.acdMessageTranslator = translator;
        service.addLogger(logger);
        Method method = service.getClass().getSuperclass().getDeclaredMethod("closeUcCommunication");
        method.setAccessible(true);
        method.invoke(service);

    }
}