package com.nice.saas.wfo.acd.cisco.uc.messages;

import com.nice.saas.wfo.acd.cisco.common.InvokeIdGenerator;
import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class OpenReqUcMessageTest {


    OpenReqUcMessage message;
    @Before
    public void  setUp() {
        message = new OpenReqUcMessage();
        InvokeIdGenerator.reset();
    }

    @After
    public void  tearDown() {
    }

    @Test
    public void  setVersionNumber() {
        message.setVersionNumber(1);
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.VersionNumber), 1);

    }

    @Test
    public void  setUccxIdleTimeout() {
        message.setUcIdleTimeout(1);
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.IdleTimeout), 1);
    }

    @Test
    public void  getUccxIdleTimeout() {
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.IdleTimeout), ConstantsUc.DefaultUcIdleTimeout);
    }

    @Test
    public void  setServicesRequested() {
        message.setServicesRequested(1);
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.ServicesRequested), 1);
    }

    @Test
    public void  setAgentStateMask() {
        message.setAgentStateMask("1");
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.AgentStateMask), 1);
    }

    @Test
    public void  getMessageTypeId1() {
        assertEquals(message.messageTypeId, (int) ConstantsUc.UcMessageTypeIds.get(OpenReqUcMessage.class));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void  deserialize() {
        ByteBuffer buffer = ByteBuffer.allocateDirect(4);

        buffer.putInt(3);
        buffer.rewind();
        message.deserialize(buffer);
    }

    @Test
    public void  getMessageTypeId() {
        assertNotNull(message);
        assertEquals(message.messageTypeId, 3);
        assertEquals(message.getMessageTypeId(), 3);
        assertEquals(message.fixedFields.size(), 9);
        assertEquals(message.fixedFieldsTypes.size(), 9);
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.InvokeId), 0);
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.InvokeId));
        Class<?> fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.InvokeId);
        assertEquals(fieldType, Integer.class);

        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.VersionNumber), 19);
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.VersionNumber));
        fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.VersionNumber);
        assertEquals(fieldType, Integer.class);

        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.IdleTimeout), message.getUcIdleTimeout());
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.IdleTimeout));
        fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.IdleTimeout);
        assertEquals(fieldType, Integer.class);

        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.Reserved), 0xFFFFFFFF);
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.Reserved));
        fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.Reserved);
        assertEquals(fieldType, Integer.class);

        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.ServicesRequested), 0x00000010);
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.ServicesRequested));
        fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.ServicesRequested);
        assertEquals(fieldType, Integer.class);

        fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.CallMsgMask);
        assertEquals(fieldType, Integer.class);

        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.AgentStateMask), 0x00000001|0x00000002|0x00000004|0x00000008|0x00000010|0x00000020|0x00000040|0x00000080|0x00000100|0x00000200);
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.AgentStateMask));
        fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.AgentStateMask);
        assertEquals(fieldType, Integer.class);

        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.ConfigMsgMask), 0);
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.ConfigMsgMask));
        fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.ConfigMsgMask);
        assertEquals(fieldType, Integer.class);

       
    }

    @Test
    public void  serialize() {
        ByteBuffer buffer = message.serialize();
        assertNotNull(buffer);
        assertEquals(buffer.position(), 0);
        assertEquals(buffer.remaining(), 59);
        assertEquals(buffer.getInt() , 51);
        assertEquals(buffer.getInt() , 3);
        assertEquals(buffer.getInt() , 0);
        assertEquals(buffer.getInt() , 19);
        assertEquals(buffer.getInt() , message.getUcIdleTimeout());
        assertEquals(buffer.getInt() , 0xFFFFFFFF);
        assertEquals(buffer.getInt() , 0x00000010);
        assertEquals(buffer.getInt() , 0);
        assertEquals(buffer.getInt() , 0x00000001|0x00000002|0x00000004|0x00000008|0x00000010|0x00000020|0x00000040|0x00000080|0x00000100|0x00000200);
        assertEquals(buffer.getInt() , 0);
        byte[] ar = new byte[12];
        buffer.get(ar, 0, 12); //unused block
        assertEquals(buffer.get() , (byte) 1);
        assertEquals(buffer.get() , (byte) 2);
        ar = new byte[64];
        buffer.get(ar, 0, 1); //clientid block
        assertEquals(buffer.get() , (byte) 0);
        assertEquals(buffer.get() , (byte) 2);
        ar = new byte[64];
        buffer.get(ar, 0, 1); //clientpassword block
    }

}