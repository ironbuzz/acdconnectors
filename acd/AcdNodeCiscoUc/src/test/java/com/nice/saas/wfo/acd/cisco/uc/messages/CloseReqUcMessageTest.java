package com.nice.saas.wfo.acd.cisco.uc.messages;

import com.nice.saas.wfo.acd.cisco.common.InvokeIdGenerator;
import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CloseReqUcMessageTest {
    CloseReqUcMessage message;
    @Before
    public void  setUp() {
        message = new CloseReqUcMessage();
        InvokeIdGenerator.reset();
    }

    @After
    public void  tearDown() {
    }

    @Test(expected = UnsupportedOperationException.class)
    public void  deserialize() {
        ByteBuffer buffer = ByteBuffer.allocateDirect(8);

        buffer.putInt(3);
        buffer.putInt(4);
        buffer.rewind();
        message.deserialize(buffer);
    }

    @Test
    public void  getMessageTypeId() {
        assertNotNull(message);
        assertEquals(message.messageTypeId, 7);
        assertEquals(message.getMessageTypeId(), 7);
        assertEquals(message.fixedFields.size(), 2);
        assertEquals(message.fixedFieldsTypes.size(), 2);
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.InvokeId), 0);
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.InvokeId));
        Class<?> fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.InvokeId);
        assertEquals(fieldType, Integer.class);

        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.Status), 0);
        assertNotNull(message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.Status));
        fieldType = message.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.Status);
        assertEquals(fieldType, Integer.class);
    }

    @Test
    public void  serialize() {
        ByteBuffer buffer = message.serialize();
        assertNotNull(buffer);
        assertEquals(buffer.position(), 0);
        assertEquals(buffer.remaining(), 16);
        assertEquals(buffer.getInt() , 8);
        assertEquals(buffer.getInt() , 7);
        assertEquals(buffer.getInt() , 0);
        assertEquals(buffer.getInt() , 0);

    }

}