package com.nice.saas.wfo.acd.cisco.uc.messages;

import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;
import org.junit.Test;

import java.nio.ByteBuffer;

import static org.junit.Assert.*;

public class AgentStateChangeUcMessageTest {
    AgentStateChangeUcMessage m = new AgentStateChangeUcMessage() {

    };

    @Test
    public void constructionTest() {
        assertEquals(m.getMessageTypeId(), 30);
        assertNull(m.fixedFields.get(ConstantsUc.UcMessageFields.UnusedBlock1));
        assertNull(m.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.UnusedBlock1));
        assertEquals((long)m.fixedFieldsLength.get(ConstantsUc.UcMessageFields.UnusedBlock1), 30);

        assertNull(m.fixedFields.get(ConstantsUc.UcMessageFields.AgentState));
        assertEquals(m.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.AgentState), Short.class);

        assertNull(m.fixedFields.get(ConstantsUc.UcMessageFields.EventReasonCode));
        assertEquals(m.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.EventReasonCode), Short.class);

        assertNull(m.fixedFields.get(ConstantsUc.UcMessageFields.UnusedBlock2));
        assertNull(m.fixedFieldsTypes.get(ConstantsUc.UcMessageFields.UnusedBlock2));

        assertNull(m.floatingFields.get(ConstantsUc.UcMessageFields.AgentId));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void  serialize() {
        ByteBuffer buffer = m.serialize();
    }


}