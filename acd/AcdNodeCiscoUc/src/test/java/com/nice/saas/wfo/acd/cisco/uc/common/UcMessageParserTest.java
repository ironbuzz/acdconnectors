package com.nice.saas.wfo.acd.cisco.uc.common;

import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import com.nice.saas.wfo.acd.cisco.uc.messages.*;

import com.nice.saas.wfo.comp.manager.asc.generic.GeneralError;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;


import static org.junit.Assert.*;


public class UcMessageParserTest {
    UcMessageParser parser;
    GenericLogger logger = new GenericLogger() {
        @Override
        public void info(String s) {

        }

        @Override
        public void error(String s, Throwable throwable, Object... objects) {

        }

        @Override
        public void warning(String s) {

        }
    };
    @Before
    public void  setUp() {
        parser = new UcMessageParser();
    }

    @After
    public void  tearDown() {
    }




    @Test
    public void  parseCloseConfUcMessage() {
        ByteBuffer buffer = ByteBuffer.allocateDirect(12);
        buffer.putInt(4);
        buffer.putInt(8);

        buffer.putInt(3);
        buffer.rewind();
        Message message = parser.parse(buffer);
        assertTrue(message instanceof CloseConfUcMessage);
        UcMessage ucMessage = (UcMessage) message;
        assertEquals(ucMessage.getMessageTypeId(), 8);
        assertEquals(ucMessage.fixedFields.get(ConstantsUc.UcMessageFields.InvokeId), 3);
    }

    @Test
    public void  parseFailureConfUcMessage() {
        ByteBuffer buffer = ByteBuffer.allocateDirect(23);
        buffer.putInt(15);
        buffer.putInt(1);

        buffer.putInt(1);
        buffer.putInt(0);
        buffer.put((byte) 7);
        buffer.put((byte) 5);
        String str = "Vasya";
        byte[] tempArray = str.getBytes(Charset.forName("UTF-8"));
        buffer.put(tempArray);
        buffer.rewind();
        Message message = parser.parse(buffer);
        assertTrue(message instanceof FailureConfUcMessage);
        UcMessage ucMessage = (UcMessage) message;
        assertEquals(ucMessage.fixedFields.get(ConstantsUc.UcMessageFields.InvokeId), 1);
        assertEquals(ucMessage.fixedFields.get(ConstantsUc.UcMessageFields.Status), 0);
        assertEquals(ucMessage.floatingFields.get(ConstantsUc.UcMessageFields.Text), "Vasya");
    }

    @Test
    public void  parseFailureEventUcMessage() {
        ByteBuffer buffer = ByteBuffer.allocateDirect(19);
        buffer.putInt(15);
        buffer.putInt(2);

        buffer.putInt(0);
        buffer.put((byte) 7);
        buffer.put((byte) 5);
        String str = "Vasya";
        byte[] tempArray = str.getBytes(Charset.forName("UTF-8"));
        buffer.put(tempArray);
        buffer.rewind();
        Message message = parser.parse(buffer);
        assertTrue(message instanceof FailureEventUcMessage);
        UcMessage ucMessage = (UcMessage) message;
        assertEquals(ucMessage.fixedFields.get(ConstantsUc.UcMessageFields.Status), 0);
        assertEquals(ucMessage.floatingFields.get(ConstantsUc.UcMessageFields.Text), "Vasya");
    }

    @Test
    public void  parseHeartbeatConfUcMessage() {
        ByteBuffer buffer = ByteBuffer.allocateDirect(12);
        buffer.putInt(4);
        buffer.putInt(6);

        buffer.putInt(3);
        buffer.rewind();
        Message message = parser.parse(buffer);
        assertTrue(message instanceof HeartbeatConfUcMessage);
        UcMessage ucMessage = (UcMessage) message;
        assertEquals(ucMessage.fixedFields.get(ConstantsUc.UcMessageFields.InvokeId), 3);
    }

    @Test
    public void  parseOpenConfUcMessage() {
        ByteBuffer buffer = ByteBuffer.allocateDirect(30);
        buffer.putInt(22);
        buffer.putInt(4);

        buffer.putInt(3);
        buffer.putInt(4);
        buffer.put(new byte[12]);
        buffer.putShort((short)5);

        buffer.rewind();
        Message message = parser.parse(buffer);
        assertTrue(message instanceof OpenConfUcMessage);
        UcMessage ucMessage = (UcMessage) message;

        assertEquals(ucMessage.fixedFields.get(ConstantsUc.UcMessageFields.InvokeId), 3);
        assertEquals(ucMessage.fixedFields.get(ConstantsUc.UcMessageFields.ServicesGranted), 4);
        assertEquals(ucMessage.fixedFields.get(ConstantsUc.UcMessageFields.UcOnline), (short)5);
    }


    @Test
    public void  parseSystemEventUcMessage() {
        ByteBuffer buffer = ByteBuffer.allocateDirect(41);
        buffer.putInt(33);
        buffer.putInt(31);

        buffer.putInt(1);
        buffer.putInt(2);
        buffer.putInt(3);
        buffer.putInt(4);
        buffer.putInt(5);
        buffer.putInt(6);
        buffer.putShort((short)7);

        buffer.put((byte) 7);
        buffer.put((byte) 5);
        String str = "Vasya";
        byte[] tempArray = str.getBytes(Charset.forName("UTF-8"));
        buffer.put(tempArray);
        buffer.rewind();
        Message message = parser.parse(buffer);
        assertTrue(message instanceof SystemEventUcMessage);
        UcMessage ucMessage = (UcMessage) message;

        assertEquals(ucMessage.fixedFields.get(ConstantsUc.UcMessageFields.Status), 1);
        assertEquals(ucMessage.fixedFields.get(ConstantsUc.UcMessageFields.Time), 2);
        assertEquals(ucMessage.fixedFields.get(ConstantsUc.UcMessageFields.SystemEventId), 3);
        assertEquals(ucMessage.fixedFields.get(ConstantsUc.UcMessageFields.SystemEventArg1), 4);
        assertEquals(ucMessage.fixedFields.get(ConstantsUc.UcMessageFields.SystemEventArg2), 5);
        assertEquals(ucMessage.fixedFields.get(ConstantsUc.UcMessageFields.SystemEventArg3), 6);
        assertEquals(ucMessage.fixedFields.get(ConstantsUc.UcMessageFields.EventDeviceType), (short)7);
        assertEquals(ucMessage.floatingFields.get(ConstantsUc.UcMessageFields.Text), "Vasya");

    }

    @Test
    public void addLogger() throws NoSuchFieldException, IllegalAccessException {
        parser.addLogger(logger);
        Field field = parser.getClass().getDeclaredField("logger");
        field.setAccessible(true);
        assertNotNull(field.get(parser));
    }

    @Test(expected = NullPointerException.class)
    public void parseError(){
        parser.parse(null);
    }


    @Test(expected = GeneralError.class)
    public void parseError1(){
        ByteBuffer buffer = ByteBuffer.allocateDirect(41);
        buffer.putInt(33);
        buffer.putInt(56);

        buffer.putInt(1);
        buffer.putInt(2);
        buffer.putInt(3);
        buffer.putInt(4);
        buffer.putInt(5);
        buffer.putInt(6);
        buffer.putShort((short)7);

        buffer.put((byte) 7);
        buffer.put((byte) 5);
        String str = "Vasya";
        byte[] tempArray = str.getBytes(Charset.forName("UTF-8"));
        buffer.put(tempArray);
        buffer.rewind();
        parser.parse(buffer);
    }
}