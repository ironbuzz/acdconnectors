package com.nice.saas.wfo.acd.cisco.uc.messages;

import com.nice.saas.wfo.acd.cisco.common.InvokeIdGenerator;
import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import static com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc.UcFloatingFieldsTypeIds;
import static org.junit.Assert.*;

public class AgentStateChangeUccxMessageTest {
    AgentStateChangeUccxMessage message;

    @Before
    public void  setUp() {
        message = new AgentStateChangeUccxMessage();
        InvokeIdGenerator.reset();
    }

    @After
    public void  tearDown() {
    }

    @Test(expected = UnsupportedOperationException.class)
    public void  serialize() {
        ByteBuffer buffer = message.serialize();
    }

    @Test
    public void  getMessageTypeId() {
        assertNotNull(message);
        assertEquals(message.messageTypeId, 30);
        assertEquals(message.getMessageTypeId(), 30);
        assertEquals(message.fixedFields.size(), 4);
        assertEquals(message.fixedFieldsTypes.size(), 4);
        assertEquals(message.floatingFields.size(), 1);

    }

    @Test
    public void  deserialize() {
        UcFloatingFieldsTypeIds.put((byte)0xc2, ConstantsUc.UcMessageFields.AgentId);
        ByteBuffer buffer = ByteBuffer.allocateDirect(65);

        buffer.put(new byte[30]);
        buffer.putShort((short)2);
        buffer.putShort((short)3);

        buffer.put(new byte[24]);
        buffer.put((byte)0xc2);
        buffer.put((byte) 5);
        String str = "Vasya";
        byte[] tempArray = str.getBytes(Charset.forName("UTF-8"));
        buffer.put(tempArray);
        buffer.rewind();
        message = (AgentStateChangeUccxMessage)message.deserialize(buffer);
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.AgentState), (short)2);
        assertEquals(message.fixedFields.get(ConstantsUc.UcMessageFields.EventReasonCode), (short)3);
        assertEquals(message.floatingFields.get(ConstantsUc.UcMessageFields.AgentId), "Vasya");
    }

}