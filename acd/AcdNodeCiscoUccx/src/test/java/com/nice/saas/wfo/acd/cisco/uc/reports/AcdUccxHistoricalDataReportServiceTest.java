package com.nice.saas.wfo.acd.cisco.uc.reports;

import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class AcdUccxHistoricalDataReportServiceTest {
    @Test
    public void  getAcdType() throws NoSuchFieldException, IllegalAccessException {
        AcdUccxHistoricalDataReportService service = new AcdUccxHistoricalDataReportService();
        assertEquals(service.getAcdType(), "CISCO_UCCX");
        service.setQuery("query");
        Field field = service.getClass().getSuperclass().getDeclaredField("query");
        field.setAccessible(true);
        assertEquals(field.get(service), "query");
    }

}