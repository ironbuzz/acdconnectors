package com.nice.saas.wfo.acd.cisco.uc.reports;

import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class AcdUccxAdherenceReportServiceTest {
    @Test
    public void  getAcdType() throws NoSuchFieldException, IllegalAccessException {
        AcdUccxAdherenceReportService service = new AcdUccxAdherenceReportService();
        assertEquals(service.getAcdType(), "CISCO_UCCX");
        service.setQuery("query");
        Field field = service.getClass().getSuperclass().getDeclaredField("query");
        field.setAccessible(true);
        assertEquals(field.get(service), "query");
    }

}