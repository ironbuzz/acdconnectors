package com.nice.saas.wfo.acd.cisco.uc.common;

import com.nice.saas.wfo.acd.cisco.common.DbConnectionFactory;
import org.junit.Test;
import org.mockito.Mockito;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertNotNull;

public class DbConnectionFactoryTest {
     DbConnectionFactory f = Mockito.mock(DbConnectionFactory.class);


    @Test(expected = SQLException.class)
    public void getInstanceInformixWrong() throws Exception {
        DbConnectionFactory f = DbConnectionFactory.getInstance("com.informix.jdbc.IfxDriver", "jdbc:informix-sqli://127.0.0.1:123/qwerty:INFORMIXSERVER=vasya", "vasya","123");
        assertNotNull(f);
        Connection c = f.getConnection();
        assertNotNull(c);
    }




}