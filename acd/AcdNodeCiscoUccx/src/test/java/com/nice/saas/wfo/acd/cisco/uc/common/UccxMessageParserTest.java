package com.nice.saas.wfo.acd.cisco.uc.common;

import com.nice.saas.wfo.acd.cisco.interfaces.Message;
import com.nice.saas.wfo.acd.cisco.uc.messages.AgentStateChangeUcMessage;
import com.nice.saas.wfo.acd.cisco.uc.messages.AgentStateChangeUccxMessage;
import com.nice.saas.wfo.acd.cisco.uc.messages.UcMessage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import static com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc.UcFloatingFieldsTypeIds;
import static com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc.UcMessageTypeIds;
import static com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc.UcMessageTypes;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UccxMessageParserTest {
    UcMessageParser parser;
    @Before
    public void  setUp() {
        parser = new UcMessageParser();
    }

    @After
    public void  tearDown() {
    }

    @Test
    public void parseAgentStateChangeUcMessage() {
        UcFloatingFieldsTypeIds.put((byte)0xc2, ConstantsUc.UcMessageFields.AgentId);
        UcMessageTypeIds.put(AgentStateChangeUccxMessage.class, 30);
        UcMessageTypes.put(ConstantsUc.UcMessageTypeIds.get(AgentStateChangeUccxMessage.class), AgentStateChangeUccxMessage.class);
        ByteBuffer buffer = ByteBuffer.allocateDirect(73);
        buffer.putInt(65);
        buffer.putInt(30);

        buffer.put(new byte[30]);
        buffer.putShort((short)2);
        buffer.putShort((short)3);

        buffer.put(new byte[24]);
        buffer.put((byte)0xc2);
        buffer.put((byte) 5);
        String str = "Vasya";
        byte[] tempArray = str.getBytes(Charset.forName("UTF-8"));
        buffer.put(tempArray);
        buffer.rewind();
        Message message = parser.parse(buffer);
        assertTrue(message instanceof AgentStateChangeUcMessage);
        UcMessage ucMessage = (UcMessage) message;
        assertEquals(ucMessage.getMessageTypeId(), 30);
        assertEquals(ucMessage.fixedFields.get(ConstantsUc.UcMessageFields.AgentState), (short)2);
        assertEquals(ucMessage.fixedFields.get(ConstantsUc.UcMessageFields.EventReasonCode), (short)3);
        assertEquals(ucMessage.floatingFields.get(ConstantsUc.UcMessageFields.AgentId), "Vasya");
    }
}
