package com.nice.saas.wfo.acd.cisco.uc.messages;

import com.nice.saas.wfo.acd.cisco.uc.common.ConstantsUc;

/**
 * AgentStateChangeUccxMessage - indicates agent state change
 */
public class AgentStateChangeUccxMessage extends AgentStateChangeUcMessage {
    public AgentStateChangeUccxMessage() {
        fixedFieldsLength.put(ConstantsUc.UcMessageFields.UnusedBlock2, 24);
    }

}
