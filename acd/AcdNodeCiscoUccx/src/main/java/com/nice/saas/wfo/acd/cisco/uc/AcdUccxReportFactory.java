package com.nice.saas.wfo.acd.cisco.uc;

import com.nice.saas.wfo.acd.cisco.AcdEventServiceBase;
import com.nice.saas.wfo.acd.cisco.AcdReportService;
import com.nice.saas.wfo.acd.cisco.common.Constants;
import com.nice.saas.wfo.acd.cisco.uc.reports.AcdUccxAdherenceReportService;
import com.nice.saas.wfo.acd.cisco.uc.reports.AcdUccxHistoricalDataReportService;
import com.nice.saas.wfo.acd.historicaldata.GenericReportFactory;
import com.nice.saas.wfo.acd.historicaldata.GenericReportService;
import com.nice.saas.wfo.comp.manager.asc.generic.GenericLogger;
import com.nice.saas.wfo.comp.manager.asc.generic.ReportType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ACD report factory
 */
@Component
public class AcdUccxReportFactory implements GenericReportFactory {

    private String acdType = Constants.AcdCiscoUccxReportType;

    @Autowired
    AcdUccxAdherenceReportService acdUccxAdherenceReportService;

    @Autowired
    AcdUccxHistoricalDataReportService acdUccxHistoricalDataReportService;

    @Autowired
    AcdEventServiceBase rtaService;


    private GenericLogger logger;

    @Override
    public void addLogger(GenericLogger logger) {
        this.logger = logger;
    }

    @Override
    public void terminate(GenericLogger logger) {
        rtaService.destroy();
    }

    /**
     * Get report service by type
     * @param reportType
     * @return
     */
    @Override
    public GenericReportService getReportService(ReportType reportType) {
        AcdReportService reportService = null;
        if (acdType == null) {
            acdType = Constants.AcdCiscoUccxReportType;
        }

        switch (reportType) {
            case Adherence:
                reportService = acdUccxAdherenceReportService;
                break;
            case Forecast:
                reportService = acdUccxHistoricalDataReportService;
                break;
        }



        if(reportService != null){
            reportService.addLogger(logger);
            reportService.setAcdType(acdType);
        }
        return reportService;
    }

    /**
     * get ACD type
     * @return
     */
    @Override
    public String getAcdType() {
        return acdType;
    }
}
