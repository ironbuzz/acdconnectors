package com.nice.saas.wfo.acd.cisco.uc.reports;

import com.nice.saas.wfo.acd.cisco.AcdReportService;
import com.nice.saas.wfo.acd.cisco.common.Constants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * AcdUccxAdherenceReportService
 */
@Component
public class AcdUccxAdherenceReportService extends AcdReportService {

    public AcdUccxAdherenceReportService() {
        super.setDbClassName(Constants.InformixDriverClassName);
    }

    @Override
    @Value("${cisco.uc.acd.reports.acdUccxAdherenceReportService.query}")
    protected void setQuery(String query) {
        super.setQuery(query);
    }

}
