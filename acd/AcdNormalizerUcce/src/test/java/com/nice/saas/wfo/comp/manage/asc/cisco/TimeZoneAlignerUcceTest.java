package com.nice.saas.wfo.comp.manage.asc.cisco;

import org.junit.Test;

import static org.junit.Assert.*;

public class TimeZoneAlignerUcceTest {
    @Test
    public void align() throws Exception {
        TimeZoneAlignerUcce t= new TimeZoneAlignerUcce();
        assertEquals("+00:00", t.align(""));
        assertEquals("+00:00", t.align("0 "));
        assertEquals("-03:00", t.align("-180"));
        assertEquals("+02:00", t.align("120"));
        assertEquals("-10:00", t.align("-600"));
        assertEquals("+10:00", t.align("600"));
    }

}