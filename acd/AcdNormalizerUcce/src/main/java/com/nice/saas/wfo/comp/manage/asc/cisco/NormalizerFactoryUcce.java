package com.nice.saas.wfo.comp.manage.asc.cisco;

import org.springframework.stereotype.Component;

@Component
public class NormalizerFactoryUcce extends NormalizerFactoryBase {

    public NormalizerFactoryUcce() {
        acdType = "CISCO_UCCE";
        timeZoneAligner = new TimeZoneAlignerUcce();
        super.init();
    }

}
