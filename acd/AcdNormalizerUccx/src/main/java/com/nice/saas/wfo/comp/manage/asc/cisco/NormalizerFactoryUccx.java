package com.nice.saas.wfo.comp.manage.asc.cisco;


import org.springframework.stereotype.Component;

@Component
public class NormalizerFactoryUccx extends NormalizerFactoryBase {

    public NormalizerFactoryUccx() {
        acdType = "CISCO_UCCX";
        timeZoneAligner = new TimeZoneAlignerUccx();
        super.init();
    }

}
