package com.nice.saas.wfo.comp.manage.asc.cisco;

import org.junit.Test;
import com.nice.saas.wfo.comp.manager.asc.generic.Normalizer;
import com.nice.saas.wfo.comp.manager.asc.generic.ReportType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


public class TimeZoneAlignerUccxTest {
    @Test
    public void align() throws Exception {
        TimeZoneAlignerUccx t= new TimeZoneAlignerUccx();
        assertEquals("+00:00", t.align(""));
        assertEquals("+00:00", t.align("0 "));
        assertEquals("-03:00", t.align("-180"));
        assertEquals("+02:00", t.align("120"));
        assertEquals("-10:00", t.align("-600"));
        assertEquals("+10:00", t.align("600"));
    }

}