package com.nice.saas.wfo.comp.manage.asc.cisco;

import com.nice.saas.wfo.comp.manager.asc.generic.Normalizer;
import com.nice.saas.wfo.comp.manager.asc.generic.ReportType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.util.Map;



public class NormalizerFactoryUccxTest {
    NormalizerFactoryUccx n = new NormalizerFactoryUccx();
    @Before
    public void setUp() throws NoSuchFieldException, IllegalAccessException {

        n.init();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void init() throws NoSuchFieldException, IllegalAccessException {
        Map<ReportType, Normalizer> normalizers = null;

        Field field = n.getClass().getSuperclass().getDeclaredField("normalizers");
        field.setAccessible(true);
        normalizers = (Map<ReportType, Normalizer>)field.get(n);
        assertNotNull (field.get(n));
        assertEquals(normalizers.values().size(), 2);
        assertTrue(normalizers.get(ReportType.Adherence) instanceof AdherenceNormalizer);
        assertTrue(normalizers.get(ReportType.Forecast) instanceof ForecastNormalizer);
    }

    @Test
    public void getNormalizer() {

        Normalizer r = n.getNormalizer(ReportType.Adherence, 0);
        assertTrue(r instanceof AdherenceNormalizer);
        r =n.getNormalizer(ReportType.Forecast, 0);
        assertTrue(r instanceof ForecastNormalizer);
    }

    @Test
    public void getAcdType() throws NoSuchFieldException, IllegalAccessException {
        String acdType = null;
        Field field = n.getClass().getSuperclass().getDeclaredField("acdType");
        field.setAccessible(true);
        field.set(n, "CISCO_UCCX");
        assertEquals(n.getAcdType(), "CISCO_UCCX");

        field.set(n, "CISCO_UCCE");
        assertEquals(n.getAcdType(), "CISCO_UCCE");

    }

}